import React, { Component } from 'react';
import { Text } from 'react-native';
import * as fonts from './fonts';

class FontawesomeIcon extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <Text style={{
                fontFamily: fonts.Fontawesome,
                color: this.props.color,
                fontSize: this.props.size,
                ...this.props.customStyle
            }}>
                {this.props.nameIcon}
            </Text>
        );
    }
}

export default FontawesomeIcon;
