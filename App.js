import React, { Component } from 'react';
import { View, Text } from 'react-native';
import MainApp from './Components/Main Structure/MainApp'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <MainApp />
      </View>
    );
  }
}

export default App;
