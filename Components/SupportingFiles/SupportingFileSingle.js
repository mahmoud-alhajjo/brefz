import React, { Component } from 'react';
import { View, Text, StyleSheet, Animated, Dimensions } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import mainStyles from '../../css/MainStyle';
import OverlayButton from '../Helpers/OverlayButton';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

import Comments from '../Main Structure/Comments';
import DrawerHeader from '../Main Structure/DrawerHeader';

class SupportingFileSingle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animationDrawer: new Animated.Value(-Dimensions.get('window').width),
            widthExternalUrlButton: 0
        };
    }

    startAnimationOpenDrawer = () => {
        Animated.timing(this.state.animationDrawer, {
            toValue: 0,
            duration: 300,
            delay: 200,
        }).start();
    }

    startAnimationCloseDrawer = () => {
        Animated.timing(this.state.animationDrawer, {
            toValue: -Dimensions.get('window').width,
            duration: 500,
        }).start();
    }

    goBackAction = () => {
        this.startAnimationCloseDrawer();
        this.props.navigation.goBack();
    }

    render() {
        this.startAnimationOpenDrawer();
        const animationStyle = { transform: [{ translateX: this.state.animationDrawer }] }
        let colorOverlay = colors.overlay
        if (this.props.navigation.state.params) {
            const { hideOverlayInNextScreen } = this.props.navigation.state.params
            colorOverlay = hideOverlayInNextScreen ? 'tranparent' : colors.overlay
        }
        let single = false
        let styleExternalUrlButton = single ? styles.fileLinkButtonSingle : [styles.fileLinkButton, { left: MeasurementCalculator.measure(26, 'w') }]
        let stylePhysicalFileUrlButton = single ? styles.fileLinkButtonSingle : [styles.fileLinkButton, { left: MeasurementCalculator.measure(168, 'w')}]
        let externalUrl =
            <OverlayButton
                buttonBackgroundColor={colors.purple3}
                buttonTextColor={colors.white}
                clickButton={() => { }}
                style={{ flex: 1 }}
                text={' External URL'}
                nameIcon={icons.External_Link}
                size={MeasurementCalculator.measureFont(12)}
                customStyle={styleExternalUrlButton}
                textButtonStyle={styles.fileLinkTextButton} />
        let physicalFileUrl =
            <OverlayButton
                buttonBackgroundColor={colors.green3}
                buttonTextColor={colors.white}
                clickButton={() => { }}
                style={{ flex: 1 }}
                text={' Physical file URL'}
                nameIcon={icons.Share_Square_O}
                size={MeasurementCalculator.measureFont(12)}
                customStyle={stylePhysicalFileUrlButton}
                textButtonStyle={styles.fileLinkTextButton} />

        return (
            <View style={[styles.overlay, { backgroundColor: colorOverlay }]}>
                <Animated.View style={[animationStyle, styles.container]}>
                    <DrawerHeader navigation={this.props.navigation} title={'File Name'} goBackAction={this.goBackAction} />
                    <View style={styles.supportingFileDetails}>
                        {externalUrl}
                        {physicalFileUrl}
                        <View style={[mainStyles.rowFlex, { justifyContent: 'space-between', alignItems: 'center' }]}>
                            <View style={{ paddingTop: MeasurementCalculator.measure(27.1) }}>
                                <Text style={styles.creationDate}>Creation Date</Text>
                                <Text style={styles.CommentsText}>Comments</Text>
                            </View>
                            <View style={{ paddingTop: MeasurementCalculator.measure(24) }}>
                                <Text style={styles.dateTime}>22/7/2019</Text>
                                <Text style={styles.countComments}>23</Text>
                            </View>
                        </View>
                    </View>
                    <Comments />
                </Animated.View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    overlay: {
        flex: 1,
        width: Dimensions.get('window').width,
        backgroundColor: colors.overlay,
    },
    container: {
        flex: 1,
        width: Dimensions.get('window').width - 31,
        backgroundColor: colors.white,
    },
    supportingFileDetails: {
        backgroundColor: colors.lightGray,
        paddingLeft: MeasurementCalculator.measure(26, 'w'),
        paddingRight: MeasurementCalculator.measure(38.4, 'w'),
        paddingBottom: MeasurementCalculator.measure(19.5),
        borderBottomRightRadius: 80,
    },
    fileLinkButton: {
        width: MeasurementCalculator.measure(139, 'w'),
        height: MeasurementCalculator.measure(23),
        justifyContent: 'center',
        position: 'absolute',
        top: MeasurementCalculator.measure(-17.5),
        zIndex: 1
    },
    fileLinkButtonSingle: {
        paddingHorizontal: MeasurementCalculator.measure(16.9, 'w'),
        paddingVertical: MeasurementCalculator.measure(7.25),
        height: MeasurementCalculator.measure(25.49),
        justifyContent: 'center',
        position: 'absolute',
        top: MeasurementCalculator.measure(-12.745),
        left: MeasurementCalculator.measure(26, 'w'),
        zIndex: 1
    },
    fileLinkTextButton: {
        fontFamily: fonts.Montserrat_Regular,
        fontSize: MeasurementCalculator.measureFont(12),
        textAlign: 'center',
    },
    creationDate: {
        fontFamily: fonts.Montserrat_Light,
        fontSize: MeasurementCalculator.measureFont(16),
        color: colors.purple2,
    },
    dateTime: {
        fontFamily: fonts.Montserrat_Bold,
        fontSize: MeasurementCalculator.measureFont(10),
        color: colors.white,
        backgroundColor: colors.lightBlue,
        borderRadius: 8,
        textAlign: 'center',
        letterSpacing: 2.5,
        paddingVertical: MeasurementCalculator.measure(6.4),
        paddingHorizontal: MeasurementCalculator.measure(12.3, 'w')
    },
    CommentsText: {
        fontFamily: fonts.Montserrat_Bold,
        fontSize: MeasurementCalculator.measureFont(20),
        color: colors.black,
        paddingRight: MeasurementCalculator.measure(12, 'w'),
        marginTop: MeasurementCalculator.measure(17.3)
    },
    countComments: {
        fontFamily: fonts.Montserrat_Bold,
        fontSize: MeasurementCalculator.measureFont(10),
        color: colors.white,
        backgroundColor: colors.lightBlue,
        borderRadius: 8,
        textAlign: 'center',
        alignSelf: 'flex-start',
        paddingVertical: MeasurementCalculator.measure(6.4),
        paddingHorizontal: MeasurementCalculator.measure(8.8, 'w'),
        marginTop: MeasurementCalculator.measure(12.2)
    },
})

export default SupportingFileSingle;
