import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import OverlayButton from '../Helpers/OverlayButton';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

class SupportingFileItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            heightSupportingFileDate: 0,
        };
    }

    supportingFileSingleAction = () => {
        this.props.navigation.navigate('SupportingFileSingle', {
            hideOverlayInNextScreen: true
        });
    }

    findDimesions(layout) {
        const { x, y, width, height } = layout;
        this.setState({ heightSupportingFileDate: height })
    }

    render() {
        let textButton = (this.props.item.status == 'openFile') ? ' OPEN FILE' : ' DOWNLOAD'
        let backgroundColorButton = (this.props.item.status == 'openFile') ? colors.green5 : colors.lightBlue
        let iconButton = (this.props.item.status == 'openFile') ? icons.Pencil_Square_O : icons.Upload
        let backgroundColor = colors.white
        let backgroundColorCorner = colors.lightGray
        let paddingTop = MeasurementCalculator.measure(29) - this.state.heightSupportingFileDate / 2
        let paddingBottomLastItem = 0
        if (this.props.index === 0) {
            paddingTop = this.props.heightDrawerHeader + MeasurementCalculator.measure(19)
        }
        if ((this.props.index % 2) == 0) {
            backgroundColor = colors.lightGray
            backgroundColorCorner = colors.white
        }
        if (this.props.lastItem == this.props.index) {
            paddingBottomLastItem = MeasurementCalculator.measure(50)
        }
        return (
            <TouchableOpacity onPress={this.supportingFileSingleAction} activeOpacity={.9}>
                <View style={[mainStyles.flex, { backgroundColor: backgroundColorCorner, paddingBottom: paddingBottomLastItem }]}>
                    <View style={[styles.allsupportingFilesDetails, mainStyles.rowFlex, { backgroundColor: backgroundColor, marginBottom: this.state.heightSupportingFileDate / 2, paddingTop: paddingTop }]}>
                        <View style={[mainStyles.rowFlex, { alignItems: 'center' }]}>
                            <FontawesomeIcon nameIcon={icons.File_Pdf_O} color={colors.black} size={MeasurementCalculator.measureFont(16)} />
                            <Text style={styles.textSupportingFile}>{this.props.item.fileName}</Text>
                        </View>
                        <OverlayButton
                            buttonBackgroundColor={backgroundColorButton}
                            buttonTextColor={colors.white}
                            clickButton={() => { }}
                            style={{ flex: 1 }}
                            text={textButton}
                            nameIcon={iconButton}
                            size={MeasurementCalculator.measureFont(11)}
                            customStyle={styles.fileButton}
                            textButtonStyle={styles.fileTextButton} />
                        <Text onLayout={(event) => { this.findDimesions(event.nativeEvent.layout) }} style={[styles.supportingFileDate, { bottom: - this.state.heightSupportingFileDate / 2 }]}>{this.props.item.date}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    allsupportingFilesDetails: {
        paddingLeft: MeasurementCalculator.measure(19, 'w'),
        paddingRight: MeasurementCalculator.measure(56, 'w'),
        paddingTop: MeasurementCalculator.measure(29),
        paddingBottom: MeasurementCalculator.measure(32.3),
        borderBottomRightRadius: 80,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    imageStyle: {
        width: MeasurementCalculator.measure(17.92, 'w'),
        height: MeasurementCalculator.measure(20.24),
    },
    textSupportingFile: {
        fontSize: MeasurementCalculator.measureFont(16),
        fontFamily: fonts.Montserrat_SemiBold,
        color: colors.purple2,
        paddingLeft: MeasurementCalculator.measure(16.1, 'w'),
    },
    fileButton: {
        height: MeasurementCalculator.measure(24.14),
        width: MeasurementCalculator.measure(95, 'w'),
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    fileTextButton: {
        fontFamily: fonts.Montserrat_Regular,
        fontSize: MeasurementCalculator.measureFont(12),
        letterSpacing: 0.45,
        textAlign: 'center'
    },
    supportingFileDate: {
        zIndex: 2,
        position: 'absolute',
        left: MeasurementCalculator.measure(17, 'w'),
        fontFamily: fonts.Montserrat_Medium,
        fontSize: MeasurementCalculator.measureFont(10),
        color: colors.white,
        textAlign: 'center',
        paddingHorizontal: MeasurementCalculator.measure(14.5, 'w'),
        paddingVertical: MeasurementCalculator.measure(4),
        borderRadius: 29,
        backgroundColor: colors.lightBlue,
        letterSpacing: 1.9
    }
})

export default SupportingFileItem;
