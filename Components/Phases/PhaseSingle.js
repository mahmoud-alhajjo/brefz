import React, { Component } from 'react';
import { View, Text, StyleSheet, Animated, Dimensions } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

import Comments from '../Main Structure/Comments';
import DrawerHeader from '../Main Structure/DrawerHeader';

class PhaseSingle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animationDrawer: new Animated.Value(-Dimensions.get('window').width),
        };
    }

    startAnimationOpenDrawer = () => {
        Animated.timing(this.state.animationDrawer, {
            toValue: 0,
            duration: 300,
            delay: 200,
        }).start();
    }

    startAnimationCloseDrawer = () => {
        Animated.timing(this.state.animationDrawer, {
            toValue: -Dimensions.get('window').width,
            duration: 500,
        }).start();
    }

    goBackAction = () => {
        this.startAnimationCloseDrawer();
        this.props.navigation.goBack();
    }

    render() {
        this.startAnimationOpenDrawer();
        const animationStyle = { transform: [{ translateX: this.state.animationDrawer }] }
        let colorOverlay = colors.overlay
        if (this.props.navigation.state.params) {
            const { hideOverlayInNextScreen } = this.props.navigation.state.params
            colorOverlay = hideOverlayInNextScreen ? 'tranparent' : colors.overlay
        }
        return (
            <View style={[styles.overlay, { backgroundColor: colorOverlay }]}>
                <Animated.View style={[animationStyle, styles.container]}>
                    <DrawerHeader navigation={this.props.navigation} title={'File Name'} goBackAction={this.goBackAction} />
                    <View style={styles.phaseDetails}>
                        <Text style={styles.dateHeader}>20/7/2019</Text>
                        <View style={[mainStyles.rowFlex, { justifyContent: 'space-between' }]}>
                            <View>
                                <Text style={styles.textDate}>Estimated Time</Text>
                                <Text style={[styles.textDate, {marginBottom: MeasurementCalculator.measure(13)}]}>Start Date</Text>
                                <Text style={styles.textDate}>End Date</Text>
                                <Text style={styles.CommentsText}>Comments</Text>
                            </View>
                            <View>
                                <Text style={styles.dateTime}>56 Hours</Text>
                                <Text style={styles.dateTime}>22/7/2019</Text>
                                <Text style={styles.dateTime}>22/7/2019</Text>
                                <Text style={styles.countComments}>23</Text>
                            </View>
                        </View>
                    </View>
                    <Comments />
                </Animated.View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    overlay: {
        flex: 1,
        width: Dimensions.get('window').width,
    },
    container: {
        flex: 1,
        width: Dimensions.get('window').width - 31,
        backgroundColor: colors.white,
    },
    phaseDetails: {
        backgroundColor: colors.lightGray,
        paddingLeft: MeasurementCalculator.measure(28.2, 'w'),
        paddingRight: MeasurementCalculator.measure(35.4, 'w'),
        paddingBottom: MeasurementCalculator.measure(20.5),
        paddingTop: MeasurementCalculator.measure(23),
        borderBottomRightRadius: 80,
    },
    dateHeader: {
        paddingHorizontal: MeasurementCalculator.measure(11, 'w'),
        paddingVertical: MeasurementCalculator.measure(4),
        height: MeasurementCalculator.measure(27),
        textAlign: 'center',
        position: 'absolute',
        top: MeasurementCalculator.measure(-13.5),
        left: MeasurementCalculator.measure(26, 'w'),
        zIndex: 1,
        fontFamily: fonts.Montserrat_Medium,
        fontSize: MeasurementCalculator.measureFont(12),
        color: colors.white,
        letterSpacing: 1.9,
        backgroundColor: colors.darkBlue3,
        borderRadius: 29
    },
    textDate: {
        fontFamily: fonts.Montserrat_Light,
        fontSize: MeasurementCalculator.measureFont(16),
        color: colors.purple2,
        marginBottom:  MeasurementCalculator.measure(14)
    },
    dateTime: {
        fontFamily: fonts.Montserrat_Bold,
        fontSize: MeasurementCalculator.measureFont(10),
        color: colors.white,
        backgroundColor: colors.lightBlue,
        borderRadius: 8,
        textAlign: 'center',
        alignSelf: 'flex-start',
        letterSpacing: 2.5,
        paddingVertical: MeasurementCalculator.measure(6.4),
        paddingHorizontal: MeasurementCalculator.measure(12.3, 'w'),
        marginBottom:  MeasurementCalculator.measure(7.2)
    },
    CommentsText: {
        fontFamily: fonts.Montserrat_Bold,
        fontSize: MeasurementCalculator.measureFont(20),
        color: colors.black,
        paddingRight: MeasurementCalculator.measure(12, 'w'),
        marginTop: MeasurementCalculator.measure(4.3)
    },
    countComments: {
        fontFamily: fonts.Montserrat_Bold,
        fontSize: MeasurementCalculator.measureFont(10),
        color: colors.white,
        backgroundColor: colors.lightBlue,
        borderRadius: 8,
        textAlign: 'center',
        alignSelf: 'flex-start',
        paddingVertical: MeasurementCalculator.measure(6.4),
        paddingHorizontal: MeasurementCalculator.measure(8.8, 'w'),
        marginTop: MeasurementCalculator.measure(13.2)
    },
})

export default PhaseSingle;
