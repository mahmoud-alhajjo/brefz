import React, { Component } from 'react';
import { View, Text, StyleSheet, Animated, Dimensions, FlatList } from 'react-native';
import colors from '../../css/colors';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

import PhasesItem from './PhasesItem';
import DrawerHeader from '../Main Structure/DrawerHeader';

class AllPhases extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animationDrawer: new Animated.Value(-Dimensions.get('window').width),
        };
        phases = [
            {
                phase: 'Design',
                status: 'Done',
                date: '20/7/2019'
            },
            {
                phase: 'Front end',
                status: 'CANCELD',
                date: '20/7/2019'
            },
            {
                phase: 'Back end',
                status: 'WAITING',
                date: '20/7/2019'
            },
            {
                phase: 'Testing',
                status: 'Done',
                date: '20/7/2019'
            },
            {
                phase: 'Testing',
                status: 'CANCELD',
                date: '20/7/2019'
            },
            {
                phase: 'App Design',
                status: 'WAITING',
                date: '20/7/2019'
            },
            {
                phase: 'Design',
                status: 'Done',
                date: '20/7/2019'
            },
            {
                phase: 'Front end',
                status: 'CANCELD',
                date: '20/7/2019'
            },
            {
                phase: 'Back end',
                status: 'WAITING',
                date: '20/7/2019'
            },
            {
                phase: 'Testing',
                status: 'Done',
                date: '20/7/2019'
            },
            {
                phase: 'Testing',
                status: 'CANCELD',
                date: '20/7/2019'
            },
        ]
    }

    startAnimationOpenDrawer = () => {
        Animated.timing(this.state.animationDrawer, {
            toValue: 0,
            duration: 300,
            delay: 200,
        }).start();
    }

    startAnimationCloseDrawer = () => {
        Animated.timing(this.state.animationDrawer, {
            toValue: -Dimensions.get('window').width,
            duration: 500,
        }).start();
    }

    goBackAction = () => {
        this.startAnimationCloseDrawer();
        this.props.navigation.goBack();
    }

    render() {
        this.startAnimationOpenDrawer();
        const animationStyle = { transform: [{ translateX: this.state.animationDrawer }] }        
        return (
            <View style={styles.overlay}>
                <Animated.View style={[animationStyle, styles.container]}>
                    <DrawerHeader navigation={this.props.navigation} title={'All Phases'} goBackAction={this.goBackAction} />
                    <FlatList
                        data={phases}
                        renderItem={({ item, index }) =>
                            <PhasesItem item={item} index={index} navigation={this.props.navigation} lastItem={phases.length - 1} heightDrawerHeader={MeasurementCalculator.measure(96.92792443971377)} />
                        }
                        keyExtractor={(item, index) => index.toString()}
                        showsVerticalScrollIndicator={false}
                        style={styles.list} />
                </Animated.View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    overlay: {
        flex: 1,
        width: Dimensions.get('window').width,
        backgroundColor: colors.overlay,
    },
    container: {
        flex: 1,
        width: Dimensions.get('window').width - 31,
        backgroundColor: 'transparent',
    },
    list: {
        width: Dimensions.get('window').width - 31,
        height: Dimensions.get('window').height,
        position: 'absolute',
        backgroundColor: colors.white,
        top: 0,
        left: 0,
        zIndex: -1,
    }
})

export default AllPhases;
