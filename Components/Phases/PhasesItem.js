import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

class PhasesItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            heightPhaseDate: 0,
        };
    }

    phaseSingleAction = () => {
        this.props.navigation.navigate('PhaseSingle', {
            hideOverlayInNextScreen: true
        });
    }

    findDimesions(layout) {
        const { x, y, width, height } = layout;
        this.setState({ heightPhaseDate: height })
    }

    render() {
        let circleDoneBackgroundColor = this.props.item.status == 'Done' ? colors.green4 : this.props.item.status == 'WAITING' ? colors.yellow7 : colors.red6
        let textDoneBackgroundColor = this.props.item.status == 'Done' ? colors.lightGreen4 : this.props.item.status == 'WAITING' ? colors.lightYellow2 : colors.lightRed5
        let backgroundColor = colors.white
        let backgroundColorCorner = colors.lightGray
        let paddingTop = MeasurementCalculator.measure(29) - this.state.heightPhaseDate / 2
        let paddingBottomLastItem = 0
        if (this.props.index === 0) {
            paddingTop = this.props.heightDrawerHeader + MeasurementCalculator.measure(19)
        }
        if ((this.props.index % 2) == 0) {
            backgroundColor = colors.lightGray
            backgroundColorCorner = colors.white
        }
        if (this.props.lastItem == this.props.index) {
            paddingBottomLastItem = MeasurementCalculator.measure(50)
        }
        return (
            <TouchableOpacity onPress={this.phaseSingleAction} activeOpacity={.9}>
                <View style={[mainStyles.flex, { backgroundColor: backgroundColorCorner, paddingBottom: paddingBottomLastItem }]}>
                    <View style={[styles.allphasesDetails, mainStyles.rowFlex, { backgroundColor: backgroundColor, marginBottom: this.state.heightPhaseDate / 2, paddingTop: paddingTop }]}>
                        <View style={[mainStyles.rowFlex, { alignItems: 'center' }]}>
                            <Image style={styles.imageStyle} resizeMode="contain" tintColor={colors.black} source={require('../../Images/Phase.png')} />
                            <Text style={styles.textPhase}>{this.props.item.phase}</Text>
                        </View>
                        <View style={[mainStyles.rowFlex, styles.phaseDone, { alignItems: 'center', backgroundColor: textDoneBackgroundColor }]}>
                            <View style={[styles.circleDone, { backgroundColor: circleDoneBackgroundColor }]} />
                            <Text style={styles.textDone}> {this.props.item.status}</Text>
                        </View>
                        <Text onLayout={(event) => { this.findDimesions(event.nativeEvent.layout) }} style={[styles.phaseDate, { bottom: - this.state.heightPhaseDate / 2 }]}>{this.props.item.date}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    allphasesDetails: {
        paddingLeft: MeasurementCalculator.measure(19, 'w'),
        paddingRight: MeasurementCalculator.measure(56, 'w'),
        paddingTop: MeasurementCalculator.measure(29),
        paddingBottom: MeasurementCalculator.measure(32.3),
        borderBottomRightRadius: 80,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    imageStyle: {
        width: MeasurementCalculator.measure(17.92, 'w'),
        height: MeasurementCalculator.measure(20.24),
    },
    textPhase: {
        fontSize: MeasurementCalculator.measureFont(16),
        fontFamily: fonts.Montserrat_SemiBold,
        color: colors.purple2,
        paddingLeft: MeasurementCalculator.measure(8.5, 'w'),
    },
    phaseDone: {
        paddingHorizontal: MeasurementCalculator.measure(7, 'w'),
        paddingVertical: MeasurementCalculator.measure(2.5),
        borderRadius: 5,
        backgroundColor: colors.lightGreen4,
    },
    circleDone: {
        width: MeasurementCalculator.measure(8),
        height: MeasurementCalculator.measure(8),
        backgroundColor: colors.green4,
        borderRadius: 50,
    },
    textDone: {
        fontSize: MeasurementCalculator.measureFont(12),
        fontFamily: fonts.Montserrat_Medium,
        color: colors.black,
    },
    phaseDate: {
        zIndex: 2,
        position: 'absolute',
        left: MeasurementCalculator.measure(17, 'w'),
        fontFamily: fonts.Montserrat_Medium,
        fontSize: MeasurementCalculator.measureFont(10),
        color: colors.white,
        textAlign: 'center',
        paddingHorizontal: MeasurementCalculator.measure(14.5, 'w'),
        paddingVertical: MeasurementCalculator.measure(4),
        borderRadius: 29,
        backgroundColor: colors.lightBlue,
        letterSpacing: 1.9
    }
})

export default PhasesItem;
