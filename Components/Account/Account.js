import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Switch, ScrollView } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';
import ConfirmationPopup from '../Popup/ConfirmationPopup';

import Modal from 'react-native-modal';
import { closeOverlay, openOverlay } from 'react-native-blur-overlay';

class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      switchPushNotification: false,
      switchPhoneCaledar: false,
      switchEnableFinger: false,
      accountSettingsModal: false,
      LogoutModal: false,
    };
  }

  toggleSwitchPushNotification = (value) => {
    this.setState({ switchPushNotification: value })
  }
  toggleSwitchPhoneCaledar = (value) => {
    this.setState({ switchPhoneCaledar: value })
  }
  toggleSwitchEnableFinger = (value) => {
    this.setState({ switchEnableFinger: value })
  }

  closeAccountSettingsPopup = () => {
    this.setState({ accountSettingsModal: false });
    closeOverlay();
  }

  openAccountSettingsPopup = () => {
    openOverlay();
    this.setState({ accountSettingsModal: true });
  };

  closeLogoutPopup = () => {
    this.setState({ LogoutModal: false });
    closeOverlay();
  }

  openLogoutPopup = () => {
    openOverlay();
    this.setState({ LogoutModal: true });
  };


  ChangePasswordAction = () => {
    this.props.navigation.navigate('ChangePassword');
  }

  ChangeEmailAction = () => {
    this.props.navigation.navigate('ChangeEmail');
  }

  PrivacyPolicyAction = () => {
    this.props.navigation.navigate('PrivacyPolicy');
  }

  TermsAndConditionAction = () => {
    this.props.navigation.navigate('TermsAndCondition');
  }

  render() {
    return (
      <ScrollView style={[mainStyles.flex, styles.container, { backgroundColor: colors.white }]}>
        <View style={[mainStyles.rowFlex, styles.itemView]}>
          <Text style={[styles.itemText]}>
            <FontawesomeIcon nameIcon={icons.Bell_O} color={colors.black2} size={MeasurementCalculator.measureFont(16)} /> Push Notification
          </Text>
          <Switch
            style={{ backgroundColor: colors.white }}
            thumbColor={colors.white}
            trackColor={{ false: colors.gray2, true: colors.blue2 }}
            ios_backgroundColor={colors.gray2}
            onValueChange={this.toggleSwitchPushNotification}
            value={this.state.switchPushNotification}
          />
        </View>
        <View style={[mainStyles.rowFlex, styles.itemView]}>
          <Text style={styles.itemText}>
            <FontawesomeIcon nameIcon={icons.Calendar_Plus_O} color={colors.black2} size={MeasurementCalculator.measureFont(16)} customStyle={{}} /> Sync with phone calendar
          </Text>
          <Switch
            style={{ backgroundColor: colors.white }}
            thumbColor={colors.white}
            trackColor={{ false: colors.gray2, true: colors.blue2 }}
            ios_backgroundColor={colors.gray2}
            onValueChange={this.toggleSwitchPhoneCaledar}
            value={this.state.switchPhoneCaledar}
          />
        </View>
        <View style={[mainStyles.rowFlex, styles.itemView]}>
          <Text style={styles.itemText}>
            <FontawesomeIcon nameIcon={icons.Hand_Pointer_O} color={colors.black2} size={MeasurementCalculator.measureFont(16)} customStyle={{}} /> Enable Fingerprint
          </Text>
          <Switch
            style={{ backgroundColor: colors.white }}
            thumbColor={colors.white}
            trackColor={{ false: colors.gray2, true: colors.blue2 }}
            ios_backgroundColor={colors.gray2}
            onValueChange={this.toggleSwitchEnableFinger}
            value={this.state.switchEnableFinger}
          />
        </View>
        <View style={styles.LineBottom}>
          <TouchableOpacity style={[mainStyles.rowFlex, styles.itemViewNotClickable]} onPress={this.ChangePasswordAction}>
            <Text style={styles.itemText}>
              <FontawesomeIcon nameIcon={icons.Anlock_Alt} color={colors.black2} size={MeasurementCalculator.measureFont(18)} />   Change password
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.LineBottom}>
          <TouchableOpacity style={[mainStyles.rowFlex, styles.itemViewNotClickable]} onPress={this.ChangeEmailAction}>
            <Text style={styles.itemText}>
              <FontawesomeIcon nameIcon={icons.Envelope_O} color={colors.black2} size={MeasurementCalculator.measureFont(14)} />  Change email
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.LineBottom}>
          <TouchableOpacity style={[mainStyles.rowFlex, styles.itemViewNotClickable]} onPress={() => this.openAccountSettingsPopup()}>
            <Text style={styles.itemText}>
              <FontawesomeIcon nameIcon={icons.Cog} color={colors.black2} size={MeasurementCalculator.measureFont(16)} />  Account settings
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.LineBottom}>
          <TouchableOpacity style={[mainStyles.rowFlex, styles.itemViewNotClickable]} onPress={this.TermsAndConditionAction}>
            <Text style={styles.itemText}>
              <FontawesomeIcon nameIcon={icons.List} color={colors.black2} size={MeasurementCalculator.measureFont(15)} />  Terms & Conditions
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.LineBottom}>
          <TouchableOpacity style={[mainStyles.rowFlex, styles.itemViewNotClickable]} onPress={this.PrivacyPolicyAction}>
            <Text style={styles.itemText}>
              <FontawesomeIcon nameIcon={icons.Pencil_Square_O} color={colors.black2} size={MeasurementCalculator.measureFont(14)} />  Privacy policy
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={[mainStyles.rowFlex, styles.itemView, { borderBottomWidth: 0, marginTop: MeasurementCalculator.measure(20), marginBottom: MeasurementCalculator.measure(13), justifyContent: 'flex-start' }]} onPress={() => this.openLogoutPopup()}>
          <View style={{ transform: [{ rotate: '180deg' }] }}>
            <FontawesomeIcon nameIcon={icons.Sign_In} color={colors.black4} size={MeasurementCalculator.measureFont(16)} />
          </View>
          <Text style={[styles.itemText, { color: colors.lightRed, fontSize: MeasurementCalculator.measureFont(15) }]}>   Log Out</Text>
        </TouchableOpacity>
        <Modal
          isVisible={this.state.accountSettingsModal}
          animationIn={'slideInUp'}
          animationInTiming={500}
          animationOut={'slideOutDown'}
          animationOutTiming={700}
          backdropOpacity={.05}
          backdropColor={colors.black}
          style={{ justifyContent: 'center', alignItems: 'center' }}
        >
          <ConfirmationPopup
            backgroundColor={colors.yellow4}
            icon={icons.Question_Circle}
            title={'Account Settings'}
            oneButton={'Delete Account'}
            textBetweenButtons={'OR'}
            secondButton={'Export Account'}
            cancel={() => this.closeAccountSettingsPopup()}
          />
        </Modal>
        <Modal
          isVisible={this.state.LogoutModal}
          animationIn={'slideInUp'}
          animationInTiming={500}
          animationOut={'slideOutDown'}
          animationOutTiming={700}
          backdropOpacity={.05}
          backdropColor={colors.black}
          style={{ justifyContent: 'center', alignItems: 'center' }}
        >
          <ConfirmationPopup
            backgroundColor={colors.yellow5}
            icon={icons.Exclamation_Triangle}
            title={'Are you sure!'}
            description={'confirm logout confirm logout\nconfirm logout confirm logout..'}
            oneButton={'Logout'}
            cancel={() => this.closeLogoutPopup()}
          />
        </Modal>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
  },
  itemView: {
    paddingVertical: MeasurementCalculator.measure(11),
    alignItems: 'center',
    justifyContent: 'space-between',
    marginRight: MeasurementCalculator.measure(25),
    marginLeft: MeasurementCalculator.measure(25),
    borderBottomWidth: 0.5,
    borderBottomColor: colors.gray1,
    borderStyle: "solid",
    backgroundColor: colors.white
  },
  itemViewNotClickable: {
    paddingVertical: MeasurementCalculator.measure(5.5),
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemText: {
    paddingRight: 5,
    fontFamily: fonts.Karla_Regular,
    fontSize: MeasurementCalculator.measureFont(16),
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black2
  },
  LineBottom: {
    paddingVertical: MeasurementCalculator.measure(11),
    marginRight: MeasurementCalculator.measure(25),
    marginLeft: MeasurementCalculator.measure(25),
    borderBottomWidth: 0.5,
    borderBottomColor: colors.gray1,
    borderStyle: "solid",
  }
});
export default Account;
