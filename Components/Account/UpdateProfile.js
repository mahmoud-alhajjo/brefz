import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, Platform } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import OverlayButton from '../Helpers/OverlayButton';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';
import ConfirmationPopup from '../Popup/ConfirmationPopup';

import Modal from 'react-native-modal';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { closeOverlay, openOverlay } from 'react-native-blur-overlay';

class UpdateProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      updateProfileModal: false,
    };
  }

  closeUpdateProfilePopup = () => {
    this.setState({ updateProfileModal: false });
    closeOverlay();
  }

  openUpdateProfilePopup = () => {
    openOverlay();
    this.setState({ updateProfileModal: true });
  };

  render() {
    let textInputShadowiOS = Platform.OS === 'ios' ? styles.textInputShadowiOS : null
    return (
      <View style={mainStyles.flex}>
        <KeyboardAwareScrollView style={{ backgroundColor: colors.white }} contentContainerStyle={{ backgroundColor: colors.white }} bounces={true} enableResetScrollToCoords={true} scrollEnabled={true} enableOnAndroid={true}>
          <View style={styles.formStyle}>
            <View>
              <Text style={styles.textLabel}>First Name</Text>
              <TextInput
                ref="firstNameInput"
                style={[styles.inputStyle, textInputShadowiOS]}
                underlineColorAndroid={"transparent"}
                textAlignVertical={'center'}
                placeholder='First Name'
                placeholderTextColor={colors.placeholderColor}
                autoCapitalize={"none"}
                autoCorrect={false}
                maxLength={50}
                blurOnSubmit={false}
                returnKeyType={"next"}
                onChangeText={() => { }}
                value={this.state.firstName}
                onSubmitEditing={(event) => this.refs.lastNameInput.focus()}
              />
            </View>
            <View style={styles.marginTopFields}>
              <Text style={styles.textLabel}>Last Name</Text>
              <TextInput
                ref="lastNameInput"
                style={[styles.inputStyle, textInputShadowiOS]}
                underlineColorAndroid={"transparent"}
                textAlignVertical={'center'}
                placeholder='Last Name'
                placeholderTextColor={colors.placeholderColor}
                autoCapitalize={"none"}
                autoCorrect={false}
                maxLength={50}
                blurOnSubmit={false}
                returnKeyType={"next"}
                onChangeText={() => { }}
                value={this.state.lastName}
                onSubmitEditing={(event) => this.refs.mobileInput.focus()}
              />
            </View>
            <View style={styles.marginTopFields}>
              <Text style={styles.textLabel}>Mobile</Text>
              <TextInput
                ref="mobileInput"
                style={[styles.inputStyle, textInputShadowiOS]}
                underlineColorAndroid={"transparent"}
                placeholder='Number'
                placeholderTextColor={colors.placeholderColor}
                textAlignVertical={'center'}
                keyboardType={"phone-pad"}
                maxLength={10}
                blurOnSubmit={false}
                returnKeyType={"next"}
                onChangeText={() => { }}
                value={this.state.mobile}
                onSubmitEditing={(event) => this.refs.addressInput.focus()}
              />
            </View>
            <View style={styles.marginTopFields}>
              <Text style={styles.textLabel}>Address</Text>
              <TextInput
                ref="addressInput"
                style={[styles.inputStyle, textInputShadowiOS, { height: 161.33, paddingTop: 18.2 }]}
                placeholder={"Enter Your Address"}
                textAlignVertical={'top'}
                underlineColorAndroid={"transparent"}
                placeholderTextColor={colors.placeholderColor}
                multiline={true}
                numberOfLines={4}
                autoCapitalize={"none"}
                autoCorrect={false}
                blurOnSubmit={false}
                value={this.state.address}
                onSubmitEditing={(event) => this.refs.addressInput.blur()}
              />
            </View>
          </View>
          <View style={styles.buttonView}>
            <OverlayButton
              buttonBackgroundColor={colors.lightGreen}
              buttonTextColor={colors.white}
              clickButton={() => this.openUpdateProfilePopup()}
              style={{ flex: 1 }}
              text={"Update Profile"}
              customStyle={styles.updateProfilButton}
              textButtonStyle={styles.TextButton} />
          </View>
        </KeyboardAwareScrollView>
        <Modal
          isVisible={this.state.updateProfileModal}
          animationIn={'slideInUp'}
          animationInTiming={500}
          animationOut={'slideOutDown'}
          animationOutTiming={700}
          backdropOpacity={.05}
          backdropColor={colors.black}
          style={{ justifyContent: 'center', alignItems: 'center' }}
        >
          <ConfirmationPopup
            backgroundColor={colors.lightGreen2}
            icon={icons.Check}
            title={'Profile Updated'}
            oneButton={'Back To Account'}
            cancel={() => this.closeUpdateProfilePopup()}
          />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  formStyle: {
    marginTop: MeasurementCalculator.measure(42.1),
    paddingHorizontal: MeasurementCalculator.measure(26, 'w'),
  },
  textLabel: {
    fontSize: MeasurementCalculator.measureFont(21),
    fontFamily: fonts.Montserrat_Regular,
    color: colors.black2,
  },
  marginTopFields: {
    marginTop: MeasurementCalculator.measure(23.6)
  },
  inputStyle: {
    backgroundColor: colors.white,
    marginTop: MeasurementCalculator.measure(11.5),
    paddingLeft: 20.3,
    height: 50,
    fontSize: 16,
    fontFamily: fonts.Karla_Regular,
    color: colors.black,
    borderColor: colors.black_5,
    borderRadius: 8,
    elevation: 3
  },
  textInputShadowiOS: {
    borderWidth: 1,
    borderColor: colors.black_5,
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.05,
    shadowRadius: 14
  },
  buttonView: {
    marginLeft: MeasurementCalculator.measure(26, 'w'),
    marginTop: MeasurementCalculator.measure(32.7),
    marginBottom: MeasurementCalculator.measure(54.5)
  },
  updateProfilButton: {
    height: MeasurementCalculator.measure(68),
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: MeasurementCalculator.measure(37),
  },
  TextButton: {
    fontFamily: fonts.Montserrat_SemiBold,
    fontSize: MeasurementCalculator.measureFont(20),
    letterSpacing: 0,
  },
});

export default UpdateProfile;
