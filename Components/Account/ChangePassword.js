import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import OverlayButton from '../Helpers/OverlayButton';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        let textInputShadowiOS = Platform.OS === 'ios' ? styles.textInputShadowiOS : null
        return (
            <View style={mainStyles.flex}>
                <KeyboardAwareScrollView style={{ backgroundColor: colors.white }} contentContainerStyle={{ backgroundColor: colors.white }} bounces={true} enableResetScrollToCoords={true} scrollEnabled={true} enableOnAndroid={true}>
                    <View style={styles.formStyle}>
                        <View>
                            <Text style={styles.textLabel}>Old Password</Text>
                            <TextInput
                                ref="oldPasswordInput"
                                style={[styles.inputStyle, textInputShadowiOS]}
                                underlineColorAndroid={"transparent"}
                                textAlignVertical={'center'}
                                placeholder='Old password'
                                placeholderTextColor={colors.placeholderColor}
                                autoCapitalize={"none"}
                                autoCorrect={false}
                                maxLength={50}
                                blurOnSubmit={false}
                                secureTextEntry={true}
                                returnKeyType={"next"}
                                value={this.state.oldPassword}
                                onChangeText={() => { }}
                                value={this.state.oldPassword}
                                onSubmitEditing={(event) => this.refs.newPasswordInput.focus()}
                            />
                        </View>
                        <View style={{ marginTop: MeasurementCalculator.measure(23.6) }}>
                            <Text style={styles.textLabel}>New Password</Text>
                            <TextInput
                                ref="newPasswordInput"
                                style={[styles.inputStyle, textInputShadowiOS]}
                                underlineColorAndroid={"transparent"}
                                textAlignVertical={'center'}
                                placeholder='New password'
                                placeholderTextColor={colors.placeholderColor}
                                autoCapitalize={"none"}
                                autoCorrect={false}
                                maxLength={50}
                                blurOnSubmit={false}
                                secureTextEntry={true}
                                returnKeyType={"next"}
                                onChangeText={() => { }}
                                value={this.state.newPassword}
                                onSubmitEditing={(event) => this.refs.repeatPasswordInput.focus()}
                            />
                        </View>
                        <View style={{ marginTop: MeasurementCalculator.measure(23.6) }}>
                            <Text style={styles.textLabel}>Repeat Password</Text>
                            <TextInput
                                ref="repeatPasswordInput"
                                style={[styles.inputStyle, textInputShadowiOS]}
                                underlineColorAndroid={"transparent"}
                                textAlignVertical={'center'}
                                placeholder='Repeat password'
                                placeholderTextColor={colors.placeholderColor}
                                autoCapitalize={"none"}
                                autoCorrect={false}
                                maxLength={50}
                                blurOnSubmit={false}
                                secureTextEntry={true}
                                returnKeyType={"done"}
                                onChangeText={() => { }}
                                value={this.state.repeatPassword}
                                onSubmitEditing={(event) => this.refs.repeatPasswordInput.blur()}
                            />
                        </View>
                    </View>
                    <View style={styles.buttonView}>
                        <OverlayButton
                            buttonBackgroundColor={colors.lightGreen}
                            buttonTextColor={colors.white}
                            clickButton={() => { }}
                            style={{ flex: 1 }}
                            text={"Update Password"}
                            customStyle={styles.updatePasswordButton}
                            textButtonStyle={styles.TextButton} />
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    formStyle: {
        marginTop: MeasurementCalculator.measure(42.1),
        paddingHorizontal: MeasurementCalculator.measure(26, 'w'),
    },
    textLabel: {
        fontSize: MeasurementCalculator.measureFont(21),
        fontFamily: fonts.Montserrat_Regular,
        color: colors.black2,
    },
    inputStyle: {
        backgroundColor: colors.white,
        marginTop: MeasurementCalculator.measure(11.5),
        paddingLeft: 20.3,
        height: 50,
        fontSize: 16,
        fontFamily: fonts.Karla_Regular,
        color: colors.black,
        borderColor: colors.black_5,
        borderRadius: 8,
        elevation: 3,
    },
    textInputShadowiOS: {
        borderWidth: 1,
        borderColor: colors.black_5,
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.05,
        shadowRadius: 14
    },
    buttonView: {
        marginLeft: MeasurementCalculator.measure(26, 'w'),
        marginTop: MeasurementCalculator.measure(64.5),
        marginBottom: MeasurementCalculator.measure(30)
    },
    updatePasswordButton: {
        height: MeasurementCalculator.measure(68),
        borderTopLeftRadius: 50,
        borderBottomLeftRadius: 50,
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: MeasurementCalculator.measure(37, 'w'),
    },
    TextButton: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: MeasurementCalculator.measureFont(20),
        letterSpacing: 0,
    },
});

export default ChangePassword;
