import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import OverlayButton from '../Helpers/OverlayButton';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator'
import ConfirmationPopup from '../Popup/ConfirmationPopup';

import Modal from 'react-native-modal';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { closeOverlay, openOverlay } from 'react-native-blur-overlay';


class ChangeEmail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmModal: false,
        };
    }

    closeConfirmPopup = () => {
        this.setState({ confirmModal: false })
        closeOverlay();
    }

    openConfirmPopup = () => {
        openOverlay();
        this.setState({ confirmModal: true });
    };

    render() {
        let textInputShadowiOS = Platform.OS === 'ios' ? styles.textInputShadowiOS : null
        return (
            <View style={mainStyles.flex}>
                <KeyboardAwareScrollView style={{ backgroundColor: colors.white }} contentContainerStyle={{ backgroundColor: colors.white }} bounces={true} enableResetScrollToCoords={true} scrollEnabled={true} enableOnAndroid={true}>
                    <View style={styles.formStyle}>
                        <View>
                            <Text style={[styles.textLabel, { fontSize: MeasurementCalculator.measureFont(15) }]}>Your Current Email</Text>
                            <Text style={styles.currentEmail}>Brefz@Brefz.app</Text>
                            <Text style={styles.confirmEmail}>
                                Waiting for confirmation with this Email:{'\n'}
                                <Text style={{ fontFamily: fonts.Montserrat_ExtraBold }}>Hello@Brefz.app</Text>
                            </Text>
                        </View>
                        <View style={{ marginTop: MeasurementCalculator.measure(31.1) }}>
                            <Text style={styles.textLabel}>Your New Email</Text>
                            <TextInput
                                ref='email'
                                style={[styles.inputStyle, textInputShadowiOS]}
                                underlineColorAndroid={"transparent"}
                                textAlignVertical={'center'}
                                placeholder='Enter Your New Email'
                                placeholderTextColor={colors.placeholderColor}
                                textContentType='emailAddress'
                                keyboardType={"email-address"}
                                autoCapitalize={"none"}
                                maxLength={50}
                                blurOnSubmit={false}
                                returnKeyType={"done"}
                                onChangeText={() => { }}
                                value={this.state.email}
                                onSubmitEditing={(event) => this.refs.email.blur()}
                            />
                        </View>
                    </View>
                    <View style={styles.buttonView}>
                        <OverlayButton
                            buttonBackgroundColor={colors.lightGreen}
                            buttonTextColor={colors.white}
                            clickButton={() => this.openConfirmPopup()}
                            style={{ flex: 1 }}
                            text={"Update Email"}
                            customStyle={styles.updateEmailButton}
                            textButtonStyle={styles.TextButton} />
                    </View>
                </KeyboardAwareScrollView>
                <Modal
                    isVisible={this.state.confirmModal}
                    animationIn={'slideInUp'}
                    animationInTiming={500}
                    animationOut={'slideOutDown'}
                    animationOutTiming={700}
                    backdropOpacity={.05}
                    backdropColor={colors.black}
                    style={{ justifyContent: 'center', alignItems: 'center' }}
                >
                    <ConfirmationPopup
                        backgroundColor={colors.yellow3}
                        icon={icons.Refresh}
                        title={'Confirm Your Email'}
                        description={'After you confirm your email\nyou will see it as your current Email'}
                        oneButton={'Back To Account'}
                        cancel={() => this.closeConfirmPopup()}
                    />
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    formStyle: {
        marginTop: MeasurementCalculator.measure(27.1),
        paddingHorizontal: MeasurementCalculator.measure(26, 'w'),
    },
    textLabel: {
        fontSize: MeasurementCalculator.measureFont(21),
        fontFamily: fonts.Montserrat_Regular,
        color: colors.black2,
    },
    currentEmail: {
        fontSize: MeasurementCalculator.measureFont(16),
        fontFamily: fonts.Montserrat_Light,
        color: colors.white,
        backgroundColor: colors.lightBlue,
        paddingLeft: MeasurementCalculator.measure(23.3, 'w'),
        paddingVertical: MeasurementCalculator.measure(15),
        marginTop: MeasurementCalculator.measure(12.5),
        borderRadius: 6,
        letterSpacing: 0.7,
    },
    confirmEmail: {
        fontSize: MeasurementCalculator.measureFont(14),
        fontFamily: fonts.Montserrat_Regular,
        color: colors.black4,
        backgroundColor: colors.lightRed1,
        paddingLeft: MeasurementCalculator.measure(12, 'w'),
        paddingVertical: MeasurementCalculator.measure(9.5),
        marginTop: MeasurementCalculator.measure(13.5),
        lineHeight: 22
    },
    inputStyle: {
        backgroundColor: colors.white,
        marginTop: MeasurementCalculator.measure(11.5),
        paddingLeft: 20.3,
        height: 50,
        fontSize: 16,
        fontFamily: fonts.Roboto_Regular,
        color: colors.black,
        borderColor: colors.black_5,
        borderRadius: 8,
        elevation: 3
    },
    textInputShadowiOS: {
        borderWidth: 1,
        borderColor: colors.black_5,
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.05,
        shadowRadius: 14
    },
    buttonView: {
        marginLeft: MeasurementCalculator.measure(26, 'w'),
        marginTop: MeasurementCalculator.measure(54.5),
        marginBottom: MeasurementCalculator.measure(30)
    },
    updateEmailButton: {
        height: MeasurementCalculator.measure(68),
        borderTopLeftRadius: 50,
        borderBottomLeftRadius: 50,
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: MeasurementCalculator.measure(37, 'w'),
    },
    TextButton: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: MeasurementCalculator.measureFont(20),
        letterSpacing: 0,
    },
});

export default ChangeEmail;
