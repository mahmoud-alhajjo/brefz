import React, { Component } from 'react';
import { ScrollView, Text, StyleSheet } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

import FontawesomeIcon from '../../css/FontawesomeIcon';

class TermsAndCondition extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <ScrollView style={[mainStyles.flex, styles.container]}>
                <Text style={styles.title}>Privacy</Text>
                <Text style={styles.subtitle}>
                    <FontawesomeIcon nameIcon={icons.Point} color={colors.black2} size={MeasurementCalculator.measureFont(17)} />  Term here as lorem for item... {'\n'}
                    <FontawesomeIcon nameIcon={icons.Point} color={colors.black2} size={MeasurementCalculator.measureFont(17)} />  Term here as lorem for item... {'\n'}
                    <FontawesomeIcon nameIcon={icons.Point} color={colors.black2} size={MeasurementCalculator.measureFont(17)} />  Term here as lorem for item... {'\n'}
                    <FontawesomeIcon nameIcon={icons.Point} color={colors.black2} size={MeasurementCalculator.measureFont(17)} />  Term here as lorem for item... {'\n'}
                    <FontawesomeIcon nameIcon={icons.Point} color={colors.black2} size={MeasurementCalculator.measureFont(17)} />  Term here as lorem for item... {'\n'}
                    <FontawesomeIcon nameIcon={icons.Point} color={colors.black2} size={MeasurementCalculator.measureFont(17)} />  Term here as lorem for item... {'\n'}
                </Text>
                <Text style={[styles.title, {marginTop: MeasurementCalculator.measure(15)}]}>Title here</Text>
                <Text style={styles.subtitle}>
                    <FontawesomeIcon nameIcon={icons.Point} color={colors.black2} size={MeasurementCalculator.measureFont(17)} />  Term here as lorem for item... {'\n'}
                    <FontawesomeIcon nameIcon={icons.Point} color={colors.black2} size={MeasurementCalculator.measureFont(17)} />  Term here as lorem for item... {'\n'}
                    <FontawesomeIcon nameIcon={icons.Point} color={colors.black2} size={MeasurementCalculator.measureFont(17)} />  Term here as lorem for item... {'\n'}
                    <FontawesomeIcon nameIcon={icons.Point} color={colors.black2} size={MeasurementCalculator.measureFont(17)} />  Term here as lorem for item... {'\n'}
                    <FontawesomeIcon nameIcon={icons.Point} color={colors.black2} size={MeasurementCalculator.measureFont(17)} />  Term here as lorem for item... {'\n'}
                    <FontawesomeIcon nameIcon={icons.Point} color={colors.black2} size={MeasurementCalculator.measureFont(17)} />  Term here as lorem for item... {'\n'}
                    <FontawesomeIcon nameIcon={icons.Point} color={colors.black2} size={MeasurementCalculator.measureFont(17)} />  Term here as lorem for item... {'\n'}
                </Text>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        marginTop: MeasurementCalculator.measure(30),
        marginBottom: MeasurementCalculator.measure(12),
    },
    title: {
        fontSize: MeasurementCalculator.measureFont(18),
        fontFamily: fonts.Montserrat_Regular,
        color: colors.black2,
        marginLeft: MeasurementCalculator.measure(27, 'w')
    },
    subtitle: {
        fontSize: MeasurementCalculator.measureFont(15),
        fontFamily: fonts.Montserrat_Regular,
        marginTop: MeasurementCalculator.measure(19),
        marginHorizontal: MeasurementCalculator.measure(25, 'w'),
        paddingHorizontal: MeasurementCalculator.measure(20, 'w'),
        paddingVertical: MeasurementCalculator.measure(21),
        color: colors.black4,
        backgroundColor: colors.lightRed1,
        lineHeight: 22
    },
});

export default TermsAndCondition;
