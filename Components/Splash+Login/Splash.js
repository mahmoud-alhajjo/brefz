import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, SafeAreaView } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import CustomStatusBar from '../Main Structure/CustomStatusBar'

import * as Animatable from 'react-native-animatable';

class Splash extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.navigate('OnBoarding');
        }, 2500)
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
              <CustomStatusBar isDark routeName={this.props.navigation.state.routeName}/>
                <Animatable.Image
                    style={styles.imageStyle}
                    source={require('../../Images/logo.png')}
                    resizeMode='contain'
                    animation="fadeInDownBig"
                    easing="linear"
                    duration={700}
                />
                <View style={{ flexDirection: 'row' }}>
                    <Animatable.Text animation="bounceIn" easing="linear" duratio={800} delay={700} style={[styles.textStyle, {}]}>B</Animatable.Text>
                    <Animatable.Text animation="bounceIn" easing="linear" duratio={900} delay={800} style={[styles.textStyle, {}]}>R</Animatable.Text>
                    <Animatable.Text animation="bounceIn" easing="linear" duratio={1000} delay={900} style={[styles.textStyle, {}]}>E</Animatable.Text>
                    <Animatable.Text animation="bounceIn" easing="linear" duratio={1100} delay={1000} style={[styles.textStyle, {}]}>F</Animatable.Text>
                    <Animatable.Text animation="bounceIn" easing="linear" duratio={1200} delay={1100} style={[styles.textStyle, {}]}>Z</Animatable.Text>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.white,
    },
    imageStyle: {
        width: 128,
        height: 120,
        alignSelf: 'center',
        marginBottom: 19,
    },
    textStyle: {
        fontSize: 50,
        fontWeight: "500",
        color: colors.darkBlue,
        fontFamily: fonts.Montserrat_SemiBold,
        letterSpacing: 15
    }
});

export default Splash;
