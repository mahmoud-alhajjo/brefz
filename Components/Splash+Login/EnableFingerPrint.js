import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, Image, TouchableOpacity } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import OverlayButton from '../Helpers/OverlayButton';
import CustomStatusBar from '../Main Structure/CustomStatusBar'

import MeasurementCalculator from '../Helpers/MeasurementCalculator'

class EnableFingerPrint extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    goToMainApp = () => {
        this.props.navigation.navigate('AppDashboard');
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
              <CustomStatusBar isDark routeName={this.props.navigation.state.routeName}/>
                <View style={{ flex: 1 }}>
                    <Image style={styles.imageStyle1} source={require('../../Images/solo.png')} resizeMode='contain' />
                    <Image style={styles.imageStyle2} source={require('../../Images/FingerPrint.png')} resizeMode='contain' />
                    <Text style={styles.textStyle}>Enable Fingerprint</Text>
                    <Text style={styles.subtitle}>If you enable touch ID, you don’t {'\n'}need to enter your password when {'\n'}you login.</Text>
                    <View style={styles.bottomView}>
                        <OverlayButton
                            buttonBackgroundColor={colors.lightGreen}
                            buttonTextColor={colors.white}
                            clickButton={this.goToMainApp}
                            style={{ flex: 1 }}
                            text={"Enable"}
                            customStyle={styles.LoginButton}
                            textButtonStyle={styles.LoginTextButton} />
                    </View>
                    <TouchableOpacity onPress={this.goToMainApp}>
                        <Text style={styles.textforgotPassword}>Not Now</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    imageStyle1: {
        height: MeasurementCalculator.measure(132),
        width: MeasurementCalculator.measure(139, 'w'),
        marginTop: MeasurementCalculator.measure(59),
        alignSelf: "center"
    },
    imageStyle2: {
        height: MeasurementCalculator.measure(138.11),
        width: MeasurementCalculator.measure(124.27, 'w'),
        marginTop: MeasurementCalculator.measure(41.9),
        alignSelf: "center"
    },
    textStyle: {
        fontFamily: fonts.Karla_Bold,
        fontSize: MeasurementCalculator.measureFont(27),
        color: colors.black2,
        textAlign: 'center',
        marginTop: MeasurementCalculator.measure(37.1),
    },
    subtitle: {
        fontFamily: fonts.Helvetica_Neue,
        fontSize: MeasurementCalculator.measureFont(18),
        color: colors.gray,
        textAlign: 'center',
        marginTop: MeasurementCalculator.measure(16),
    },
    bottomView: {
        marginLeft: MeasurementCalculator.measure(24, 'w'),
        marginTop: MeasurementCalculator.measure(28),
    },
    LoginButton: {
        height: MeasurementCalculator.measure(68),
        borderTopLeftRadius: 50,
        borderBottomLeftRadius: 50,
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: MeasurementCalculator.measure(37, 'w'),
    },
    LoginTextButton: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: MeasurementCalculator.measureFont(20),
        letterSpacing: 0,
    },
    textforgotPassword: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: 12,
        color: colors.purple,
        marginLeft: MeasurementCalculator.measure(24, 'w'),
        marginTop: MeasurementCalculator.measure(35),
    }
});

export default EnableFingerPrint;
