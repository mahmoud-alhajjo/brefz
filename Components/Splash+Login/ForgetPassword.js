import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput, TouchableOpacity } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import OverlayButton from '../Helpers/OverlayButton';
import MeasurementCalculator from '../Helpers/MeasurementCalculator'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class ForgetPassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: ''
        };
    }

    goBackLogin = () => {
        this.props.navigation.navigate('Login');
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <KeyboardAwareScrollView style={{ backgroundColor: colors.white }} contentContainerStyle={{ backgroundColor: colors.white }} bounces={false} enableResetScrollToCoords={true} scrollEnabled={true} enableOnAndroid={true}>
                    <View style={styles.formStyle}>
                        <TextInput
                            ref='email'
                            style={styles.inputStyle}
                            underlineColorAndroid={"transparent"}
                            textAlignVertical={'bottom'}
                            placeholder='Email'
                            placeholderTextColor={colors.placeholderColor}
                            textContentType='emailAddress'
                            keyboardType={"email-address"}
                            autoCapitalize={"none"}
                            returnKeyType={"done"}
                            blurOnSubmit={false}
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email}
                            onSubmitEditing={(event) => this.refs.email.blur()}
                        />
                    </View>
                    <View style={styles.bottomView}>
                        <OverlayButton
                            buttonBackgroundColor={colors.lightBlue}
                            buttonTextColor={colors.white}
                            clickButton={this.goBackLogin}
                            style={{ flex: 1 }}
                            text={"SUBMIT"}
                            customStyle={styles.LoginButton}
                            textButtonStyle={styles.LoginTextButton} />
                    </View>
                    <View style={{ flexDirection: 'row', marginLeft: MeasurementCalculator.measure(24, 'w'), marginTop: MeasurementCalculator.measure(45) }}>
                        <Text style={styles.textforgotPassword}>Problem?  </Text>
                        <TouchableOpacity onPress={() => { }}>
                            <Text style={[styles.textforgotPassword, { textDecorationLine: 'underline' }]}>CONTACT US</Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAwareScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    formStyle: {
        marginTop: MeasurementCalculator.measure(36),
        paddingHorizontal: MeasurementCalculator.measure(24, 'w'),
    },
    inputStyle: {
        height: 55,
        fontSize: 16,
        fontFamily: fonts.Montserrat_Medium,
        color: colors.black,
        borderBottomWidth: 1,
        borderBottomColor: colors.borderGrey,
        paddingBottom: Platform.OS === 'ios' ? 0 : 11,
        paddingLeft: 0,
    },
    bottomView: {
        marginLeft: MeasurementCalculator.measure(24, 'w'),
        marginTop: MeasurementCalculator.measure(41),
    },
    LoginButton: {
        height: MeasurementCalculator.measure(68),
        borderTopLeftRadius: 50,
        borderBottomLeftRadius: 50,
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: MeasurementCalculator.measure(37, 'w'),
    },
    LoginTextButton: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: MeasurementCalculator.measureFont(20),
        letterSpacing: 0,
    },
    textforgotPassword: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: 12,
        color: colors.purple,
    }
});

export default ForgetPassword;
