import React, { Component } from 'react';
import { View, Text, SafeAreaView, StyleSheet, Image, TextInput, TouchableOpacity, Platform } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import OverlayButton from '../Helpers/OverlayButton';
import MeasurementCalculator from '../Helpers/MeasurementCalculator'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
        };
    }

    forgetPassword = () => {
        this.props.navigation.navigate('ForgetPassword');
    }

    login = () => {
        this.props.navigation.navigate('NotificationMessage');
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <KeyboardAwareScrollView style={{ backgroundColor: colors.white }} contentContainerStyle={{ backgroundColor: colors.white }} bounces={false} enableResetScrollToCoords={true} scrollEnabled={false} enableOnAndroid={true}>
                    <View style={styles.formStyle}>
                        <TextInput
                            ref='email'
                            style={styles.inputStyle}
                            underlineColorAndroid={"transparent"}
                            textAlignVertical={'bottom'}
                            placeholder='Email'
                            placeholderTextColor={colors.placeholderColor}
                            textContentType='emailAddress'
                            keyboardType={"email-address"}
                            autoCapitalize={"none"}
                            blurOnSubmit={false}
                            returnKeyType={"next"}
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email}
                            onSubmitEditing={(event) => this.refs.password.focus()}
                        />
                        <TextInput
                            style={[styles.inputStyle, { marginTop: MeasurementCalculator.measure(12) }]}
                            ref='password'
                            underlineColorAndroid={"transparent"}
                            textAlignVertical={'bottom'}
                            placeholder="Password"
                            placeholderTextColor={colors.placeholderColor}
                            textContentType='password'
                            secureTextEntry={true}
                            autoCapitalize={"none"}
                            returnKeyType={"done"}
                            onChangeText={(password) => this.setState({ password })}
                            value={this.state.password}
                            onSubmitEditing={(event) => this.refs.password.blur()}
                        />
                    </View>
                    <View style={styles.bottomView}>
                        <OverlayButton
                            buttonBackgroundColor={colors.lightGreen}
                            buttonTextColor={colors.white}
                            clickButton={this.login}
                            style={{ flex: 1 }}
                            text={"Login"}
                            customStyle={styles.LoginButton}
                            textButtonStyle={styles.LoginTextButton} />
                    </View>
                    <TouchableOpacity onPress={this.forgetPassword}>
                        <Text style={styles.textforgotPassword}>FORGOT PASSWORD</Text>
                    </TouchableOpacity>
                </KeyboardAwareScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    formStyle: {
        marginTop: MeasurementCalculator.measure(108),
        paddingHorizontal: MeasurementCalculator.measure(24, 'w'),
    },
    inputStyle: {
        height: 55,
        fontSize: 16,
        fontFamily: fonts.Montserrat_Medium,
        color: colors.black,
        borderBottomWidth: 1,
        borderBottomColor: colors.borderGrey,
        paddingBottom: Platform.OS === 'ios' ? 0 : 11,
        paddingLeft: 0,
    },
    bottomView: {
        marginLeft: MeasurementCalculator.measure(24, 'w'),
        marginTop: MeasurementCalculator.measure(45),
    },
    LoginButton: {
        height: MeasurementCalculator.measure(68),
        borderTopLeftRadius: 50,
        borderBottomLeftRadius: 50,
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: MeasurementCalculator.measure(37, 'w'),
    },
    LoginTextButton: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: MeasurementCalculator.measureFont(20),
        letterSpacing: 0,
    },
    textforgotPassword: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: 12,
        color: colors.purple,
        marginLeft: MeasurementCalculator.measure(24, 'w'),
        marginTop: MeasurementCalculator.measure(55),
    }
});

export default Login;
