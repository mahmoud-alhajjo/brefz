import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  SafeAreaView,
} from 'react-native';

import colors from '../../css/colors';
import mainStyles from '../../css/MainStyle';
import * as fonts from '../../css/fonts';
import OverlayButton from '../Helpers/OverlayButton'
import MeasurementCalculator from '../Helpers/MeasurementCalculator'
import CustomStatusBar from '../Main Structure/CustomStatusBar'

import Swiper from 'react-native-swiper';

class OnBoarding extends Component {

  constructor() {
    super();
    this.state = {

    }
  }

  skipLogin = () => {
    this.props.navigation.navigate('Login');
  }

  renderPagination = (index, total) => {
    var dots = []
    for (var i = 0; i < total; i++) {
      dots.push(<View key={i} style={{ width: 17.61, height: 17.61, backgroundColor: i == index ? colors.lightGreen : colors.lightGrey, borderRadius: 8.805, marginRight: (i == (total - 1)) ? null : 17.6 }} />)
    }
    return (
      <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 50, justifyContent: 'flex-end', alignItems: 'center'}}>
        <View style={mainStyles.rowFlex}>
          {dots}
        </View>
      </View>
    )
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={mainStyles.flex}>
          <CustomStatusBar isDark routeName={this.props.navigation.state.routeName}/>
          <Swiper loop={false} showsButtons={false} showsPagination={true} scrollEnabled={true} renderPagination={(index, total, context) => this.renderPagination(index, total)} >
            <View style={{flex: 1 }}>
              <View style={{paddingHorizontal: 5, marginTop: MeasurementCalculator.measure(64) }}>
                <Text style={styles.helloText}>
                  Hello, We Are
                </Text>
                <Text style={styles.brefzText}>
                  BREFZ
                </Text>
              </View>
              <Text style={styles.doneText}>
                Ready to get stuff done?
              </Text>
              <Image style={styles.image1} source={require('../../Images/onBoarding_1.png')} resizeMode='contain' />
            </View>
            <View style={{ flex: 1 }}>
              <Image style={styles.image2} source={require('../../Images/onBoarding_2.png')} resizeMode='contain' />
              <View style={{paddingHorizontal: 5, marginTop: MeasurementCalculator.measure(81.5) }}>
                <Text style={styles.helloText}>
                  Get Organized
                </Text>
                <View style={{paddingHorizontal: 5, flexDirection: 'row', justifyContent: 'center' }}>
                  <Text style={[styles.textonBoardingTow, { color: colors.lightGreen }]}>Anything</Text>
                  <Text style={[styles.textonBoardingTow, { color: colors.black1 }]}>,</Text>
                  <Text style={[styles.textonBoardingTow, { color: colors.blue }]}> Anywhere</Text>
                </View>
              </View>
            </View>
            <View style={{ flex: 1 }}>
              <Image style={styles.image3} source={require('../../Images/onBoarding_3.png')} resizeMode='contain' />
              <View style={{paddingHorizontal: 5, marginTop: MeasurementCalculator.measure(68.7) }}>
                <Text style={styles.helloText}>Add Details..</Text>
                <Text style={styles.helloText}>Pictures.. Checklist..{'\n'}</Text>
                <Text style={[styles.helloText, { fontFamily: fonts.Montserrat_Light }]}>And More..</Text>
              </View>
            </View>
          </Swiper>
        </View>
        <View style={styles.bottomView}>
          <OverlayButton
            buttonBackgroundColor={colors.lightBlue}
            buttonTextColor={colors.white}
            clickButton={this.skipLogin}
            style={mainStyles.flex}
            text={"Skip To Login"}
            customStyle={styles.skipLoginButton}
            textButtonStyle={styles.skipLoginText} />
        </View>
      </SafeAreaView>
    );
  }
}


export default OnBoarding;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  bottomView: {
    marginLeft: MeasurementCalculator.measure(53, 'w'),
    marginBottom: MeasurementCalculator.measure(24),
    marginTop: MeasurementCalculator.measure(57.4),
  },
  skipLoginButton: {
    height: MeasurementCalculator.measure(68),
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: MeasurementCalculator.measure(66, 'w'),
  },
  skipLoginText: {
    fontFamily: fonts.Montserrat_SemiBold,
    fontSize: MeasurementCalculator.measureFont(20),
    letterSpacing: 0,
    textAlign: "center",
  },
  helloText: {
    fontFamily: fonts.Montserrat_Bold,
    fontSize: MeasurementCalculator.measureFont(24),
    textAlign: "center",
    color: colors.black1
  },
  brefzText: {
    fontFamily: fonts.Montserrat_SemiBold,
    fontSize: MeasurementCalculator.measureFont(27),
    letterSpacing: 2,
    textAlign: "center",
    color: colors.black1
  },
  doneText: {
    paddingHorizontal: 5,
    marginTop: MeasurementCalculator.measure(23.2),
    fontFamily: fonts.Arial,
    fontSize: MeasurementCalculator.measureFont(19),
    textAlign: "center",
    color: colors.black1_5
  },
  image1: {
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: MeasurementCalculator.measure(69.8),
    width: MeasurementCalculator.measure(313.93, 'w'),
    height: MeasurementCalculator.measure(229)
  },
  image2: {
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: MeasurementCalculator.measure(120.1),
    width: MeasurementCalculator.measure(260.68, 'w'),
    height: MeasurementCalculator.measure(215.38)
  },
  textonBoardingTow: {
    fontSize: MeasurementCalculator.measureFont(24),
    fontFamily: fonts.Montserrat_SemiBold
  },
  image3: {
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: MeasurementCalculator.measure(107),
    width: MeasurementCalculator.measure(222.07, 'w'),
    height: MeasurementCalculator.measure(222.38)
  },
});
