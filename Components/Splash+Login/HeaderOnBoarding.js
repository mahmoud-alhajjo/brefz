import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, SafeAreaView } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import MeasurementCalculator from '../Helpers/MeasurementCalculator'
import CustomStatusBar from '../Main Structure/CustomStatusBar'

class HeaderOnBoarding extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    getScreeTitleAndBackground = () => {
        let obj = {screenName: "", color: null, routeName: ""}
        let routeName = this.props.navigation.state.routeName
        obj.routeName = routeName
        if (routeName == "Login") {
          obj.title = "Login"
          obj.color = colors.lightBlue
        } else if (routeName == "ForgetPassword") {
          obj.title = "Forget?"
          obj.color = colors.yellow
        }
        return obj
    }

    render() {
      let screenOptions = this.getScreeTitleAndBackground();
      let bottomView = null
      if(screenOptions.routeName == 'Login') {
        bottomView = <Image style={styles.imageStyle} source={require('../../Images/solo.png')} resizeMode='contain' />
      } else if (screenOptions.routeName == 'ForgetPassword') {
        bottomView = <Text style={styles.subtitle}>Please enter your email below to recevie {'\n'}your password reset instructions</Text>
      }
      return (
          <SafeAreaView style={{borderBottomLeftRadius: 80, backgroundColor: screenOptions.color }}>
            <View style={styles.backgroundView} />
            <CustomStatusBar routeName={this.props.navigation.state.routeName}/>
            <View style={[styles.header, {backgroundColor: screenOptions.color}]}>
              <Text style={styles.title}>{screenOptions.title}</Text>
              {bottomView}
            </View>
          </SafeAreaView>
      );
    }
}
const styles = StyleSheet.create({
    header: {
        height: MeasurementCalculator.measure(185),
        borderBottomLeftRadius: 80,
    },
    title: {
        textAlign: 'center',
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: MeasurementCalculator.measureFont(25),
        color: colors.white,
        marginTop: MeasurementCalculator.measure(36)
    },
    imageStyle: {
        height: MeasurementCalculator.measure(132),
        width: MeasurementCalculator.measure(139, 'w'),
        marginTop: MeasurementCalculator.measure(53),
        alignSelf: "center"
    },
    subtitle: {
        textAlign: 'center',
        fontFamily: fonts.Montserrat_Regular,
        fontSize: MeasurementCalculator.measureFont(16),
        color: colors.white,
        marginTop: MeasurementCalculator.measure(27.5)
    },
    backgroundView: {
      position: 'absolute',
      height: 60,
      left:0,
      right:0,
      bottom: 0,
      backgroundColor: colors.white,
      zIndex: -1
    }
});

export default HeaderOnBoarding;
