import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, Image, TouchableOpacity } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import OverlayButton from '../Helpers/OverlayButton';
import CustomStatusBar from '../Main Structure/CustomStatusBar'

import MeasurementCalculator from '../Helpers/MeasurementCalculator'

class NotificationMessage extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    goToEnableFinger = () => {
        this.props.navigation.navigate('EnableFingerPrint');
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
              <CustomStatusBar isDark routeName={this.props.navigation.state.routeName}/>
                <View style={{ flex: 1 }}>
                    <Image style={styles.imageStyle1} source={require('../../Images/solo.png')} resizeMode='contain' />
                    <Image style={styles.imageStyle2} source={require('../../Images/Group.png')} resizeMode='contain' />
                    <Text style={styles.textStyle}>Do you want to turn on {'\n'}notification?</Text>
                    <View style={styles.bottomView}>
                        <OverlayButton
                            buttonBackgroundColor={colors.lightGreen}
                            buttonTextColor={colors.white}
                            clickButton={this.goToEnableFinger}
                            style={{ flex: 1 }}
                            text={"Turn Notifications"}
                            customStyle={styles.LoginButton}
                            textButtonStyle={styles.LoginTextButton} />
                    </View>
                    <TouchableOpacity onPress={this.goToEnableFinger}>
                        <Text style={styles.textforgotPassword}>Not Now</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    imageStyle1: {
        height: MeasurementCalculator.measure(132),
        width: MeasurementCalculator.measure(139, 'w'),
        marginTop: MeasurementCalculator.measure(59),
        alignSelf: "center"
    },
    imageStyle2: {
        height: MeasurementCalculator.measure(204.63),
        width: MeasurementCalculator.measure(375, 'w'),
        marginTop: MeasurementCalculator.measure(40.5),
        alignSelf: "center"
    },
    textStyle: {
        fontFamily: fonts.Karla_Bold,
        fontSize: MeasurementCalculator.measureFont(27),
        color: colors.black2,
        textAlign: 'center',
        marginTop: MeasurementCalculator.measure(32),
    },
    bottomView: {
        marginLeft: MeasurementCalculator.measure(24, 'w'),
        marginTop: MeasurementCalculator.measure(21.9),
    },
    LoginButton: {
        height: MeasurementCalculator.measure(68),
        borderTopLeftRadius: 50,
        borderBottomLeftRadius: 50,
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: MeasurementCalculator.measure(37, 'w'),
    },
    LoginTextButton: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: MeasurementCalculator.measureFont(20),
        letterSpacing: 0,
    },
    textforgotPassword: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: 12,
        color: colors.purple,
        marginLeft: MeasurementCalculator.measure(24, 'w'),
        marginTop: MeasurementCalculator.measure(35),
    }
});

export default NotificationMessage;
