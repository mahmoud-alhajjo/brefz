import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

class NotificationItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        let circleNotifigation = null
        if(this.props.item.notification == 1){
            circleNotifigation =<View style={styles.circleNotifigation} />
        }
        return (
            <View style={[mainStyles.rowFlex, mainStyles.flex, styles.container]}>
                <View>
                    {circleNotifigation}
                    <Image style={styles.imageStyle} resizeMode='contain' source={this.props.item.image} />
                </View>
                <View style={[mainStyles.columnFlex, mainStyles.flex ,{ marginTop: MeasurementCalculator.measure(19) }]}>
                    <Text style={{ paddingLeft: MeasurementCalculator.measure(12.4, 'w') }}>
                        <Text style={styles.textName}>{this.props.item.name}</Text>
                        <Text style={styles.textDescraption}> {this.props.item.descraption}</Text>
                    </Text>
                    <Text style={styles.textTime}>{this.props.item.time}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: "flex-start",
        backgroundColor: colors.white,
        paddingBottom: MeasurementCalculator.measure(20),
        marginLeft: MeasurementCalculator.measure(29.3, 'w'),
        marginRight: MeasurementCalculator.measure(38.5, 'w'),
        borderBottomWidth: 0.5,
        borderBottomColor: colors.gray5,
    },
    imageStyle: {
        height: MeasurementCalculator.measure(38.94),
        width: MeasurementCalculator.measure(38.94),
        borderRadius: 50,
        marginTop: MeasurementCalculator.measure(23.5),
    },
    textName: {
        fontFamily: fonts.Karla_Bold,
        fontSize: MeasurementCalculator.measureFont(16),
        color: colors.black2,
        lineHeight: 24,
    },
    textDescraption: {
        fontSize: MeasurementCalculator.measureFont(16),
        color: colors.black2,
        fontFamily: fonts.Karla_Regular,
        lineHeight: 24,
    },
    textTime: {
        fontFamily: fonts.Roboto_Regular,
        fontSize: 12,
        color: colors.gray4,
        lineHeight: 14,
        paddingLeft: MeasurementCalculator.measure(17.4, 'w'),
        marginTop: MeasurementCalculator.measure(4),
    },
    circleNotifigation: {
        position: 'absolute',
        left: 0,
        bottom: 0,
        width: MeasurementCalculator.measure(10),
        height: MeasurementCalculator.measure(10),
        borderRadius: 50,
        backgroundColor: colors.red2,
        zIndex: 1
    }
});


export default NotificationItem;
