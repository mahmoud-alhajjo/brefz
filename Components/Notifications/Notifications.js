import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

import NotificationItem from './NotificationItem';

class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifications: [
        {
          image: require('../../Images/imgeNotification.png'),
          name: 'YounesJamal',
          descraption: 'Added a new Review to your Profile.',
          time: '6 mins ago',
          notification : 1,
        },
        {
          image: require('../../Images/imgeNotification.png'),
          name: 'YounesJamal',
          descraption: 'Added a new Review to your Profile.',
          time: '6 mins ago',
          notification : 1,
        },
        {
          image: require('../../Images/imgeNotification.png'),
          name: 'YounesJamal',
          descraption: 'Added a new Review to your Profile.',
          time: '6 mins ago'
        },
        {
          image: require('../../Images/imgeNotification.png'),
          name: 'YounesJamal',
          descraption: 'Added a new Review to your Profile.',
          time: '6 mins ago'
        },
        {
          image: require('../../Images/imgeNotification.png'),
          name: 'YounesJamal',
          descraption: 'Added a new Review to your Profile.',
          time: '6 mins ago'
        },
        {
          image: require('../../Images/imgeNotification.png'),
          name: 'YounesJamal',
          descraption: 'Added a new Review to your Profile.',
          time: '6 mins ago'
        },
      ]
    };
  }

  render() {
    let noNotifigation = <View style={[mainStyles.flex, { justifyContent: 'center', alignItems: 'center' }]}><Text style={styles.textNoNotifigation}>You have no notifications</Text></View>
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <FlatList
          data={this.state.notifications}
          renderItem={({ item, index }) =>
            <NotificationItem item={item} index={index} />
          }
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: MeasurementCalculator.measureFont(45), paddingTop: MeasurementCalculator.measureFont(20)}}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
  },
  textNoNotifigation: {
    fontFamily: fonts.Montserrat_SemiBold,
    fontSize: MeasurementCalculator.measureFont(18),
    textAlign: "center",
    color: colors.gray4
  }
});

export default Notifications;
