import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';
import EscalationTextInputPopup from '../Popup/EscalationTextInputPopup';
import OverlayButton from '../Helpers/OverlayButton';

import Modal from 'react-native-modal';
import { closeOverlay, openOverlay } from 'react-native-blur-overlay';

class DashboardItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            escalationModal: false
        };
    }

    openEscalationPopup = () => {
        openOverlay();
        this.setState({ escalationModal: true });
    };

    closeEscalationPopup = () => {
        this.setState({ escalationModal: false });
        closeOverlay();
    }

    allPahsesAction = () => {
        this.props.navigation.navigate('AllPhases')
    }

    allSupportingFilesAction = () => {
        this.props.navigation.navigate('AllSupportingFiles')
    }

    calculateProgress = (progress) => {
        let Progress = { widthProgress: 0, heightProgress: 24 }
        if (progress <= 65) {
            Progress.widthProgress = (Dimensions.get('window').width - 8) * progress / 65
        } else {
            Progress.widthProgress = Dimensions.get('window').width
            Progress.heightProgress = (210 * (progress - 65) / 35) + 24
        }
        return (Progress);
    }

    render() {
        let progressValue = 47
        let progress = this.calculateProgress(progressValue)
        let status = null
        let projectManagers = null
        let phases = null
        let SupportingFiles = null
        let progressView = null

        progressView =
            <View style={{ backgroundColor: colors.lightGray4, }}>
                <View style={styles.containerProgress}>
                    <View style={[styles.barProgress, { height: progress.heightProgress, width: progress.widthProgress, }]} />
                    <View style={[mainStyles.rowFlex, styles.headerProgress]}>
                        <View style={styles.containerlogoImage}>
                            <Image style={styles.logoImageStyle} resizeMode="contain" source={require('../../Images/solo.png')} />
                        </View>
                        <View style={[mainStyles.flex, { marginTop: MeasurementCalculator.measure(-3), paddingRight: MeasurementCalculator.measure(35, 'w') }]}>
                            <Text style={styles.title}>BREFZ App Team Project</Text>
                            <Text style={styles.subtitle}>Lorem Project Text About The Project Here</Text>
                            <View style={[mainStyles.rowFlex, { marginTop: MeasurementCalculator.measure(7), alignItems: 'center' }]} >
                                <View style={styles.borderCircle}>
                                    <Image style={styles.ImageStyle} resizeMode="contain" source={require('../../Images/Oval.png')} />
                                </View>
                                <Text style={styles.textBy}>By </Text>
                                <Text style={styles.textName}>Mari</Text>
                            </View>
                        </View>
                    </View>
                    <Text style={styles.progressValue}>{progressValue}%</Text>
                </View>
            </View>

        if (this.props.header == 'Status') {
            status =
                <View>
                    {progressView}
                    < View style={styles.sectionStatus} >
                        <Text style={styles.headerStatus}>{this.props.header}</Text>
                        <View style={[styles.sectionItemStatus, mainStyles.rowFlex]}>
                            <Text style={styles.ItemStatusTitle}>Estimated Cost</Text>
                            <Text style={[styles.ItemStatusSubTitle, { backgroundColor: colors.yellow }]}>${this.props.item.estimatedCost}</Text>
                        </View>
                        <View style={[styles.sectionItemStatus, mainStyles.rowFlex]}>
                            <Text style={styles.ItemStatusTitle}>Actual Cost</Text>
                            <Text style={[styles.ItemStatusSubTitle, { backgroundColor: colors.yellow }]}>${this.props.item.actualCost}</Text>
                        </View>
                        <View style={[styles.sectionItemStatus, mainStyles.rowFlex]}>
                            <Text style={styles.ItemStatusTitle}>Start Date</Text>
                            <Text style={[styles.ItemStatusSubTitle, {}]}>{this.props.item.startDate}</Text>
                        </View>
                        <View style={[styles.sectionItemStatus, mainStyles.rowFlex]}>
                            <Text style={styles.ItemStatusTitle}>Signing Date</Text>
                            <Text style={[styles.ItemStatusSubTitle, {}]}>{this.props.item.signingDate}</Text>
                        </View>
                        <View style={[styles.sectionItemStatus, mainStyles.rowFlex]}>
                            <Text style={styles.ItemStatusTitle}>Scoping Date</Text>
                            <Text style={[styles.ItemStatusSubTitle, {}]}>{this.props.item.scopingDate}</Text>
                        </View>
                        <View style={[styles.sectionItemStatus, mainStyles.rowFlex]}>
                            <Text style={styles.ItemStatusTitle}>Estimated Deadline</Text>
                            <Text style={[styles.ItemStatusSubTitle, {}]}>{this.props.item.estimatedDeadline}</Text>
                        </View>
                        <View style={[styles.sectionItemStatus, mainStyles.rowFlex]}>
                            <Text style={styles.ItemStatusTitle}>Actual Deadline</Text>
                            <Text style={[styles.ItemStatusSubTitle, {}]}>{this.props.item.actualDeadline}</Text>
                        </View>
                        <View style={[styles.sectionItemStatus, mainStyles.rowFlex, { borderBottomWidth: 0 }]}>
                            <Text style={styles.ItemStatusTitle}>Status</Text>
                            <Text style={[styles.ItemStatusSubTitle, { backgroundColor: colors.lightGreen }]}>{this.props.item.status}</Text>
                        </View>
                    </View >
                </View>
        }

        if (this.props.header == 'projectManagers') {
            projectManagers =
                <View style={{ backgroundColor: colors.lightGray }}>
                    <View style={styles.sectionProjectManagers}>
                        <View style={[mainStyles.rowFlex, styles.headerProjecManagers]}>
                            <Text style={styles.headerStatus}>Project Managers</Text>
                            <OverlayButton
                                buttonBackgroundColor={colors.red5}
                                buttonTextColor={colors.white}
                                clickButton={this.openEscalationPopup}
                                style={{ flex: 1 }}
                                text={' Escalation'}
                                nameIcon={icons.Exclamation_Circle}
                                size={MeasurementCalculator.measureFont(12)}
                                customStyle={styles.escalationButton}
                                textButtonStyle={styles.escalationTextButton} />
                        </View>
                        {this.props.item.map((item, index) => {
                            let lastItem = this.props.item.length - 1
                            let borderBottomWidth = lastItem == index ? 0 : 1
                            return (
                                <View style={[mainStyles.rowFlex, styles.detailsProjecManagers, { borderBottomWidth: borderBottomWidth }]} key={index}>
                                    <Image style={styles.imageProjecManagers} resizeMode='contain' source={item.image} />
                                    <View>
                                        <Text style={styles.textProjecManagers}>
                                            {item.name}
                                        </Text>
                                        <View style={[mainStyles.rowFlex]}>
                                            <OverlayButton
                                                buttonBackgroundColor={colors.black}
                                                buttonTextColor={colors.white}
                                                clickButton={() => { }}
                                                style={{ flex: 1 }}
                                                text={' Call'}
                                                nameIcon={icons.Phone}
                                                size={MeasurementCalculator.measureFont(9)}
                                                customStyle={[styles.callButton, { marginRight: MeasurementCalculator.measure(7, 'w') }]}
                                                textButtonStyle={styles.callTextButton} />
                                            <OverlayButton
                                                buttonBackgroundColor={colors.black}
                                                buttonTextColor={colors.white}
                                                clickButton={() => { }}
                                                style={{ flex: 1 }}
                                                text={' Send Email'}
                                                nameIcon={icons.Paper_Plane}
                                                size={MeasurementCalculator.measureFont(9)}
                                                customStyle={[styles.callButton, { width: MeasurementCalculator.measure(96.38, 'w') }]}
                                                textButtonStyle={styles.callTextButton} />
                                        </View>
                                    </View>
                                </View>
                            )
                        })
                        }
                    </View>
                </View>
        }

        if (this.props.header == 'phases') {
            phases =
                <View style={{ backgroundColor: colors.black, paddingBottom: 3, borderBottomRightRadius: 78 }}>
                    <View style={styles.sectionPhases}>
                        <Text style={[styles.headerStatus, { marginBottom: MeasurementCalculator.measure(28.4) }]}>Phases</Text>
                        {this.props.item.map((item, index) => {
                            let circleDoneBackgroundColor = item.status == 'done' ? colors.green4 : item.status == 'in Progress' ? colors.yellow8 : item.status == 'waiting' ? colors.yellow7 : item.status == 'new' ? colors.blue4 : item.status == 'suspended' ? colors.lightYellow3 : colors.purple4
                            let textDoneBackgroundColor = item.status == 'done' ? colors.lightGreen4 : item.status == 'in Progress' ? colors.lightYellow4 : item.status == 'waiting' ? colors.lightYellow2 : item.status == 'new' ? colors.lightBlue4 : item.status == 'suspended' ? colors.lightYellow5 : colors.lightRed6
                            return (
                                <View style={[mainStyles.rowFlex, styles.detailsPhases]} key={index}>
                                    <View style={[mainStyles.rowFlex, { alignItems: "center" }]}>
                                        <Image style={styles.imagePhaseStyle} resizeMode="contain" tintColor={colors.black} source={require('../../Images/Phase.png')} />
                                        <Text style={styles.textPhases}>{item.name}</Text>
                                    </View>
                                    <View style={[mainStyles.rowFlex, styles.phaseDone, { alignItems: 'center', backgroundColor: textDoneBackgroundColor }]}>
                                        <View style={[styles.circleDone, { backgroundColor: circleDoneBackgroundColor }]} />
                                        <Text style={styles.textDone}> {item.status.toUpperCase()}</Text>
                                    </View>
                                </View>
                            )
                        })
                        }
                    </View>
                </View>
        }

        if (this.props.header == 'supportingFiles') {
            SupportingFiles =
                <View style={{ backgroundColor: colors.black, paddingBottom: 3, borderBottomRightRadius: 78 }}>
                    <View style={styles.sectionSupportingFiles}>
                        <Text style={[styles.headerStatus, { marginBottom: MeasurementCalculator.measure(10.1) }]}>Supporting Files</Text>
                        {this.props.item.map((item, index) => {
                            let iconName = item.type == 'pdf' ? icons.File_Pdf_O : icons.File_Archive
                            let lastItem = this.props.item.length - 1
                            let borderBottomWidth = lastItem == index ? 0 : 1
                            return (
                                <View style={[mainStyles.rowFlex, styles.detailsSupportingFiles, { borderBottomWidth: borderBottomWidth }]} key={index}>
                                    <View style={[mainStyles.rowFlex, { alignItems: "center" }]}>
                                        <View style={styles.containerIconSupportingFiles}>
                                            <FontawesomeIcon nameIcon={iconName} color={colors.black} size={MeasurementCalculator.measureFont(21)} />
                                        </View>
                                        <Text style={styles.textSupportingFiles}>{item.type.toUpperCase()}</Text>
                                    </View>
                                    <Text style={styles.dateSupportingFiles}> {item.date}</Text>
                                </View>
                            )
                        })
                        }
                    </View>
                    <View style={styles.buttonPhaseContainer}>
                        <OverlayButton
                            buttonBackgroundColor={colors.black}
                            buttonTextColor={colors.white}
                            clickButton={this.allPahsesAction}
                            style={{ flex: 1 }}
                            text={'  View More'}
                            nameIcon={icons.Plus}
                            size={MeasurementCalculator.measureFont(15)}
                            customStyle={[styles.phaseButton, { marginRight: MeasurementCalculator.measure(7, 'w') }]}
                            textButtonStyle={styles.phaseTextButton} />
                    </View>
                    <View style={styles.buttonSupportingFilesContainer}>
                        <OverlayButton
                            buttonBackgroundColor={colors.black}
                            buttonTextColor={colors.white}
                            clickButton={this.allSupportingFilesAction}
                            style={{ flex: 1 }}
                            text={'  View More'}
                            nameIcon={icons.Plus}
                            size={MeasurementCalculator.measureFont(15)}
                            customStyle={[styles.phaseButton, { marginRight: MeasurementCalculator.measure(7, 'w') }]}
                            textButtonStyle={styles.phaseTextButton} />
                    </View>
                </View>
        }

        return (
            <View style={mainStyles.flex}>
                {status}
                {projectManagers}
                {phases}
                {SupportingFiles}
                <Modal
                    isVisible={this.state.escalationModal}
                    animationIn={'slideInUp'}
                    animationInTiming={500}
                    animationOut={'slideOutDown'}
                    animationOutTiming={700}
                    backdropOpacity={.05}
                    backdropColor={colors.black}
                    style={{ justifyContent: 'center', alignItems: 'center' }}
                >
                    <EscalationTextInputPopup
                        cancel={() => this.closeEscalationPopup()}
                    />
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    sectionStatus: {
        paddingTop: MeasurementCalculator.measure(24.4),
        paddingBottom: MeasurementCalculator.measure(43),
        paddingLeft: MeasurementCalculator.measure(26.7, 'w'),
        paddingRight: MeasurementCalculator.measure(40.4, 'w'),
        backgroundColor: colors.lightGray4,
        borderBottomRightRadius: 80
    },
    headerStatus: {
        fontFamily: fonts.Montserrat_Bold,
        fontSize: MeasurementCalculator.measureFont(16),
        color: colors.black,
        marginBottom: MeasurementCalculator.measure(7.7),
    },
    sectionItemStatus: {
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: MeasurementCalculator.measure(13.9),
        paddingBottom: MeasurementCalculator.measure(16.1),
        borderBottomWidth: 1,
        borderBottomColor: colors.lightGray5
    },
    ItemStatusTitle: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: MeasurementCalculator.measureFont(13),
        color: colors.black,
    },
    ItemStatusSubTitle: {
        fontFamily: fonts.Montserrat_Medium,
        fontSize: MeasurementCalculator.measureFont(11),
        color: colors.white,
        borderRadius: 8,
        paddingVertical: MeasurementCalculator.measure(3),
        paddingHorizontal: MeasurementCalculator.measure(9, 'w'),
        backgroundColor: colors.lightBlue,
    },
    sectionProjectManagers: {
        paddingTop: MeasurementCalculator.measure(31),
        paddingBottom: MeasurementCalculator.measure(38.2),
        paddingLeft: MeasurementCalculator.measure(26.7, 'w'),
        paddingRight: MeasurementCalculator.measure(40.4, 'w'),
        backgroundColor: colors.white,
        borderBottomRightRadius: 80
    },
    headerProjecManagers: {
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: MeasurementCalculator.measure(9.5)
    },
    escalationButton: {
        height: MeasurementCalculator.measure(21),
        width: MeasurementCalculator.measure(114.13, 'w'),
        borderRadius: 29,
        justifyContent: 'center',
        alignItems: 'center',
    },
    escalationTextButton: {
        fontFamily: fonts.Montserrat_Medium,
        fontSize: MeasurementCalculator.measureFont(12),
        letterSpacing: 0.83,
    },
    detailsProjecManagers: {
        borderBottomWidth: 1,
        borderBottomColor: colors.lightGray6,
        paddingBottom: MeasurementCalculator.measure(22.2),
        paddingTop: MeasurementCalculator.measure(15.5),
    },
    textProjecManagers: {
        fontFamily: fonts.Montserrat_Regular,
        fontSize: MeasurementCalculator.measureFont(16),
        color: colors.black,
        marginBottom: MeasurementCalculator.measure(9.1)
    },
    imageProjecManagers: {
        height: MeasurementCalculator.measure(57.13),
        width: MeasurementCalculator.measure(57.13),
        borderRadius: 50,
        marginRight: MeasurementCalculator.measure(15.3, 'w'),
    },
    callButton: {
        height: MeasurementCalculator.measure(24.46),
        width: MeasurementCalculator.measure(59.47, 'w'),
        borderRadius: 86,
        justifyContent: 'center',
        alignItems: 'center',
    },
    callTextButton: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: MeasurementCalculator.measureFont(9),
    },
    sectionPhases: {
        paddingTop: MeasurementCalculator.measure(30.4),
        paddingBottom: MeasurementCalculator.measure(42.8),
        paddingLeft: MeasurementCalculator.measure(26.7, 'w'),
        paddingRight: MeasurementCalculator.measure(40.4, 'w'),
        backgroundColor: colors.lightGray,
        borderBottomRightRadius: 80,
    },
    headerPhases: {
        alignItems: 'center',
        marginBottom: MeasurementCalculator.measure(9.5)
    },
    detailsPhases: {
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: MeasurementCalculator.measure(14.2),
    },
    imagePhaseStyle: {
        height: MeasurementCalculator.measure(17.92),
        width: MeasurementCalculator.measure(20.24),
        marginRight: MeasurementCalculator.measure(15.3, 'w'),
    },
    textPhases: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: MeasurementCalculator.measureFont(16),
        color: colors.black
    },
    phaseDone: {
        paddingHorizontal: MeasurementCalculator.measure(7, 'w'),
        paddingVertical: MeasurementCalculator.measure(2.5),
        borderRadius: 5,
        backgroundColor: colors.lightGreen4,
    },
    circleDone: {
        width: MeasurementCalculator.measure(8),
        height: MeasurementCalculator.measure(8),
        backgroundColor: colors.green4,
        borderRadius: 50,
    },
    textDone: {
        fontSize: MeasurementCalculator.measureFont(12),
        fontFamily: fonts.Montserrat_Medium,
        color: colors.black,
    },
    buttonPhaseContainer: {
        position: 'absolute',
        top: MeasurementCalculator.measure(-21.765),
        left: MeasurementCalculator.measure(26.7, 'w'),
    },
    phaseButton: {
        height: MeasurementCalculator.measure(39.53),
        width: MeasurementCalculator.measure(155.76, 'w'),
        borderRadius: 86,
        justifyContent: 'center',
        alignItems: 'center',
    },
    phaseTextButton: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: MeasurementCalculator.measureFont(15),
    },
    sectionSupportingFiles: {
        paddingTop: MeasurementCalculator.measure(40.4, 'w'),
        paddingBottom: MeasurementCalculator.measure(50),
        paddingLeft: MeasurementCalculator.measure(26.7, 'w'),
        paddingRight: MeasurementCalculator.measure(40.4, 'w'),
        backgroundColor: colors.white,
        borderBottomRightRadius: 80,
    },
    detailsSupportingFiles: {
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: colors.lightGray6,
        paddingTop: MeasurementCalculator.measure(19.9),
        paddingBottom: MeasurementCalculator.measure(18.1),
    },
    containerIconSupportingFiles: {
        alignItems: 'center',
        justifyContent: 'center',
        width: MeasurementCalculator.measure(46),
        height: MeasurementCalculator.measure(46),
        borderRadius: 50,
        borderWidth: 1,
        borderColor: colors.gray9
    },
    textSupportingFiles: {
        fontSize: MeasurementCalculator.measureFont(21),
        fontFamily: fonts.Montserrat_ExtraBold,
        color: colors.black,
        paddingLeft: MeasurementCalculator.measureFont(14, 'w'),
    },
    dateSupportingFiles: {
        fontSize: MeasurementCalculator.measureFont(14),
        fontFamily: fonts.Montserrat_Regular,
        color: colors.white,
        letterSpacing: 1.83,
        backgroundColor: colors.darkBlue3,
        borderRadius: 20,
        paddingVertical: MeasurementCalculator.measure(6),
        paddingHorizontal: MeasurementCalculator.measure(13.5, 'w'),
    },
    buttonSupportingFilesContainer: {
        position: 'absolute',
        bottom: MeasurementCalculator.measure(-17.765),
        left: MeasurementCalculator.measure(26.7, 'w'),
    },
    //======================
    // style progress Header
    //======================
    containerProgress: {
        backgroundColor: colors.lightBlue2,
        paddingBottom: 24,
        borderBottomRightRadius: 80,
        overflow: 'hidden'
    },
    barProgress: {
        position: 'absolute',
        backgroundColor: '#27E0FF',
        zIndex: -1,
        left: 0,
        bottom: 0
    },
    headerProgress: {
        width: Dimensions.get('window').width - 8,
        backgroundColor: colors.lightBlue,
        paddingTop: MeasurementCalculator.measure(26) + 67,
        borderBottomRightRadius: 80,
        paddingBottom: MeasurementCalculator.measure(31),
        paddingLeft: MeasurementCalculator.measure(27)
    },
    containerlogoImage: {
        backgroundColor: colors.white,
        height: MeasurementCalculator.measure(91),
        width: MeasurementCalculator.measure(95, 'w'),
        marginRight: MeasurementCalculator.measure(15, 'w'),
        borderRadius: 18,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logoImageStyle: {
        width: MeasurementCalculator.measure(50, 'w'),
        height: MeasurementCalculator.measure(48),
    },
    title: {
        fontSize: MeasurementCalculator.measureFont(17),
        fontFamily: fonts.Montserrat_Bold,
        color: colors.white,
        lineHeight: 17
    },
    subtitle: {
        fontSize: MeasurementCalculator.measureFont(10),
        paddingRight: MeasurementCalculator.measure(92, 'w'),
        fontFamily: fonts.Montserrat_SemiBold,
        color: colors.lightGray3,
        lineHeight: 13
    },
    borderCircle: {
        borderWidth: 1,
        borderColor: colors.white,
        borderRadius: 50,
    },
    ImageStyle: {
        width: MeasurementCalculator.measure(18),
        height: MeasurementCalculator.measure(18),
        borderRadius: 50,
    },
    textBy: {
        fontSize: MeasurementCalculator.measureFont(9),
        fontFamily: fonts.Montserrat_Bold,
        color: colors.white,
        paddingLeft: MeasurementCalculator.measure(5, 'w'),
    },
    textName: {
        fontSize: MeasurementCalculator.measureFont(9),
        fontFamily: fonts.Montserrat_Medium,
        color: colors.white,
        paddingRight: MeasurementCalculator.measure(12, 'w'),
    },
    progressValue: {
        position: 'absolute',
        left: MeasurementCalculator.measure(27),
        bottom: 5,
        zIndex: 6,
        fontFamily: fonts.Montserrat_ExtraBold,
        fontSize: MeasurementCalculator.measureFont(12),
        color: colors.white
    }
})

export default DashboardItem;
