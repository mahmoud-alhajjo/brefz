import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

class ProjectItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    dashboardAction = () => {
        this.props.navigation.navigate('Dashboard')
    }

    render() {
        let containerStatusProgressShadowiOS = Platform.OS === 'ios' ? styles.containerStatusProgressShadowiOS : null
        let circleDoneBackgroundColor = this.props.item.status == 'done' ? colors.darkGreen : this.props.item.status == 'in Progress' ? colors.yellow8 : this.props.item.status == 'draft' ? colors.gray11 : this.props.item.status == 'new' ? colors.blue4 : this.props.item.status == 'suspended' ? colors.lightYellow3 : colors.purple4
        let textDoneBackgroundColor = this.props.item.status == 'done' ? colors.lightGreen : this.props.item.status == 'in Progress' ? colors.lightYellow4 : this.props.item.status == 'draft' ? colors.lightGray2 : this.props.item.status == 'new' ? colors.lightBlue4 : this.props.item.status == 'suspended' ? colors.lightYellow5 : colors.lightRed6
        let progressBar = null
        let inProgress = null
        let justifyContentContainerStatusProgress = 'center'
        let marginTopLogoImage = 0
        let backgroundColor = colors.gray12
        let backgroundColorCorner = colors.gray10
        let paddingTop = MeasurementCalculator.measure(27)
        let paddingBottomLastItem = 0
        if (this.props.index === 0) {
            paddingTop = this.props.heightDrawerHeader + MeasurementCalculator.measure(27)
        }
        if ((this.props.index % 2) == 0) {
            backgroundColor = colors.gray10
            backgroundColorCorner = colors.gray12
        }
        if (this.props.lastItem == this.props.index) {
            paddingBottomLastItem = MeasurementCalculator.measure(50)
        }
        if (this.props.item.status == 'in Progress') {
            justifyContentContainerStatusProgress = 'space-between'
            marginTopLogoImage = MeasurementCalculator.measure(14)
            inProgress = this.props.item.progress * 0.95
            progressBar =
                <View style={styles.progressContainer}>
                    <View style={[styles.progress, { borderLeftWidth: MeasurementCalculator.measure(inProgress, 'w') }]} />
                    <Text style={styles.textProgress}>%{this.props.item.progress}</Text>
                </View>
        }
        return (
            <TouchableOpacity onPress={this.dashboardAction} activeOpacity={.9}>
                <View style={[mainStyles.flex, { backgroundColor: backgroundColorCorner, paddingBottom: paddingBottomLastItem }]}>
                    <View style={[styles.allProjectsDetails, mainStyles.rowFlex, { backgroundColor: backgroundColor, paddingTop: paddingTop }]}>
                        <View style={{ alignItems: 'center', marginRight: MeasurementCalculator.measure(15, 'w') }}>
                            <View style={[styles.containerStatusProgress, containerStatusProgressShadowiOS, { justifyContent: justifyContentContainerStatusProgress }]}>
                                <Image style={[styles.logoImageStyle, { marginTop: marginTopLogoImage }]} resizeMode="contain" source={require('../../Images/solo.png')} />
                                {progressBar}
                            </View>
                            <View style={[mainStyles.rowFlex, styles.ProjectDone, { alignItems: 'center', backgroundColor: textDoneBackgroundColor }]}>
                                <View style={[styles.circleDone, { backgroundColor: circleDoneBackgroundColor }]} />
                                <Text style={styles.textDone}> {this.props.item.status.toUpperCase()}</Text>
                            </View>
                        </View>
                        <View style={[mainStyles.flex, { marginTop: MeasurementCalculator.measure(-3) }]}>
                            <Text style={styles.title}>{this.props.item.title}</Text>
                            <Text style={styles.subtitle}>{this.props.item.subtitle}</Text>
                            <View style={[mainStyles.rowFlex, { marginTop: MeasurementCalculator.measure(7), alignItems: 'center' }]} >
                                <View style={styles.borderCircle}>
                                    <Image style={styles.ImageStyle} resizeMode="contain" source={require('../../Images/Oval.png')} />
                                </View>
                                <Text style={styles.textBy}>By </Text>
                                <Text style={styles.textName}>{this.props.item.name}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    allProjectsDetails: {
        paddingLeft: MeasurementCalculator.measure(19, 'w'),
        paddingRight: MeasurementCalculator.measure(23, 'w'),
        paddingVertical: MeasurementCalculator.measure(27),
        borderBottomRightRadius: 80,
        alignItems: 'flex-start',
    },
    containerStatusProgress: {
        backgroundColor: colors.white,
        height: MeasurementCalculator.measure(91),
        width: MeasurementCalculator.measure(95, 'w'),
        borderRadius: 18,
        elevation: 2.5,
    },
    containerStatusProgressShadowiOS: {
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.05,
        shadowRadius: 14
    },
    logoImageStyle: {
        width: MeasurementCalculator.measure(50, 'w'),
        height: MeasurementCalculator.measure(48),
        alignSelf: 'center'
    },
    progressContainer: {
        height: MeasurementCalculator.measure(17),
        backgroundColor: colors.lightBlue5,
        borderBottomLeftRadius: 18,
        borderBottomRightRadius: 18,
        overflow: 'hidden',
    },
    progress: {
        height: MeasurementCalculator.measure(17),
        borderLeftColor: colors.darkBlue5,
        position: 'absolute',
        left: 0,
        zIndex: 1
    },
    textProgress: {
        position: 'absolute',
        right: MeasurementCalculator.measure(14, 'w'),
        bottom: MeasurementCalculator.measure(-1.5),
        zIndex: 2,
        color: colors.black,
        fontFamily: fonts.Montserrat_Medium,
        fontSize: MeasurementCalculator.measureFont(12),
    },
    textProject: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: MeasurementCalculator.measureFont(16),
        color: colors.purple2,
        textAlign: 'center',
        paddingRight: MeasurementCalculator.measure(10, 'w'),
    },
    ProjectDone: {
        paddingHorizontal: MeasurementCalculator.measure(7, 'w'),
        paddingVertical: MeasurementCalculator.measure(2.5),
        borderRadius: 5,
        backgroundColor: colors.lightGreen4,
        marginTop: MeasurementCalculator.measure(17),
    },
    circleDone: {
        width: MeasurementCalculator.measure(8),
        height: MeasurementCalculator.measure(8),
        backgroundColor: colors.green4,
        borderRadius: 50,
    },
    textDone: {
        fontSize: MeasurementCalculator.measureFont(12),
        fontFamily: fonts.Montserrat_Medium,
        color: colors.black,
    },
    title: {
        fontSize: MeasurementCalculator.measureFont(17),
        fontFamily: fonts.Montserrat_Bold,
        color: colors.black,
        lineHeight: 17
    },
    subtitle: {
        fontSize: MeasurementCalculator.measureFont(10),
        paddingRight: MeasurementCalculator.measure(48, 'w'),
        fontFamily: fonts.Montserrat_SemiBold,
        color: colors.gray13,
        lineHeight: 13
    },
    borderCircle: {
        borderWidth: 1,
        borderColor: colors.white,
        borderRadius: 50,
    },
    ImageStyle: {
        width: MeasurementCalculator.measure(18),
        height: MeasurementCalculator.measure(18),
        borderRadius: 50,
    },
    textBy: {
        fontSize: MeasurementCalculator.measureFont(9),
        fontFamily: fonts.Montserrat_Bold,
        color: colors.black,
        paddingLeft: MeasurementCalculator.measure(5, 'w'),
    },
    textName: {
        fontSize: MeasurementCalculator.measureFont(9),
        fontFamily: fonts.Montserrat_Medium,
        color: colors.black,
        paddingRight: MeasurementCalculator.measure(12, 'w'),
    },
})

export default ProjectItem;
