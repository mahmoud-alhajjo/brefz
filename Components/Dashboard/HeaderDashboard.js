import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, SafeAreaView } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import mainStyles from '../../css/MainStyle';
import CustomStatusBar from '../Main Structure/CustomStatusBar';
import * as icons from '../../css/icons';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import OverlayButton from '../Helpers/OverlayButton';

class HeaderDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    allProjectsAction = () => {
        this.props.navigation.navigate('AllProjects');
    }

    allPaymentsAction = () => {
        this.props.navigation.navigate('AllPayments');
    }

    render() {
        return (
            <View style={[this.props.customeStyle ,{ borderBottomRightRadius: 80 }]}>
                <CustomStatusBar routeName={'Dashboard'} />
                <SafeAreaView style={[styles.header, mainStyles.rowFlex]}>
                    <View style={[mainStyles.rowFlex, { paddingTop: 17 }]}>
                        <TouchableOpacity style={styles.leftIcon} onPress={this.allProjectsAction}>
                            <FontawesomeIcon nameIcon={icons.Th_list} color={colors.white} size={17} customStyle={{ paddingTop: 5 }} />
                        </TouchableOpacity>
                        <View style={[mainStyles.columnFlex]}>
                            <Text style={[styles.textHeader]}>Dashboard</Text>
                        </View>
                    </View>
                    <View style={styles.buttonView}>
                        <OverlayButton
                            buttonBackgroundColor={colors.white}
                            buttonTextColor={colors.lightBlue2}
                            clickButton={this.allPaymentsAction}
                            style={{ flex: 1 }}
                            text={' PAYMENTS'}
                            nameIcon={icons.Money}
                            size={10}
                            customStyle={styles.PaymentsButton}
                            textButtonStyle={styles.PaymentsTextButton} />
                    </View>
                </SafeAreaView>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    header: {
        borderBottomRightRadius: 80,
        backgroundColor: colors.yellow,
        justifyContent: 'space-between',
        height: 67,
    },
    leftIcon: {
        height: 40,
        paddingLeft: 27,
        paddingRight: 9.5
    },
    textHeader: {
        fontSize: 19,
        fontFamily: fonts.Montserrat_SemiBold,
        color: colors.white,
    },
    buttonView: {
        marginRight: 40.4,
        marginTop: 17,
    },
    PaymentsButton: {
        height: 27,
        width: 106.13,
        justifyContent: 'center',
        borderRadius: 29,
    },
    PaymentsTextButton: {
        fontFamily: fonts.Montserrat_Medium,
        fontSize: 10,
        letterSpacing: 1,
    },
})

export default HeaderDashboard;
