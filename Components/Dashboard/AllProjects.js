import React, { Component } from 'react';
import { View, Text, StyleSheet, Animated, Dimensions, FlatList } from 'react-native';
import colors from '../../css/colors';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

import ProjectItem from './ProjectItem';
import DrawerHeader from '../Main Structure/DrawerHeader';

class AllProjects extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animationDrawer: new Animated.Value(-Dimensions.get('window').width),
        };
        Projects = [
            {
                title: 'BREFZ App Team Project',
                subtitle: 'Lorem Project Text About The Project Here',
                name: 'Mari',
                image: 'Mari',
                status: 'in Progress',
                progress : '45',
            },
            {
                title: 'BREFZ App Team Project',
                subtitle: 'Lorem Project Text About The Project Here',
                name: 'Mari',
                image: 'Mari',
                status: 'draft',
                progress : '45',
            },
            {
                title: 'BREFZ App Team Project',
                subtitle: 'Lorem Project Text About The Project Here',
                name: 'Mari',
                image: 'Mari',
                status: 'new',
                progress : '45',
            },
            {
                title: 'BREFZ App Team Project',
                subtitle: 'Lorem Project Text About The Project Here',
                name: 'Mari',
                image: 'Mari',
                status: 'done',
                progress : '45',
            },
            {
                title: 'BREFZ App Team Project',
                subtitle: 'Lorem Project Text About The Project Here',
                name: 'Mari',
                image: 'Mari',
                status: 'suspended',
                progress : '45',
            },
            {
                title: 'BREFZ App Team Project',
                subtitle: 'Lorem Project Text About The Project Here',
                name: 'Mari',
                image: 'Mari',
                status: 'canceled',
                progress : '45',
            },
        ]
    }

    startAnimationOpenDrawer = () => {
        Animated.timing(this.state.animationDrawer, {
            toValue: 0,
            duration: 300,
            delay: 200,
        }).start();
    }

    startAnimationCloseDrawer = () => {
        Animated.timing(this.state.animationDrawer, {
            toValue: -Dimensions.get('window').width,
            duration: 500,
        }).start();
    }

    goBackAction = () => {
        this.startAnimationCloseDrawer();
        this.props.navigation.goBack();
    }

    render() {
        this.startAnimationOpenDrawer();
        const animationStyle = { transform: [{ translateX: this.state.animationDrawer }] }        
        return (
            <View style={styles.overlay}>
                <Animated.View style={[animationStyle, styles.container]}>
                    <DrawerHeader navigation={this.props.navigation} title={'Projects'} goBackAction={this.goBackAction} />
                    <FlatList
                        data={Projects}
                        renderItem={({ item, index }) =>
                            <ProjectItem item={item} index={index} navigation={this.props.navigation} lastItem={Projects.length - 1} heightDrawerHeader={MeasurementCalculator.measure(96.92792443971377)} />
                        }
                        keyExtractor={(item, index) => index.toString()}
                        showsVerticalScrollIndicator={false}
                        style={styles.list} />
                </Animated.View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    overlay: {
        flex: 1,
        width: Dimensions.get('window').width,
        backgroundColor: colors.overlay,
    },
    container: {
        flex: 1,
        width: Dimensions.get('window').width - 31,
        backgroundColor: 'transparent',
    },
    list: {
        width: Dimensions.get('window').width - 31,
        height: Dimensions.get('window').height,
        position: 'absolute',
        backgroundColor: colors.gray12,
        top: 0,
        left: 0,
        zIndex: -1,
    }
})

export default AllProjects;
