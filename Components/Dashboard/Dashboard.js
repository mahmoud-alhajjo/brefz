import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, SectionList, Dimensions } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';
import CustomStatusBar from '../Main Structure/CustomStatusBar';
import * as icons from '../../css/icons';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import OverlayButton from '../Helpers/OverlayButton';
import DashboardItem from './DashboardItem';
import HeaderDashboard from './HeaderDashboard';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        DashboardArray = [
            {
                header: 'Status',
                data: [
                    {
                        estimatedCost: "16.287",
                        actualCost: "12.287",
                        startDate: "21/08/2019",
                        signingDate: '21/08/2019',
                        scopingDate: '21/08/2019',
                        estimatedDeadline: '21/08/2019',
                        actualDeadline: '21/08/2019',
                        status: "IN PROGRESS",
                    }
                ],
            },
            {
                header: 'projectManagers',
                data: [
                    [
                        {
                            name: 'Jamal Youness',
                            image: require('../../Images/avatar.png')
                        },
                        {
                            name: 'Jamal Youness',
                            image: require('../../Images/avatar.png'),
                        }
                    ],
                ],
            },
            {
                header: 'phases',
                data: [
                    [
                        {
                            name: 'Design',
                            status: 'in Progress',
                        },
                        {
                            name: 'Front end',
                            status: 'done',
                        },
                        {
                            name: 'Back end',
                            status: 'canceld',
                        },
                        {
                            name: 'Testing',
                            status: 'done',
                        },
                        {
                            name: 'App Design',
                            status: 'waiting',
                        },
                    ],
                ],
            },
            {
                header: 'supportingFiles',
                data: [
                    [
                        {
                            type: 'pdf',
                            date: '23/07/2019',
                        },
                        {
                            type: 'zip',
                            date: '23/07/2019',
                        },
                    ],
                ],
            },
        ];
    }

    render() {
        return (
            <View style={styles.overlay}>
                <View style={[styles.container]}>
                    <HeaderDashboard navigation={this.props.navigation} customeStyle={styles.header} />
                    <SectionList
                        sections={DashboardArray}
                        keyExtractor={(item, index) => item + index}
                        renderItem={({ item, index, section: { header } }) => <DashboardItem header={header} item={item} index={index} navigation={this.props.navigation} />}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ paddingBottom: 54 }}
                    />
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    overlay: {
        flex: 1,
        width: Dimensions.get('window').width,
        backgroundColor: colors.white,
    },
    container: {
        flex: 1,
        width: Dimensions.get('window').width,
        backgroundColor: 'transparent',
    },
    header: {
        width: Dimensions.get('window').width,
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 4,
    },
    list: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        position: 'absolute',
        backgroundColor: colors.white,
        top: 0,
        left: 0,
        zIndex: 1,
    },
})

export default Dashboard;
