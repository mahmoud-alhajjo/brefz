import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform, TouchableOpacity, PermissionsAndroid } from 'react-native';

import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import mainStyles from '../../css/MainStyle';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import OverlayButton from '../Helpers/OverlayButton';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

import RNCalendarEvents from "react-native-calendar-events";
import Moment from 'moment';

class MeetingItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    saveEventInCalendar = () => {
        RNCalendarEvents.saveEvent(this.props.item.mettingName, {
            startDate: Moment().toISOString(),
            endDate: Moment().toISOString()
        }).then((data) => {
        }).catch((error) => { })
    }

    saveInCalander = async () => {
        try {
            if (Platform.OS === 'ios') {
                await RNCalendarEvents.authorizeEventStore().then((permission) => {
                    if (permission == 'authorized') {
                        this.saveEventInCalendar();
                    } else {
                        alert('Please give permission to the calendar')
                    }
                })
            } else {
                let granted = await PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.WRITE_CALENDAR])
                if ((granted[PermissionsAndroid.PERMISSIONS.WRITE_CALENDAR] === PermissionsAndroid.RESULTS.GRANTED)) {
                    this.saveEventInCalendar();
                } else {
                    alert('Please give permission to the calendar')
                }
            }
        } catch (err) {
        }
    }

    meetingDetailsAction = () => {
        this.props.navigation.navigate('MeetingDetails');
    }

    render() {
        let cardShadowiOS = Platform.OS === 'ios' ? styles.cardShadowiOS : null
        let buttonRight = null
        let borderLeftColor = null
        let hourColor = null
        if (this.props.item.calander == 1) {
            borderLeftColor = colors.orange2,
                buttonRight = <OverlayButton
                    buttonBackgroundColor={colors.darkBlue3}
                    buttonTextColor={colors.white}
                    clickButton={() => { this.saveInCalander() }}
                    style={{ flex: 1 }}
                    text={' Add To Calendar'}
                    nameIcon={icons.Calendar_Plus_O}
                    size={8}
                    customStyle={styles.calanderButton}
                    textButtonStyle={styles.calanderTextButton} />
        } else {
            borderLeftColor = colors.lightGreen,
                hourColor = { color: colors.black, backgroundColor: colors.lightGreen3 }
            buttonRight = <Text style={styles.textDone}>Done!</Text>
        }
        return (
            <TouchableOpacity onPress={this.meetingDetailsAction}>
                <View style={[mainStyles.flex, styles.container, cardShadowiOS, { borderLeftColor: borderLeftColor }]}>
                    <View style={[mainStyles.rowFlex, styles.titleView]}>
                        <Text style={styles.title}>
                            <FontawesomeIcon nameIcon={icons.Handshake} color={colors.darkBlue2} size={MeasurementCalculator.measureFont(18)} /> {this.props.item.mettingName}
                        </Text>
                        <FontawesomeIcon nameIcon={icons.Chevron_Right} color={colors.gray7} size={MeasurementCalculator.measureFont(8)} />
                    </View>
                    <View style={[mainStyles.rowFlex, mainStyles.flex, styles.calendarView]}>
                        <View style={[mainStyles.rowFlex, { alignItems: 'center' }]}>
                            <Text style={styles.time}>{this.props.item.time}</Text>
                            <Text style={[styles.hour, hourColor]}>{this.props.item.hour}</Text>
                        </View>
                        {buttonRight}
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: MeasurementCalculator.measure(1),
        marginBottom: MeasurementCalculator.measure(25.3),
        paddingVertical: MeasurementCalculator.measure(16.5),
        paddingLeft: MeasurementCalculator.measure(16, 'w'),
        paddingRight: MeasurementCalculator.measure(13.2, 'w'),
        marginRight: MeasurementCalculator.measure(1, 'w'),
        backgroundColor: colors.white,
        borderLeftWidth: 3,
        borderBottomRightRadius: 5,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
        elevation: 2
    },
    cardShadowiOS: {
        borderWidth: 1,
        borderRightColor: colors.gray6,
        borderTopColor: colors.gray6,
        borderBottomColor: colors.gray6,
        shadowColor: colors.gray6,
        shadowOffset: {
            width: 0,
            height: 11,
        },
        shadowRadius: 14
    },
    titleView: {
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    title: {
        fontFamily: fonts.Montserrat_Medium,
        fontSize: MeasurementCalculator.measureFont(17),
        color: colors.darkBlue2,
    },
    calendarView: {
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: MeasurementCalculator.measure(13.6),
    },
    time: {
        fontFamily: fonts.Montserrat_Regular,
        fontSize: MeasurementCalculator.measureFont(13),
        color: colors.darkBlue2,
        marginRight: MeasurementCalculator.measure(12.1, 'w'),
    },
    hour: {
        fontFamily: fonts.Montserrat_Medium,
        fontSize: MeasurementCalculator.measureFont(14),
        color: colors.orange,
        backgroundColor: colors.lightOrange,
        borderRadius: 5,
        paddingVertical: MeasurementCalculator.measure(2),
        paddingHorizontal: MeasurementCalculator.measure(16, 'w'),
    },
    calanderButton: {
        paddingHorizontal: MeasurementCalculator.measure(10.4, 'w'),
        paddingVertical: MeasurementCalculator.measure(6.1),
        height: MeasurementCalculator.measure(22.2),
        justifyContent: 'center',
        borderRadius: 8,
    },
    calanderTextButton: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: MeasurementCalculator.measureFont(12),
    },
    textDone: {
        backgroundColor: colors.lightGreen,
        color: colors.white,
        borderRadius: 8,
        fontFamily: fonts.Montserrat_Medium,
        fontSize: MeasurementCalculator.measureFont(12),
        paddingHorizontal: MeasurementCalculator.measure(14.4, 'w'),
        paddingVertical: MeasurementCalculator.measure(6.1),
    }
});

export default MeetingItem;
