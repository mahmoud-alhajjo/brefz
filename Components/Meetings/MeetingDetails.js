import React, { Component } from 'react';
import { View, Text, StyleSheet, Animated, Dimensions } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import mainStyles from '../../css/MainStyle';
import OverlayButton from '../Helpers/OverlayButton';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

import Comments from '../Main Structure/Comments';
import DrawerHeader from '../Main Structure/DrawerHeader';

class MeetingDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animationDrawer: new Animated.Value(-Dimensions.get('window').width),
    };
  }

  startAnimationOpenDrawer = () => {
    Animated.timing(this.state.animationDrawer, {
      toValue: 0,
      duration: 300,
      delay: 200,
    }).start();
  }

  startAnimationCloseDrawer = () => {
    Animated.timing(this.state.animationDrawer, {
      toValue: -Dimensions.get('window').width,
      duration: 500,
    }).start();
  }

  goBackAction = () => {
    this.startAnimationCloseDrawer();
    this.props.navigation.goBack();
  }

  render() {
    this.startAnimationOpenDrawer();
    const animationStyle = { transform: [{ translateX: this.state.animationDrawer }] }
    return (
      <View style={styles.overlay}>
        <Animated.View style={[animationStyle, styles.container]}>
          <DrawerHeader navigation={this.props.navigation} title={'Meeting Name'} goBackAction={this.goBackAction} />
          <View style={styles.meetingDetails}>
            <OverlayButton
              buttonBackgroundColor={colors.darkBlue3}
              buttonTextColor={colors.white}
              clickButton={() => { }}
              style={{ flex: 1 }}
              text={' Add To Calendar'}
              nameIcon={icons.Calendar_Plus_O}
              size={MeasurementCalculator.measureFont(12)}
              customStyle={styles.calanderButton}
              textButtonStyle={styles.calanderTextButton} />
            <View style={[mainStyles.rowFlex, { justifyContent: 'space-between', alignItems: 'center' }]}>
              <Text style={styles.textDate}><FontawesomeIcon nameIcon={icons.Calendar} color={colors.purple2} size={MeasurementCalculator.measureFont(16)} customStyle={{ fontWeight: '600' }} /> Date</Text>
              <View style={mainStyles.rowFlex}>
                <Text style={styles.timeHour}>15:30</Text>
                <Text style={styles.dateTime}>22.7.2019</Text>
              </View>
            </View>
            <Text style={styles.meetingLocation}><FontawesomeIcon nameIcon={icons.Map_Pin} color={colors.purple2} size={MeasurementCalculator.measureFont(16)} customStyle={{ fontWeight: '600' }} /> Meeting Location</Text>
            <Text style={styles.meetingLocationDetails}>Lorem Location meeting lorem location test Lorem Location meeting lorem..</Text>
          </View>
          <Comments />
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  overlay: {
    flex: 1,
    width: Dimensions.get('window').width,
    backgroundColor: colors.overlay,
  },
  container: {
    flex: 1,
    width: Dimensions.get('window').width - 31,
    backgroundColor: colors.white,
  },
  meetingDetails: {
    backgroundColor: colors.lightGray,
    paddingLeft: MeasurementCalculator.measure(26, 'w'),
    paddingRight: MeasurementCalculator.measure(35.6, 'w'),
    paddingTop: MeasurementCalculator.measure(25),
    paddingBottom: MeasurementCalculator.measure(15.3),
    borderBottomRightRadius: 80,
  },
  calanderButton: {
    paddingHorizontal: MeasurementCalculator.measure(13,  'w'),
    paddingVertical: MeasurementCalculator.measure(7.35),
    height: MeasurementCalculator.measure(27.68),
    justifyContent: 'center',
    borderRadius: 8,
    position: 'absolute',
    top: MeasurementCalculator.measure(-13.84),
    left: MeasurementCalculator.measure(24, 'w'),
    zIndex: 1
  },
  calanderTextButton: {
    fontFamily: fonts.Montserrat_SemiBold,
    fontSize: MeasurementCalculator.measureFont(12),
    textAlign: 'center',
  },
  textDate: {
    fontFamily: fonts.Montserrat_SemiBold,
    fontSize: MeasurementCalculator.measureFont(16),
    color: colors.purple2
  },
  timeHour: {
    fontFamily: fonts.Montserrat_Bold,
    fontSize: MeasurementCalculator.measureFont(10),
    color: colors.black,
    backgroundColor: colors.lightRed3,
    borderRadius: 8,
    textAlign: 'center',
    paddingVertical: MeasurementCalculator.measure(6.4),
    paddingHorizontal: MeasurementCalculator.measure(12.7, 'w')
  },
  dateTime: {
    fontFamily: fonts.Montserrat_Bold,
    fontSize: MeasurementCalculator.measureFont(10),
    color: colors.black,
    backgroundColor: colors.lightYellow,
    borderRadius: 8,
    textAlign: 'center',
    marginLeft: MeasurementCalculator.measure(13.7, 'w'),
    paddingVertical: MeasurementCalculator.measure(6.4),
    paddingHorizontal: MeasurementCalculator.measure(10.2, 'w')
  },
  meetingLocation: {
    fontFamily: fonts.Montserrat_SemiBold,
    fontSize: MeasurementCalculator.measureFont(16),
    color: colors.purple2,
    paddingTop: MeasurementCalculator.measure(20.5),
    paddingBottom: MeasurementCalculator.measure(7),
    paddingRight: MeasurementCalculator.measure(12, 'w')
  },
  meetingLocationDetails: {
    fontFamily: fonts.Montserrat_Regular,
    fontSize: MeasurementCalculator.measureFont(12),
    color: colors.black,
    lineHeight: 17,
    paddingBottom: MeasurementCalculator.measure(11.5),
  },
})

export default MeetingDetails;
