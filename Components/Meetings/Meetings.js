import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import mainStyles from '../../css/MainStyle';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import OverlayButton from '../Helpers/OverlayButton';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

import MeetingItem from './MeetingItem';

class Meetings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Meetings: [
        {
          mettingName: 'Meeting Name',
          time: '30.9.2019',
          hour: '15:30',
          calander: 1,
        },
        {
          mettingName: 'Meeting Name',
          time: '30.9.2019',
          hour: '15:30',
        },
        {
          mettingName: 'Meeting Name',
          time: '2019-10-05',
          hour: '15:30',
          calander: 1,
        },
        {
          mettingName: 'Meeting Name',
          time: '22.7.2019',
          hour: '15:30',
        },
        {
          mettingName: 'Meeting Name',
          time: '22.7.2019',
          hour: '15:30',
          calander: 1,
        },
        {
          mettingName: 'Meeting Name',
          time: '22.7.2019',
          hour: '15:30',
          calander: 1,
        }
      ]
    };
  }

  render() {
    let noMeetings = <View style={[mainStyles.flex, { justifyContent: 'center', alignItems: 'center' }]}><Text style={styles.textNoMeetings}>You have no meetings</Text></View>
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <FlatList
          data={this.state.Meetings}
          renderItem={({ item, index }) =>
            <MeetingItem item={item} index={index} navigation={this.props.navigation} />
          }
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          contentContainerStyle= {{paddingVertical: MeasurementCalculator.measureFont(35)}}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingLeft: MeasurementCalculator.measureFont(20, 'w'),
    paddingRight: MeasurementCalculator.measureFont(19, 'w'),
    backgroundColor: colors.white,
  },
  textNoMeetings: {
    fontFamily: fonts.Montserrat_SemiBold,
    fontSize: MeasurementCalculator.measureFont(18),
    textAlign: "center",
    color: colors.gray4
  }
});
export default Meetings;
