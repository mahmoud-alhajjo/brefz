import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Image
} from 'react-native';
import FontawesomeIcon from '../../css/FontawesomeIcon';

import mainStyles from '../../css/MainStyle';


export default class OverlayButton extends Component {

  constructor(props) {
    super(props);
    this.state = {pressStatus: false};
  }

  onHideUnderlay() {
    this.setState({pressStatus: false});
  }

  onShowUnderlay() {
    this.setState({pressStatus: true});
  }

  clickButton = () => {
    this.props.clickButton()
  }

  renderButton = () => {
    pressStyle = {backgroundColor: this.props.buttonTextColor, opacity:this.props.buttonTextColorOpacity ? this.props.buttonTextColorOpacity : 1, borderWidth:1, borderColor: this.props.buttonBackgroundColor}
    nonPressStyle = {backgroundColor: this.props.buttonBackgroundColor, borderWidth:0, opacity:this.props.buttonBackgroundColorOpacity ? this.props.buttonBackgroundColorOpacity : 1}
    textPressStyle = {color: this.props.buttonBackgroundColor}
    textNonPressStyle = {color: this.props.buttonTextColor}
    customStyle = this.props.customStyle

    return (
        <TouchableHighlight
          disabled={this.props.disabled ? this.props.disabled : false}
          activeOpacity={1}
          onPress={this.clickButton}
          underlayColor = {this.props.buttonTextColor}
          onHideUnderlay={this.onHideUnderlay.bind(this)}
          onShowUnderlay={this.onShowUnderlay.bind(this)}
          style={[customStyle ? customStyle : styles.nextButton, (this.state.pressStatus ? pressStyle : nonPressStyle )]}
          >
           <View style={[mainStyles.rowFlex, styles.buttonInfo]}>
            {this.props.source ? <Image style={[this.props.iconStyle, {tintColor: (this.state.pressStatus ? this.props.buttonBackgroundColor : this.props.buttonTextColor)}]} source={this.props.source}/> : null}
            {this.props.nameIcon ? <FontawesomeIcon nameIcon={this.props.nameIcon} size={this.props.size} customStyle={this.props.iconStyle} color={this.state.pressStatus ? this.props.buttonBackgroundColor : this.props.buttonTextColor} /> : null}
            <Text style={[this.props.textButtonStyle ? this.props.textButtonStyle : styles.nextButtonText , this.state.pressStatus ? textPressStyle : textNonPressStyle]}>
              {this.props.text}
            </Text>
          </View>
        </TouchableHighlight>
    );
  }

  render() {
    return(
      this.renderButton()
    )
  }
}


const styles = StyleSheet.create({
  nextButton: {
    height: 48,
    justifyContent: 'center',
    borderRadius:100,
  },
  nextButtonText: {
    fontSize: 13,
    fontWeight: "bold",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "center",
  },
  buttonInfo: {
    justifyContent: 'center',
    alignItems: 'center',
  }
});
