import React, { Component } from 'react';
import {
  PixelRatio,
  Dimensions
} from 'react-native';

export default class MeasurementCalculator {

  static measure(input, type = 'h') {
    if(type == 'w') {
      return (input / 375) * Dimensions.get('window').width
    } else {
      return (input / 812) * Dimensions.get('window').height
    }
  }

  static measureFont(size) {
    const baseWidth = 375;
    const baseHeight = 812;
    
    const scaleWidth = Dimensions.get('window').width / baseWidth;
    const scaleHeight = Dimensions.get('window').height / baseHeight;

    const moderateScaleWidth =  size + ( (scaleWidth * size) - size ) * 0.5;
    const moderateScaleHeight = size + ( (scaleHeight * size) - size ) * 0.5;
    const scale = Math.min(moderateScaleWidth, moderateScaleHeight);

    return scale;
  }
}
