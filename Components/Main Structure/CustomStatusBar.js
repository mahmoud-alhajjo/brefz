/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StatusBar
} from 'react-native';

import colors from '../../css/colors';

export default class CustomStatusBar extends Component {

  constructor(props) {
    super(props);
  }

  getBackgroundColor = () => {
    if(this.props.routeName) {
      if(this.props.routeName == "Login" || this.props.routeName == "Account" || this.props.routeName == "UpdateProfile" || this.props.routeName == "ChangePassword" || this.props.routeName == "ChangeEmail" || this.props.routeName == "PrivacyPolicy" || this.props.routeName == "TermsAndCondition" || this.props.routeName == "MeetingDetails" || this.props.routeName == "SupportingFileSingle" || this.props.routeName == "PaymentSingle" || this.props.routeName == "PhaseSingle" || this.props.routeName == "AllPayments" || this.props.routeName == "AllPhases" || this.props.routeName == "AllSupportingFiles") {
        return colors.lightBlue
      } else if(this.props.routeName == "ForgetPassword" || this.props.routeName == "Dashboard"  || this.props.routeName == "Meetings"  || this.props.routeName == "Timeline"  || this.props.routeName == "Notifications" || this.props.routeName == "AllProjects") {
        return colors.yellow
      } else {
        return colors.white
      }
    }
    return colors.white
  }

  getBarStyle = () => {
    if(this.props.isDark) {
      return 'dark-content'
    }
    return 'light-content'
  }

  setHidden = () => {
    if(this.props.isHidden) {
      return true
    }
    return false
  }

  render() {
    return (
      <StatusBar
        backgroundColor = {this.getBackgroundColor()}
        barStyle= {this.getBarStyle()}
        animated={true}
        hidden={this.setHidden()}
      />
    );
  }
}
