import React, { Component } from 'react';
import { View } from 'react-native';
import colors from '../../css/colors';
import mainStyles from '../../css/MainStyle';

import HeaderOnBoarding from '../Splash+Login/HeaderOnBoarding';
import Splash from '../Splash+Login/Splash';
import OnBoarding from '../Splash+Login/OnBoarding';
import Login from '../Splash+Login/Login';
import ForgetPassword from '../Splash+Login/ForgetPassword';
import NotificationMessage from '../Splash+Login/NotificationMessage';
import EnableFingerPrint from '../Splash+Login/EnableFingerPrint';
import Dashboard from '../Dashboard/Dashboard';
import Meetings from '../Meetings/Meetings';
import Notifications from '../Notifications/Notifications';
import Timeline from '../Timeline/Timeline';
import Account from '../Account/Account';
import UpdateProfile from '../Account/UpdateProfile';
import ChangePassword from '../Account/ChangePassword';
import ChangeEmail from '../Account/ChangeEmail';
import PrivacyPolicy from '../Account/PrivacyPolicy';
import TermsAndCondition from '../Account/TermsAndCondition';
import MeetingDetails from '../Meetings/MeetingDetails';
import AllSupportingFiles from '../SupportingFiles/AllSupportingFiles';
import SupportingFileSingle from '../SupportingFiles/SupportingFileSingle';
import PaymentSingle from '../Payments/PaymentSingle';
import AllPayments from '../Payments/AllPayments';
import PhaseSingle from '../Phases/PhaseSingle';
import AllPhases from '../Phases/AllPhases';
import AllProjects from '../Dashboard/AllProjects';

import { transitionConfig } from './ScreenTransitionConfig';
import TabBar from './TabBar';
import Header from './Header';

import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation-tabs';
import BlurOverlay from 'react-native-blur-overlay';

const TabNavigatorScreens = {
  Dashboard: {
    screen: Dashboard,
  },
  Meetings: {
    screen: Meetings,
  },
  Timeline: {
    screen: Timeline,
  },
  Notifications: {
    screen: Notifications,
  },
  Account: createStackNavigator({
    Account: {
      screen: Account,
      navigationOptions: ({ navigation }) => ({
        header: null,
      })
    },
    UpdateProfile: {
      screen: UpdateProfile,
      navigationOptions: ({ navigation }) => ({
        header: null,
      }),
    },
    ChangePassword: {
      screen: ChangePassword,
      navigationOptions: ({ navigation }) => ({
        header: null,
      }),
    },
    ChangeEmail: {
      screen: ChangeEmail,
      navigationOptions: ({ navigation }) => ({
        header: null,
      }),
    },
    PrivacyPolicy: {
      screen: PrivacyPolicy,
      navigationOptions: ({ navigation }) => ({
        header: null,
      }),
    },
    TermsAndCondition: {
      screen: TermsAndCondition,
      navigationOptions: ({ navigation }) => ({
        header: null,
      }),
    },
  },
    {
      defaultNavigationOptions: {
        headerStyle: {
          backgroundColor: 'transparent',
          height: 0,
          elevation: 0,
          shadowOpacity: 0,
          borderBottomWidth: 0,
        },
      },
      headerLeft: null,
      mode: 'card',
      initialRouteName: 'Account',
      headerMode: Platform.OS === 'ios' ? 'float' : 'screen',
      transitionConfig: Platform.OS === 'ios' ? null : transitionConfig,
    })
}

const TabNavigatorOptions = {
  tabBarComponent: ({ navigation }) => <TabBar navigation={navigation} />,
  backBehavior: 'none',
  animationEnabled: true,
  swipeEnabled: false,
  lazy: true
}
const tabNavigator = createBottomTabNavigator(TabNavigatorScreens, TabNavigatorOptions)

const AppNavigator = createStackNavigator({
  Splash: {
    screen: Splash,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  OnBoarding: {
    screen: OnBoarding,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  Login: {
    screen: Login,
    navigationOptions: ({ navigation }) => ({
      header: <HeaderOnBoarding navigation={navigation} />
    }),
  },
  ForgetPassword: {
    screen: ForgetPassword,
    navigationOptions: ({ navigation }) => ({
      header: <HeaderOnBoarding navigation={navigation} />
    }),
  },
  NotificationMessage: {
    screen: NotificationMessage,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  EnableFingerPrint: {
    screen: EnableFingerPrint,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  AppDashboard: {
    screen: tabNavigator,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} />
    }),
  },
  MeetingDetails: {
    screen: MeetingDetails,
    navigationOptions: ({ navigation }) => ({
      header: null
    }),
  },
  SupportingFileSingle: {
    screen: SupportingFileSingle,
    navigationOptions: ({ navigation }) => ({
      header: null
    }),
  },
  PaymentSingle: {
    screen: PaymentSingle,
    navigationOptions: ({ navigation }) => ({
      header: null
    }),
  },
  PhaseSingle: {
    screen: PhaseSingle,
    navigationOptions: ({ navigation }) => ({
      header: null
    }),
  },
  AllPayments: {
    screen: AllPayments,
    navigationOptions: ({ navigation }) => ({
      header: null
    }),
  },
  AllPhases: {
    screen: AllPhases,
    navigationOptions: ({ navigation }) => ({
      header: null
    }),
  },
  AllSupportingFiles: {
    screen: AllSupportingFiles,
    navigationOptions: ({ navigation }) => ({
      header: null
    }),
  },
  AllProjects: {
    screen: AllProjects,
    navigationOptions: ({ navigation }) => ({
      header: null
    }),
  },
},
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: 'transparent',
        height: 0,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
      },
    },
    headerLeft: null,
    mode: 'card',
    initialRouteName: 'Splash',
    headerMode: Platform.OS === 'ios' ? 'float' : 'screen',
    transitionConfig: Platform.OS === 'ios' ? null : transitionConfig,
    transparentCard: true,
  },
);

const MainNavigator = createAppContainer(AppNavigator);

class MainApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={[mainStyles.flex, { backgroundColor: colors.white }]}>
        <MainNavigator />
        <BlurOverlay
          radius={20}
          downsampling={78}
          brightness={6}
          customStyles={mainStyles.flex}
          blurStyle="light"
        />
      </View>
    );
  }
}

export default MainApp;
