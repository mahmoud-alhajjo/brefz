import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import mainStyles from '../../css/MainStyle';

import MeasurementCalculator from '../Helpers/MeasurementCalculator';
import * as Animatable from 'react-native-animatable';


class CommentItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        let insertAnimatedComment = ''
        if (this.props.index == this.props.lastItem) {
            insertAnimatedComment = 'bounceInRight' 
        }

        let typeComment = null
        if (this.props.item.type == 'user') {
            typeComment =
                <View>
                    <View style={[mainStyles.rowFlex, styles.writerDetails, { justifyContent: 'flex-start' }]}>
                        <Image style={styles.imageStyle} resizeMode='contain' source={this.props.item.image} />
                        <View style={styles.writerOfComment}>
                            <Text style={styles.nameWriterOfComment}>{this.props.item.name}</Text>
                            <Text style={styles.dateComment}>8 Nov</Text>
                        </View>
                    </View>
                    <Animatable.Text style={styles.Comment1} animation={insertAnimatedComment} easing="linear" duratio={350}>{this.props.item.comment}</Animatable.Text>
                </View>
        } else {
            typeComment =
                <View>
                    <View style={[mainStyles.rowFlex, styles.writerDetails, { justifyContent: 'flex-end' }]}>
                        <Image style={styles.imageStyle} resizeMode='contain' source={this.props.item.image} />
                        <View style={styles.writerOfComment}>
                            <Text style={styles.nameWriterOfComment}>{this.props.item.name}</Text>
                            <Text style={styles.dateComment}>8 Nov</Text>
                        </View>
                    </View>
                    <Text style={styles.Comment2}>{this.props.item.comment}</Text>
                </View>
        }

        return typeComment;
    }
}

const styles = StyleSheet.create({
    writerDetails: {
        alignItems: 'center',
        paddingLeft: MeasurementCalculator.measure(26, 'w'),
        paddingRight: MeasurementCalculator.measure(53.1, 'w'),
    },
    imageStyle: {
        height: MeasurementCalculator.measure(26.94),
        width: MeasurementCalculator.measure(26.94),
        borderRadius: 50,
    },
    writerOfComment: {
        paddingLeft: MeasurementCalculator.measure(15, 'w'),
        paddingBottom: MeasurementCalculator.measure(1),
    },
    nameWriterOfComment: {
        fontFamily: fonts.Montserrat_Bold,
        fontSize: MeasurementCalculator.measureFont(11),
        color: colors.black,
    },
    dateComment: {
        fontFamily: fonts.Montserrat_Regular,
        fontSize: MeasurementCalculator.measureFont(8),
        color: colors.gray9,
    },
    Comment1: {
        fontFamily: fonts.Montserrat_Medium,
        fontSize: MeasurementCalculator.measureFont(12),
        color: colors.black,
        lineHeight: 20,
        backgroundColor: colors.gray10,
        borderTopLeftRadius: 50,
        borderBottomLeftRadius: 50,
        marginTop: MeasurementCalculator.measure(12),
        marginBottom: MeasurementCalculator.measure(35.1),
        marginLeft: MeasurementCalculator.measure(26, 'w'),
        paddingVertical: MeasurementCalculator.measure(14.5),
        paddingLeft: MeasurementCalculator.measure(24.5, 'w'),
        paddingRight: MeasurementCalculator.measure(14.8, 'w'),
    },
    Comment2: {
        fontFamily: fonts.Montserrat_Medium,
        fontSize: MeasurementCalculator.measureFont(12),
        color: colors.white,
        lineHeight: 20,
        backgroundColor: colors.lightBlue,
        borderTopRightRadius: 50,
        borderBottomRightRadius: 50,
        marginTop: MeasurementCalculator.measure(12),
        marginBottom: MeasurementCalculator.measure(35.1),
        marginRight: MeasurementCalculator.measure(53.1, 'w'),
        paddingVertical: MeasurementCalculator.measure(14.5),
        paddingRight: MeasurementCalculator.measure(24.5, 'w'),
        paddingLeft: MeasurementCalculator.measure(14.8, 'w'),
    },
})

export default CommentItem;
