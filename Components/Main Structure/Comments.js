import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Image, TextInput } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CommentItem from './CommentItem';

class Comments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: {
                type: 'user',
                name: 'Jack Snow',
                time: '20/7/2017',
                image: require('../../Images/Oval.png'),
                comment: null,
            },
            height: MeasurementCalculator.measure(33.02),
            arrayHolder: [],
        };

        comments = [
            {
                type: 'user',
                name: 'Jack Snow',
                time: '20/7/2017',
                image: require('../../Images/Oval.png'),
                comment: 'When I was 5 years old, my mother...'
            },
            {
                type: 'projectManager',
                name: 'Marie Carter',
                time: '23/7/2017',
                image: require('../../Images/imgeNotification.png'),
                comment: 'When I was 5 years old, my mother always told me that happiness'
            },
            {
                type: 'user',
                name: 'Jack Snow',
                time: '20/7/2017',
                image: require('../../Images/Oval.png'),
                comment: 'When I was 5 years old, my mother...'
            },
            {
                type: 'projectManager',
                name: 'Marie Carter',
                time: '23/7/2017',
                image: require('../../Images/imgeNotification.png'),
                comment: 'When I was 5 years old, my mother always told me that happiness'
            },
        ]
    }

    componentDidMount() {
        this.setState({ arrayHolder: [...comments] })
    }

    insertNewComment = () => {
        comments.push(this.state.comment);
        this.setState({ arrayHolder: [...comments] })
        this.setState(prevState => {
            let comment = Object.assign({}, prevState.comment);
            comment.comment = null;
            return { comment };
        })
    }

    render() {
        let customBorderBottomRadius = this.props.customBorderBottomRadius ? {borderBottomLeftRadius: 13, borderBottomRightRadius: 13 }: ''              
        return (
            <View style={[styles.containerComments, mainStyles.flex, customBorderBottomRadius, { justifyContent: 'flex-end' }]}>
                <FlatList
                    ref="flatList"
                    data={comments}
                    ListHeaderComponent={<Text style={styles.title}>Comments</Text>}
                    renderItem={({ item, index }) =>
                        <CommentItem item={item} index={index} lastItem={comments.length - 1}/>
                    }
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                    onContentSizeChange={() => setTimeout(() => this.refs.flatList.scrollToEnd(), 250)}
                />
                <View style={[styles.inputCommentSection, customBorderBottomRadius]}>
                    <View style={[mainStyles.rowFlex, styles.inputIcons]}>
                        <TouchableOpacity style={styles.clickCamera}>
                            <FontawesomeIcon nameIcon={icons.Camera} color={colors.darkGray3} size={MeasurementCalculator.measureFont(16)} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.clickgallery}>
                            <Image style={styles.galleryImge} resizeMode='contain' source={require('../../Images/gallery.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={[mainStyles.rowFlex, styles.commentInputContainer]}>
                        <View style={[styles.commentInput, { height: Math.max(MeasurementCalculator.measure(33.02), this.state.height) }]}>
                            <TextInput
                                ref="commentInput"
                                style={[styles.textInput]}
                                placeholder={"TEXT OF COMMENT"}
                                underlineColorAndroid={"transparent"}
                                placeholderTextColor={colors.lightBlack}
                                multiline={true}
                                autoCapitalize={"none"}
                                autoCorrect={false}
                                blurOnSubmit={false}
                                value={this.state.comment.comment}
                                onChangeText={(text) => {
                                    this.setState(prevState => {
                                        let comment = Object.assign({}, prevState.comment);
                                        comment.comment = text;
                                        return { comment };
                                    })
                                }}
                                onContentSizeChange={(event) => {
                                    let margin = MeasurementCalculator.measure(29)
                                    let finalHeight = event.nativeEvent.contentSize.height + margin
                                    this.setState({ height: finalHeight > MeasurementCalculator.measure(75) ? MeasurementCalculator.measure(75) : finalHeight })
                                }}
                                onSubmitEditing={(event) => this.refs.commentInput.blur()}
                            />
                        </View>
                        <TouchableOpacity style={styles.clickSend} onPress={this.insertNewComment} >
                            <Image style={styles.sendImge} resizeMode='contain' source={require('../../Images/send.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerComments: {
        backgroundColor: colors.white,
        borderBottomLeftRadius: 13, borderBottomRightRadius: 13,
    },
    title: {
        fontFamily: fonts.Montserrat_Bold,
        fontSize: MeasurementCalculator.measureFont(20),
        color: colors.black,
        paddingLeft: MeasurementCalculator.measure(26, 'w'),
        paddingRight: MeasurementCalculator.measure(40, 'w'),
        paddingTop: MeasurementCalculator.measure(38.2),
        paddingBottom: MeasurementCalculator.measure(15.3),
        marginBottom: MeasurementCalculator.measure(24.5),
    },
    inputCommentSection: {
        borderBottomLeftRadius: 13, borderBottomRightRadius: 13,
        backgroundColor: colors.white2,
        paddingBottom: MeasurementCalculator.measure(16.5),
    },
    inputIcons: {
        alignItems: 'center',
    },
    clickCamera: {
        paddingLeft: MeasurementCalculator.measure(16.5, 'w'),
        paddingRight: MeasurementCalculator.measure(9, 'w'),
        paddingVertical: MeasurementCalculator.measure(10)
    },
    clickgallery: {
        paddingLeft: MeasurementCalculator.measure(9, 'w'),
        paddingRight: MeasurementCalculator.measure(16.5, 'w'),
        paddingVertical: MeasurementCalculator.measure(10)
    },
    galleryImge: {
        width: MeasurementCalculator.measure(15.59, 'w'),
        height: MeasurementCalculator.measure(15.59)
    },
    commentInputContainer: {
        alignItems: 'center',
        paddingLeft: MeasurementCalculator.measure(7.3, 'w'),
        paddingRight: MeasurementCalculator.measure(15.6, 'w'),
    },
    commentInput: {
        flex: 1,
        borderRadius: 50,
        paddingHorizontal: MeasurementCalculator.measure(14.7, 'w'),
        paddingVertical: MeasurementCalculator.measure(11.5),
        borderStyle: "solid",
        borderWidth: 0.7,
        borderColor: colors.gray10,
    },
    textInput: {
        paddingVertical: 0,
        fontSize: MeasurementCalculator.measureFont(12),
        letterSpacing: 0,
        fontFamily: fonts.Karla_Regular,
        color: colors.black,
    },
    clickSend: {
        paddingVertical: MeasurementCalculator.measure(8),
        paddingLeft: MeasurementCalculator.measure(21.1, 'w')
    },
    sendImge: {
        width: MeasurementCalculator.measure(19.26, 'w'),
        height: MeasurementCalculator.measure(16.51)
    },
})

export default Comments;
