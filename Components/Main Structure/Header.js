import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity, Image, Animated, PermissionsAndroid } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import mainStyles from '../../css/MainStyle';
import OverlayButton from '../Helpers/OverlayButton';
import CustomStatusBar from '../Main Structure/CustomStatusBar';

import ImagePicker from 'react-native-image-picker';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animation: new Animated.Value(67)
        };
    }

    startAnimation = () => {
        let screenOptions = this.getScreeTitleAndBackground();
        Animated.timing(this.state.animation, {
            toValue: screenOptions.heightHeader,
            duration: screenOptions.durationAnimation
        }).start();
    }

    getScreeTitleAndBackground = () => {
        let obj = { screenName: "", title: '', color: null, iconName: '', textPaddingLeft: 0, heightHeader: 67, durationAnimation: 250, displayHeader: 'flex' }
        let routeName = this.props.navigation.state.routeName
        if (routeName == "AppDashboard") {
            let index = this.props.navigation.state.index
            switch (index) {
                case 0:
                    obj.screenName = "Dashboard"
                    obj.title = "Dashboard"
                    obj.color = colors.yellow
                    obj.iconName = icons.Th_list,
                    obj.displayHeader = 'none'
                    break;
                case 1:
                    obj.screenName = "Meetings"
                    obj.title = "Meetings"
                    obj.color = colors.yellow
                    obj.textPaddingLeft = 33
                    break;
                case 2:
                    obj.screenName = "Timeline"
                    obj.title = "Timeline"
                    obj.color = colors.yellow
                    obj.textPaddingLeft = 33
                    break;
                case 3:
                    obj.screenName = "Notifications"
                    obj.title = "Notifications"
                    obj.color = colors.yellow
                    obj.textPaddingLeft = 33
                    break;
                case 4:

                    let indexAccountRoute = this.props.navigation.state.routes[index].index
                    let routeNameAccount = this.props.navigation.state.routes[index].routes[indexAccountRoute].routeName
                    obj.color = colors.lightBlue
                    obj.heightHeader = 67
                    obj.durationAnimation = 50
                    obj.iconName = icons.Arrow_Left
                    if (routeNameAccount == 'Account') {
                        obj.screenName = 'Account'
                        obj.title = "Your Account"
                        obj.color = colors.lightBlue
                        obj.textPaddingLeft = 27
                        obj.heightHeader = 137
                        obj.durationAnimation = 200
                    } else if (routeNameAccount == 'UpdateProfile') {
                        obj.screenName = routeNameAccount
                        obj.title = "Update Profile"
                    } else if (routeNameAccount == 'ChangeEmail') {
                        obj.screenName = routeNameAccount
                        obj.title = "Change Email"
                    } else if (routeNameAccount == 'ChangePassword') {
                        obj.screenName = routeNameAccount
                        obj.title = "Change password"
                    } else if (routeNameAccount == 'PrivacyPolicy') {
                        obj.screenName = routeNameAccount
                        obj.title = "Privacy policy"
                    } else if (routeNameAccount == 'TermsAndCondition') {
                        obj.screenName = routeNameAccount
                        obj.title = "Terms & Condition"
                    }
                    break;
                default:
                    obj.screenName = ""
                    break
            }
        }
        return obj;
    }

    updateProfileAction = () => {
        this.props.navigation.navigate('UpdateProfile');
    }

    accountAction = () => {
        this.props.navigation.navigate('Account');
    }

    allProjectsAction = () => {
        this.props.navigation.navigate('AllProjects');
    }

    openImageGallery = () => {
        let options = {
            title: 'Choose a profile picture',
            takePhotoButtonTitle: 'take a picture',
            chooseFromLibraryButtonTitle: "Choose from the gallery",
            cancelButtonTitle: "cancel",
            mediaType: 'photo',
            allowsEditing: true,
            maxWidth: 400,
            maxHeight: 200,
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else { }
        });
    }

    selectProfilePic = async () => {
        try {
            let granted = await PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.CAMERA, PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE])
            if ((granted[PermissionsAndroid.PERMISSIONS.CAMERA] === PermissionsAndroid.RESULTS.GRANTED) && (granted[PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE] === PermissionsAndroid.RESULTS.GRANTED)) {
                this.openImageGallery()
            } else {
                alert('Please give permission to the camera')
            }
        } catch (err) {
        }
    }

    render() {
        const animationStyle = { height: this.state.animation }
        let screenOptions = this.getScreeTitleAndBackground();
        let leftIcon = null
        let rightButton = null
        let imageAccount = null
        let updateProfile = null
        let imageScreenAccount = null

        this.startAnimation()

        if (screenOptions.screenName == "Dashboard") {
            leftIcon =
                <TouchableOpacity style={styles.leftIcon} onPress={this.allProjectsAction}>
                    <FontawesomeIcon nameIcon={screenOptions.iconName} color={colors.white} size={17} customStyle={{ paddingTop: 5 }} />
                </TouchableOpacity>

            rightButton =
                <View style={styles.buttonView}>
                    <OverlayButton
                        buttonBackgroundColor={colors.white}
                        buttonTextColor={colors.lightBlue2}
                        clickButton={() => { }}
                        style={{ flex: 1 }}
                        text={' PAYMENTS'}
                        nameIcon={icons.Money}
                        size={10}
                        customStyle={styles.PaymentsButton}
                        textButtonStyle={styles.PaymentsTextButton} />
                </View>
        } else if (screenOptions.screenName == "Account") {
            updateProfile =
                <View>
                    <Text style={[styles.textHello, { paddingLeft: screenOptions.textPaddingLeft }]}>Hello, <Text style={styles.nameProfile}>Youness</Text></Text>
                    <TouchableOpacity style={mainStyles.rowFlex} onPress={this.updateProfileAction}>
                        <Text style={[styles.textUpdateProfile, { paddingLeft: screenOptions.textPaddingLeft }]}>Update Profile  </Text>
                        <Image style={styles.penIcon} source={require('../../Images/pen.png')} />
                    </TouchableOpacity>
                </View>

            imageScreenAccount =
                <TouchableOpacity onPress={this.updateProfileAction}>
                    <Image style={styles.imageScreenAccount} resizeMode='contain' source={require('../../Images/Oval.png')} />
                </TouchableOpacity>
        } else if (screenOptions.screenName == "UpdateProfile") {
            leftIcon =
                <TouchableOpacity style={[styles.updateProfileIcon]} onPress={this.accountAction}>
                    <FontawesomeIcon nameIcon={screenOptions.iconName} color={colors.white} size={28} />
                </TouchableOpacity>

            imageAccount =
                <TouchableOpacity style={styles.imageStyleView} onPress={this.selectProfilePic}>
                    <Image style={styles.imageStyle} resizeMode='contain' source={require('../../Images/Oval.png')} />
                    <View style={styles.containerPen}>
                        <Image style={styles.penIconUpdateProfile} resizeMode={'contain'} source={require('../../Images/pen.png')} />
                    </View>
                </TouchableOpacity>
        } else if (screenOptions.screenName == "ChangePassword" || screenOptions.screenName == "ChangeEmail" || screenOptions.screenName == "PrivacyPolicy" || screenOptions.screenName == "TermsAndCondition") {
            leftIcon =
                <TouchableOpacity style={[styles.updateProfileIcon]} onPress={this.accountAction}>
                    <FontawesomeIcon nameIcon={screenOptions.iconName} color={colors.white} size={28} />
                </TouchableOpacity>
        } else {
            imageAccount =
                <TouchableOpacity style={styles.imageStyleView} >
                    <Image style={styles.imageStyle} resizeMode='contain' source={require('../../Images/Oval.png')} />
                </TouchableOpacity>
        }

        return (
            <View style={{ backgroundColor: colors.white, display: screenOptions.displayHeader }}>
                <CustomStatusBar routeName={screenOptions.screenName} />
                <SafeAreaView style={{ overflow: 'hidden', borderBottomRightRadius: 80, backgroundColor: screenOptions.color }}>
                    <Animated.View style={[styles.header, mainStyles.rowFlex, animationStyle, { backgroundColor: screenOptions.color }]}>
                        <View style={[mainStyles.rowFlex, { paddingTop: 17 }]}>
                            {leftIcon}
                            <View style={[mainStyles.columnFlex]}>
                                <Text style={[styles.textHeader, { paddingLeft: screenOptions.textPaddingLeft }]}>{screenOptions.title}</Text>
                                {updateProfile}
                            </View>
                        </View>
                        {imageScreenAccount}
                        {rightButton}
                    </Animated.View>
                </SafeAreaView>
                {imageAccount}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        justifyContent: 'space-between',
    },
    imageStyleView: {
        position: 'absolute',
        bottom: 0,
        right: 14.5,
        width: 44,
        height: 44,
        borderRadius: 50
    },
    imageStyle: {
        height: 44,
        width: 44,
        borderRadius: 50,
    },
    textHeader: {
        fontSize: 19,
        fontFamily: fonts.Montserrat_SemiBold,
        color: colors.white,
    },
    buttonView: {
        marginRight: 40.4,
        marginTop: 17,
    },
    PaymentsButton: {
        height: 27,
        width: 106.13,
        justifyContent: 'center',
        borderRadius: 29,
    },
    PaymentsTextButton: {
        fontFamily: fonts.Montserrat_Medium,
        fontSize: 10,
        letterSpacing: 1,
    },
    imageScreenAccount: {
        marginRight: 32.5,
        marginTop: 48.5,
        height: 67,
        width: 67,
        borderRadius: 50
    },
    leftIcon: {
        height: 40,
        paddingLeft: 27,
        paddingRight: 9.5
    },
    textHello: {
        color: colors.white,
        marginTop: 28,
        fontSize: 20,
        fontFamily: fonts.Helvetica_Neue
    },
    nameProfile: {
        fontFamily: fonts.Helvetica_Neue,
        fontWeight: 'bold'
    },
    textUpdateProfile: {
        paddingBottom: 10,
        marginTop: 6.9,
        fontSize: 16,
        fontFamily: fonts.Karla_Regular,
        color: colors.black3
    },
    penIcon: {
        height: 12.88,
        width: 12.88,
        marginTop: 10
    },
    updateProfileIcon: {
        height: 40,
        paddingLeft: 26,
        paddingRight: 20
    },
    penIconUpdateProfile: {
        width: 14.12,
        height: 14.13,
        tintColor: colors.white
    },
    containerPen: {
        backgroundColor: colors.darkBlue1,
        position: 'absolute',
        width: 44,
        height: 44,
        borderRadius: 50,
        opacity: 0.7,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default Header;
