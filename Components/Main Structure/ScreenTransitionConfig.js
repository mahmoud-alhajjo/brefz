import { Easing, Animated } from 'react-native';

export const transitionConfig = () => {
  return {
    transitionSpec: {
      duration: 500,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: sceneProps => {
      const { layout, position, scene } = sceneProps

      const thisSceneIndex = scene.index
      const width = layout.initWidth
      let startDirection = width
      let opacity = null

      if (sceneProps.scene.route.routeName == "MeetingDetails" || sceneProps.scene.route.routeName == "SupportingFileSingle" || sceneProps.scene.route.routeName == "PaymentSingle" || sceneProps.scene.route.routeName == "PhaseSingle" || sceneProps.scene.route.routeName == "AllPayments" || sceneProps.scene.route.routeName == "AllPhases" || sceneProps.scene.route.routeName == "AllSupportingFiles" || sceneProps.scene.route.routeName == "AllProjects") {
        startDirection = 0
        opacity = position.interpolate({
          inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
          outputRange: [0, 1, 1],
        })
      }

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
        outputRange: [startDirection, 0, 0],
      })

      return { opacity, transform: [{ translateX }] }
    },
  }
}