import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, Image, StyleSheet, Dimensions, Animated } from 'react-native';
import colors from '../../css/colors';
import * as icons from '../../css/icons';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import mainStyles from '../../css/MainStyle';

class TabBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            animation: new Animated.Value(24.4)
        };
    }

    startAnimation = (index) => {
        Animated.timing(this.state.animation, {
            toValue: 24.4 + (((Dimensions.get('window').width - 46) / 5) * index),
            duration: 300
        }).start();
    }

    next = (index) => {
        let screenToNavigate = ""
        switch (index) {
            case 0:
                screenToNavigate = "Dashboard"
                break;
            case 1:
                screenToNavigate = "Meetings"
                break;
            case 2:
                screenToNavigate = "Timeline"
                break;
            case 3:
                screenToNavigate = "Notifications"
                break;
            case 4:
                screenToNavigate = "Account"
                break;
            default:
                break;
        }
        this.props.navigation.navigate(screenToNavigate);
        this.setState({ index: index });
        this.startAnimation(index);
    }

    render() {
        const animationStyle = { left: this.state.animation }

        return (
            <SafeAreaView style={{ backgroundColor: colors.lightBlue }}>
                {/* <View style={{ height: 27, backgroundColor: colors.white }} /> */}
                <View style={[styles.container, mainStyles.rowFlex, { justifyContent: 'space-between' }]}>
                    <Animated.View style={[styles.activeIconView, animationStyle]} />
                    <TouchableOpacity style={styles.item} onPress={this.next.bind(this, 0)}>
                        <FontawesomeIcon nameIcon={icons.Tachometer_alt} color={colors.white} size={20} customStyle={{ textAlign: 'center' }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item} onPress={this.next.bind(this, 1)}>
                        <FontawesomeIcon nameIcon={icons.Handshake} color={colors.white} size={20} customStyle={{ textAlign: 'center' }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item} onPress={this.next.bind(this, 2)}>
                        <FontawesomeIcon nameIcon={icons.Sitemap} color={colors.white} size={20} customStyle={{ textAlign: 'center' }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item} onPress={this.next.bind(this, 3)}>
                        <FontawesomeIcon nameIcon={icons.Bell} color={colors.white} size={20} customStyle={{ textAlign: 'center' }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item} onPress={this.next.bind(this, 4)}>
                        <FontawesomeIcon nameIcon={icons.User} color={colors.white} size={20} customStyle={{ textAlign: 'center' }} />
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        height: 49,
        paddingHorizontal: 23,
        backgroundColor: colors.lightBlue,
    },
    item: {
        justifyContent: 'center',
        alignItems: 'center',
        width: ((Dimensions.get('window').width - 46) / 5),
        height: 49,
    },
    activeIconView: {
        position: 'absolute',
        backgroundColor: colors.yellow,
        top: -27,
        height: 110,
        width: ((Dimensions.get('window').width - 46) / 5) - 2.8,
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
    }
});


export default TabBar;
