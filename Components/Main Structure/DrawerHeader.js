import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import mainStyles from '../../css/MainStyle';
import OverlayButton from '../Helpers/OverlayButton';
import CustomStatusBar from '../Main Structure/CustomStatusBar';

import MeasurementCalculator from '../Helpers/MeasurementCalculator';

class DrawerHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    getScreeTitleAndBackground = () => {
        let routeName = this.props.navigation.state.routeName
        let obj = {
            screenName: routeName,
            iconName: icons.Arrow_Left,
            color: colors.lightBlue,
        }
        switch (routeName) {
            case "AllProjects":
                obj.color = colors.yellow;
                break;
            default:
                break;
        }
        return obj;
    }

    render() {
        let screenOptions = this.getScreeTitleAndBackground();
        let headerDetail = null
        let borderRadiusHeaderContainer = ''
        if (screenOptions.screenName == 'MeetingDetails' || screenOptions.screenName == 'PaymentSingle') {
            headerDetail = <Text style={[styles.textHeaderDetail]}>{this.props.title}</Text>
        } else if (screenOptions.screenName == 'SupportingFileSingle') {
            headerDetail =
                <View style={[mainStyles.rowFlex, { alignItems: 'center' }]}>
                    <View style={[styles.imageView]}>
                        <Image style={styles.imageStyle} resizeMode="contain" source={require('../../Images/solo.png')} />
                    </View>
                    <Text style={[styles.textHeader, { paddingLeft: MeasurementCalculator.measure(16.1, 'w') }]}><FontawesomeIcon nameIcon={icons.File_Pdf_O} color={colors.white} size={MeasurementCalculator.measureFont(16)} customStyle={{ fontWeight: '600' }} />  {this.props.title}</Text>
                </View>
        } else if (screenOptions.screenName == 'PhaseSingle') {
            headerDetail =
                <View style={[mainStyles.rowFlex, { alignItems: 'center' }]}>
                    <View style={[styles.imageView]}>
                        <Image style={styles.imageStyle} resizeMode="contain" source={require('../../Images/solo.png')} />
                    </View>
                    <View style={[mainStyles.rowFlex, { alignItems: 'center', paddingLeft: MeasurementCalculator.measure(16.1, 'w') }]}>
                        <Image style={{ width: MeasurementCalculator.measure(12.96, 'w'), height: MeasurementCalculator.measure(14.64) }} resizeMode="contain" tintColor={colors.white} source={require('../../Images/Phase.png')} />
                        <Text style={[styles.textHeader, { paddingLeft: 0 }]}> Design</Text>
                        <View style={[mainStyles.rowFlex, styles.pahseDone, { alignItems: 'center' }]}>
                            <View style={styles.circleDone} />
                            <Text style={styles.textDone}> DONE</Text>
                        </View>
                    </View>
                </View>
        } else if (screenOptions.screenName == 'AllProjects') {
            borderRadiusHeaderContainer = { borderBottomRightRadius: 80 }
            headerDetail =
                    <Text style={[styles.textHeader, { fontSize: MeasurementCalculator.measureFont(19), paddingLeft: 0 }]}>{this.props.title}</Text>
        } else {
            borderRadiusHeaderContainer = { borderBottomRightRadius: 80 }
            headerDetail =
                <View style={[mainStyles.rowFlex, { alignItems: 'center' }]}>
                    <View style={[styles.imageView]}>
                        <Image style={styles.imageStyle} resizeMode="contain" source={require('../../Images/solo.png')} />
                    </View>
                    <Text style={[styles.textHeader, { paddingLeft: MeasurementCalculator.measure(19, 'w') }]}>{this.props.title}</Text>
                </View>
        }

        return (
            <View style={[styles.headerContainer, borderRadiusHeaderContainer]}>
                <CustomStatusBar routeName={screenOptions.screenName} />
                <SafeAreaView style={[styles.header, mainStyles.rowFlex, { backgroundColor: screenOptions.color }]}>
                    <TouchableOpacity style={[styles.backIcon]} onPress={this.props.goBackAction}>
                        <FontawesomeIcon nameIcon={screenOptions.iconName} color={colors.white} size={MeasurementCalculator.measureFont(28)} />
                    </TouchableOpacity>
                    {headerDetail}
                </SafeAreaView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: colors.lightGray,
    },
    header: {
        borderBottomRightRadius: 80,
        alignItems: 'center',
        paddingVertical: MeasurementCalculator.measure(27.5),
        paddingLeft: MeasurementCalculator.measure(26, 'w'),
        paddingRight: MeasurementCalculator.measure(69.2, 'w'),
    },
    backIcon: {
        height: MeasurementCalculator.measure(40),
        paddingRight: MeasurementCalculator.measure(17, 'w'),
        paddingTop: MeasurementCalculator.measure(5),
    },
    textHeaderDetail: {
        fontSize: MeasurementCalculator.measureFont(16),
        fontFamily: fonts.Montserrat_Light,
        color: colors.purple2,
        backgroundColor: colors.white,
        paddingVertical: MeasurementCalculator.measure(11.5),
        paddingLeft: MeasurementCalculator.measure(23.8, 'w'),
        paddingRight: MeasurementCalculator.measure(67, 'w'),
        borderRadius: 18
    },
    textHeader: {
        fontSize: MeasurementCalculator.measureFont(16),
        fontFamily: fonts.Montserrat_SemiBold,
        color: colors.white,
        paddingLeft: MeasurementCalculator.measure(19, 'w'),
        paddingRight: MeasurementCalculator.measure(10, 'w'),
    },
    imageView: {
        backgroundColor: colors.white,
        borderRadius: 18,
        width: MeasurementCalculator.measure(44, 'w'),
        height: MeasurementCalculator.measure(42.15),
        alignItems: 'center',
        justifyContent: 'center',
    },
    imageStyle: {
        width: MeasurementCalculator.measure(21.99, 'w'),
        height: MeasurementCalculator.measure(23.16),
    },
    pahseDone: {
        marginLeft: MeasurementCalculator.measure(18, 'w'),
        paddingHorizontal: MeasurementCalculator.measure(7, 'w'),
        paddingVertical: MeasurementCalculator.measure(2.5),
        borderRadius: 5,
        backgroundColor: colors.lightGreen4,
    },
    circleDone: {
        width: MeasurementCalculator.measure(8),
        height: MeasurementCalculator.measure(8),
        backgroundColor: colors.green4,
        borderRadius: 50,
    },
    textDone: {
        fontSize: MeasurementCalculator.measureFont(12),
        fontFamily: fonts.Montserrat_Medium,
        color: colors.black,
    },
});

export default DrawerHeader;
