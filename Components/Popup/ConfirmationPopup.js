import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import mainStyles from '../../css/MainStyle';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import OverlayButton from '../Helpers/OverlayButton';
import MeasurementCalculator from '../Helpers/MeasurementCalculator'

class ConfirmationPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        let icon = null
        let description = this.props.description && <Text style={styles.description}>{this.props.description}</Text>
        let textBetweenButtons = this.props.textBetweenButtons && <Text style={styles.textBetweenButtons}>{this.props.textBetweenButtons}</Text>
        let secondButton =  this.props.secondButton && <View style={[styles.buttonView, { marginTop: MeasurementCalculator.measure(15) }]}>
            <OverlayButton
                buttonBackgroundColor={colors.white}
                buttonTextColor={colors.black}
                clickButton={this.props.cancel}
                style={{ flex: 1 }}
                text={this.props.secondButton}
                customStyle={styles.popupButton}
                textButtonStyle={styles.popupTextButton} />
        </View>
        if (this.props.icon == icons.Exclamation_Triangle ||this.props.icon ==  icons.Question_Circle) {
            icon = <FontawesomeIcon nameIcon={this.props.icon} color={colors.red} size={MeasurementCalculator.measureFont(75)} />
        } else {
            icon = <View style={styles.containerIcon}>
                <FontawesomeIcon nameIcon={this.props.icon} color={colors.white} size={MeasurementCalculator.measureFont(25)} />
            </View>
        }

        return (
            <View style={[styles.container, { backgroundColor: this.props.backgroundColor }]}>
                <TouchableOpacity style={styles.touchView} onPress={this.props.cancel}>
                    <View style={styles.containerIconClose}>
                        <FontawesomeIcon nameIcon={icons.Close} color={colors.white} size={MeasurementCalculator.measureFont(14)} />
                    </View>
                </TouchableOpacity>
                {icon}
                <Text style={styles.title}>{this.props.title}</Text>
                {description}
                <View style={styles.buttonView}>
                    <OverlayButton
                        buttonBackgroundColor={colors.white}
                        buttonTextColor={colors.black}
                        clickButton={this.props.cancel}
                        style={{ flex: 1 }}
                        text={this.props.oneButton}
                        customStyle={styles.popupButton}
                        textButtonStyle={styles.popupTextButton} />
                </View>
                {textBetweenButtons}
                {secondButton}
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        opacity: 0.92,
        alignItems: 'center',
        paddingVertical: MeasurementCalculator.measure(43),
        paddingHorizontal: MeasurementCalculator.measure(25, 'w'),
        borderRadius: 13,
    },
    containerIconClose: {
        height: MeasurementCalculator.measure(35.22),
        width: MeasurementCalculator.measure(35.22),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        backgroundColor: colors.black,
    },
    touchView: {
        position: 'absolute',
        right: MeasurementCalculator.measure(-40, 'w'),
        top: MeasurementCalculator.measure(-40),
        height: MeasurementCalculator.measure(80),
        width: MeasurementCalculator.measure(80, 'w'),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'
    },
    containerIcon: {
        height: MeasurementCalculator.measure(75),
        width: MeasurementCalculator.measure(75),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        backgroundColor: colors.green
    },
    title: {
        fontSize: MeasurementCalculator.measureFont(24),
        fontFamily: fonts.Montserrat_Regular,
        color: colors.white,
        lineHeight: 30,
        textAlign: 'center',
        marginTop: MeasurementCalculator.measure(25),
    },
    description: {
        fontSize: MeasurementCalculator.measureFont(16),
        fontFamily: fonts.Roboto_Regular,
        color: colors.white,
        lineHeight: 24,
        letterSpacing: 0.12,
        textAlign: 'center',
        marginTop: MeasurementCalculator.measure(10),
    },
    buttonView: {
        marginTop: MeasurementCalculator.measure(11),
    },
    popupButton: {
        height: MeasurementCalculator.measure(47),
        width: MeasurementCalculator.measure(280, 'w'),
        borderRadius: 23.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    popupTextButton: {
        fontFamily: fonts.Karla_Regular,
        fontSize: MeasurementCalculator.measureFont(17),
        letterSpacing: 0,
    },
    textBetweenButtons: {
        fontSize: MeasurementCalculator.measureFont(19),
        fontFamily: fonts.Montserrat_Medium,
        color: colors.black4,
        textAlign: 'center',
        marginTop: MeasurementCalculator.measure(15),
        letterSpacing: 0.7,
    },
});

export default ConfirmationPopup;
