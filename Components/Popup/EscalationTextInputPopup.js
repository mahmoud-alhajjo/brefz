import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput } from 'react-native';

import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import OverlayButton from '../Helpers/OverlayButton';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

class EscalationTextInputPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        let textInputShadowiOS = Platform.OS === 'ios' ? styles.textInputShadowiOS : null
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.touchView} onPress={this.props.cancel}>
                    <View style={styles.containerIconClose}>
                        <FontawesomeIcon nameIcon={icons.Close} color={colors.white} size={MeasurementCalculator.measureFont(14)} />
                    </View>
                </TouchableOpacity>
                <Text style={styles.textLabel}>Set The Reason</Text>
                <TextInput
                    ref="reasonEscalationInput"
                    style={[styles.inputStyle, textInputShadowiOS]}
                    placeholder={"Enter Your Reason"}
                    textAlignVertical={'top'}
                    underlineColorAndroid={"transparent"}
                    placeholderTextColor={colors.placeholderColor}
                    multiline={true}
                    numberOfLines={4}
                    autoCapitalize={"none"}
                    autoCorrect={false}
                    blurOnSubmit={false}
                    value={this.state.address}
                    onSubmitEditing={(event) => this.refs.reasonEscalationInput.blur()}
                />
                <OverlayButton
                    buttonBackgroundColor={colors.lightGreen}
                    buttonTextColor={colors.white}
                    clickButton={this.props.cancel}
                    style={{ flex: 1 }}
                    text={'Send Escalation'}
                    customStyle={styles.cancelButton}
                    textButtonStyle={styles.cancelTextButton} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: MeasurementCalculator.measure(33.1),
        paddingHorizontal: MeasurementCalculator.measure(27.5, 'w'),
        borderRadius: 13,
        backgroundColor: colors.white,
    },
    containerIconClose: {
        height: MeasurementCalculator.measure(35.22),
        width: MeasurementCalculator.measure(35.22),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        backgroundColor: colors.black,
    },
    touchView: {
        position: 'absolute',
        right: MeasurementCalculator.measure(-40, 'w'),
        top: MeasurementCalculator.measure(-40),
        height: MeasurementCalculator.measure(80),
        width: MeasurementCalculator.measure(80, 'w'),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'
    },
    textLabel: {
        fontSize: MeasurementCalculator.measureFont(21),
        fontFamily: fonts.Montserrat_Regular,
        color: colors.black2,
    },
    inputStyle: {
        backgroundColor: colors.white,
        marginTop: MeasurementCalculator.measure(23.9),
        height: MeasurementCalculator.measure(185),
        width: MeasurementCalculator.measure(263, 'w'),
        paddingTop: MeasurementCalculator.measure(21),
        paddingLeft: MeasurementCalculator.measure(25, 'w'),
        fontSize: 16,
        fontFamily: fonts.Karla_Regular,
        color: colors.black,
        borderColor: colors.black_5,
        borderRadius: 8,
        elevation: 3
    },
    textInputShadowiOS: {
        borderWidth: 1,
        borderColor: colors.black_5,
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.05,
        shadowRadius: 14
    },
    cancelButton: {
        height: MeasurementCalculator.measure(47),
        width: MeasurementCalculator.measure(259, 'w'),
        marginTop: MeasurementCalculator.measure(32),
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cancelTextButton: {
        fontFamily: fonts.Karla_Regular,
        fontSize: MeasurementCalculator.measureFont(17),
    },
})
export default EscalationTextInputPopup;
