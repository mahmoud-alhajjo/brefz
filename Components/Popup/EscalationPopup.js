import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import mainStyles from '../../css/MainStyle';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import OverlayButton from '../Helpers/OverlayButton';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Comments from '../Main Structure/Comments';

class EscalationPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={[styles.container]}>
                <TouchableOpacity style={styles.touchView} onPress={this.props.cancel}>
                    <View style={styles.containerIconClose}>
                        <FontawesomeIcon nameIcon={icons.Close} color={colors.white} size={MeasurementCalculator.measureFont(14)} />
                    </View>
                </TouchableOpacity>
                <View style={{ paddingHorizontal: MeasurementCalculator.measure(24.7, 'w'), paddingBottom: MeasurementCalculator.measure(10) }}>
                    <View style={[styles.titleEscalation, mainStyles.rowFlex, { justifyContent: 'space-between', alignItems: 'center' }]}>
                        <Text style={styles.textEscalation}><FontawesomeIcon nameIcon={icons.Exclamation_Triangle} color={colors.white} size={MeasurementCalculator.measureFont(13)} /> Escalation</Text>
                        <OverlayButton
                            buttonBackgroundColor={colors.red5}
                            buttonTextColor={colors.white}
                            clickButton={this.props.cancel}
                            style={{ flex: 1 }}
                            text={' CANCEL'}
                            nameIcon={icons.Exclamation_Circle}
                            size={MeasurementCalculator.measureFont(12)}
                            customStyle={styles.cancelButton}
                            textButtonStyle={styles.cancelTextButton} />
                    </View>
                    <Text style={styles.subtitleEscalation}>When I was 5 years old, my mother...Lorem When I was 5 years old, mymother... Lorem When I was 5 yearsold, my mother... Lorem</Text>
                </View>
                <Comments customBorderBottomRadius={true}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        paddingTop: MeasurementCalculator.measure(19),
        borderRadius: 13,
        backgroundColor: colors.white,
    },
    containerIconClose: {
        height: MeasurementCalculator.measure(35.22),
        width: MeasurementCalculator.measure(35.22),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        backgroundColor: colors.black,
    },
    touchView: {
        position: 'absolute',
        right: MeasurementCalculator.measure(-40, 'w'),
        top: MeasurementCalculator.measure(-40),
        height: MeasurementCalculator.measure(80),
        width: MeasurementCalculator.measure(80, 'w'),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'
    },
    titleEscalation: {
        backgroundColor: colors.darkRed,
        paddingRight: MeasurementCalculator.measure(12.8, 'w'),
        paddingLeft: MeasurementCalculator.measure(23.3, 'w'),
        paddingVertical: MeasurementCalculator.measure(10.5),
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
    },
    textEscalation: {
        fontSize: MeasurementCalculator.measureFont(13),
        fontFamily: fonts.Montserrat_Regular,
        color: colors.white,
        paddingRight: MeasurementCalculator.measure(10, 'w'),
    },
    subtitleEscalation: {
        fontSize: MeasurementCalculator.measureFont(12),
        fontFamily: fonts.Montserrat_Medium,
        color: colors.white,
        lineHeight: 20,
        backgroundColor: colors.lightRed4,
        paddingLeft: MeasurementCalculator.measure(23.3, 'w'),
        paddingRight: MeasurementCalculator.measure(32, 'w'),
        paddingVertical: MeasurementCalculator.measure(17.5),
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
    },
    cancelButton: {
        height: MeasurementCalculator.measure(21),
        width: MeasurementCalculator.measure(81, 'w'),
        borderRadius: 29,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cancelTextButton: {
        fontFamily: fonts.Montserrat_Medium,
        fontSize: MeasurementCalculator.measureFont(12),
        letterSpacing: 0.83,
    },
})

export default EscalationPopup;
