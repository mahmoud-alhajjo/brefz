import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';
import EscalationPopup from '../Popup/EscalationPopup';

import Modal from 'react-native-modal';
import { closeOverlay, openOverlay } from 'react-native-blur-overlay';

class TimeLineItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            escalationModal: false
        };
    }

    getIcon = (icon) => {
        let iconMain = ""
        let size = 20
        switch (icon) {
            case "Meetings":
                iconMain = icons.Handshake
                break;
            case "Customer":
                iconMain = icons.User
                size = 16
                break;
            case "ProjectManager":
                iconMain = icons.Users
                size = 16
                break;
            case "SupportingFile":
                iconMain = icons.Paperclip
                break;
            case "Escalation":
                iconMain = icons.Exclamation_Triangle
                size = 16
                break;
            case "Payment":
                iconMain = icons.Money
                break;
            case "ProjectDetails":
                iconMain = <Image style={{ width: MeasurementCalculator.measure(17.92, 'w'), height: MeasurementCalculator.measure(20.24) }} resizeMode="contain" source={require('../../Images/solo.png')} />
                break;
            case "Phase":
                iconMain = <Image style={{ width: MeasurementCalculator.measure(23.16, 'w'), height: MeasurementCalculator.measure(21.99) }} resizeMode="contain" source={require('../../Images/Phase.png')} />
            default:
                break;
        }
        if (icon == "Phase" || icon == "ProjectDetails") {
            return <View style={[styles.iconContainer, { borderColor: this.props.item.color }]}>
                {iconMain}
            </View>
        }
        else {
            return <View style={[styles.iconContainer, { borderColor: this.props.item.color }]}>
                <FontawesomeIcon nameIcon={iconMain} color={this.props.item.color} size={MeasurementCalculator.measureFont(size)} />
            </View>
        }
    }

    getIconType = (type) => {
        let iconType = ""
        let color = ""
        switch (type) {
            case "Added":
                iconType = icons.Plus
                color = colors.green2;
                break;
            case "Deleted":
                iconType = icons.Trash_O
                color = colors.red3
                break;
            case "Send Invoice":
                iconType = icons.Paper_Plane
                color = colors.blue3
                break;
            case "Escalation":
                iconType = icons.Exclamation_Triangle
                color = colors.red4
                break;
            case "Attendance":
                iconType = icons.Calendar_Plus_O
                color = colors.darkBlue4
                break;
            case "Edit":
                iconType = icons.Cog
                color = colors.yellow6
                break;
            default:
                break;
        }
        return <Text style={[styles.type, { backgroundColor: color }]}>
            <FontawesomeIcon nameIcon={iconType} color={colors.white} size={MeasurementCalculator.measureFont(9)} customStyle={{ fontWeight: '800' }} /> {type}
        </Text>;
    }

    openEscalationPopup = () => {
        openOverlay();
        this.setState({ escalationModal: true });
    };

    closeEscalationPopup = () => {
        this.setState({ escalationModal: false });
        closeOverlay();
    }

    navigationAction = (title) => {
        let screenToNavigate = null
        switch (title) {
            case "Meetings":
                screenToNavigate = 'MeetingDetails'
                break;
            case "Supporting File":
                screenToNavigate = 'SupportingFileSingle'
                break;
            case "Project Manager":
                screenToNavigate = 'AllPhases'
                break;
            case "Escalation":
                screenToNavigate = 'Escalation'
                break;
            case "Payment":
                screenToNavigate = 'PaymentSingle'
                break;
            case "Project Details":
                screenToNavigate = 'AllSupportingFiles'
                break;
            case "Phase":
                screenToNavigate = 'PhaseSingle'
                break;
            case "Customer":
                screenToNavigate = 'AllPayments'
                break;
            default:
                break;
        }
        if (screenToNavigate == 'Escalation')
            this.openEscalationPopup()
        else
            this.props.navigation.navigate(screenToNavigate);
    }

    render() {
        let cardShadowiOS = Platform.OS === 'ios' ? styles.cardShadowiOS : null
        let title = this.props.item.title ? this.props.item.title : null
        let color = this.props.item.color ? this.props.item.color : null
        let icon = this.props.item.icon ? this.props.item.icon : null;
        let type = this.props.item.type ? this.props.item.type : null;
        let warning = null;
        if (this.props.item.warning) {
            warning = <Text style={styles.warning}>{this.props.item.warning}</Text>
        }
        return (
            <TouchableOpacity onPress={() => this.navigationAction(title)}>
                <View style={[mainStyles.rowFlex, mainStyles.flex, styles.container, cardShadowiOS, { borderLeftColor: color }]}>
                    <View style={[styles.triangle, { borderBottomColor: color }]} />
                    {this.getIcon(icon)}
                    <View style={[mainStyles.columnFlex, mainStyles.flex, styles.details]}>
                        {this.getIconType(type)}
                        <Text style={styles.title}>{this.props.item.title}</Text>
                        <View style={mainStyles.rowFlex}>
                            <Image style={styles.imageStyle} resizeMode='contain' source={this.props.item.image} />
                            <View>
                                <Text style={styles.name}>{this.props.item.name}</Text>
                                <Text style={styles.time}>{this.props.item.time}</Text>
                            </View>
                        </View>
                        <Text style={styles.description}>{this.props.item.description}</Text>
                        <Text style={[styles.subtitle, { color: color }]}>'{this.props.item.subtitle}'</Text>
                        {warning}
                    </View>
                </View>
                <Modal
                    isVisible={this.state.escalationModal}
                    animationIn={'slideInUp'}
                    animationInTiming={500}
                    animationOut={'slideOutDown'}
                    animationOutTiming={700}
                    backdropOpacity={.05}
                    backdropColor={colors.black}
                    style={{ justifyContent: 'center', alignItems: 'center' }}
                >
                    <EscalationPopup
                        cancel={() => this.closeEscalationPopup()}
                    />
                </Modal>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: "flex-start",
        marginBottom: MeasurementCalculator.measure(26.8),
        marginLeft: MeasurementCalculator.measure(60.3, 'w'),
        marginRight: MeasurementCalculator.measure(17.9, 'w'),
        backgroundColor: colors.white,
        borderLeftWidth: 3.75,
        borderBottomRightRadius: 5,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
        elevation: 3.5
    },
    cardShadowiOS: {
        borderWidth: 1,
        borderColor: colors.gray6,
        shadowColor: colors.gray6,
        shadowOffset: {
            width: 0,
            height: 11,
        },
        shadowRadius: 14
    },
    triangle: {
        position: 'absolute',
        width: 0,
        height: 0,
        top: MeasurementCalculator.measure(38.5),
        left: MeasurementCalculator.measure(-8.6, 'w'),
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: MeasurementCalculator.measure(5.5),
        borderRightWidth: MeasurementCalculator.measure(5.5),
        borderBottomWidth: MeasurementCalculator.measure(8, 'w'),
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        transform: [
            { rotate: '-90deg' }
        ]
    },
    iconContainer: {
        borderRadius: 50,
        borderWidth: 1,
        width: MeasurementCalculator.measure(52),
        height: MeasurementCalculator.measure(52),
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: MeasurementCalculator.measure(19),
        marginLeft: MeasurementCalculator.measure(16, 'w')
    },
    details: {
        backgroundColor: colors.white,
        paddingRight: MeasurementCalculator.measure(18.6, 'w'),
        paddingLeft: MeasurementCalculator.measure(16, 'w'),
        paddingVertical: MeasurementCalculator.measure(25),
    },
    type: {
        position: 'absolute',
        left: MeasurementCalculator.measure(16, 'w'),
        top: MeasurementCalculator.measure(-7.1),
        paddingLeft: MeasurementCalculator.measure(9, 'w'),
        paddingRight: MeasurementCalculator.measure(13, 'w'),
        paddingVertical: MeasurementCalculator.measure(6),
        fontFamily: fonts.Montserrat_Bold,
        textAlign: 'center',
        fontSize: MeasurementCalculator.measureFont(9),
        color: colors.white,
        borderRadius: 5,
        zIndex: 1,
    },
    title: {
        fontSize: MeasurementCalculator.measureFont(15),
        fontFamily: fonts.Montserrat_Bold,
        color: colors.black,
        borderBottomWidth: 1,
        borderBottomColor: colors.gray6,
        paddingBottom: MeasurementCalculator.measure(8.5),
    },
    imageStyle: {
        height: MeasurementCalculator.measure(26.94),
        width: MeasurementCalculator.measure(26.94),
        borderRadius: 50,
        marginTop: MeasurementCalculator.measure(13.5),
    },
    name: {
        color: colors.black4,
        fontSize: MeasurementCalculator.measureFont(16),
        fontFamily: fonts.Montserrat_SemiBold,
        marginTop: MeasurementCalculator.measure(9.5),
        marginLeft: MeasurementCalculator.measure(9.1, 'w'),
    },
    time: {
        color: colors.gray8,
        fontSize: MeasurementCalculator.measureFont(10),
        fontFamily: fonts.Montserrat_SemiBold,
        marginLeft: MeasurementCalculator.measure(9.1, 'w')
    },
    description: {
        color: colors.gray9,
        fontSize: MeasurementCalculator.measureFont(15),
        fontFamily: fonts.Montserrat_Regular,
        marginTop: MeasurementCalculator.measure(7.1),
    },
    subtitle: {
        fontSize: MeasurementCalculator.measureFont(15),
        fontFamily: fonts.Montserrat_Bold,
    },
    warning: {
        paddingRight: MeasurementCalculator.measure(33, 'w'),
        paddingLeft: MeasurementCalculator.measure(12, 'w'),
        paddingVertical: MeasurementCalculator.measure(12),
        fontFamily: fonts.Montserrat_Regular,
        fontSize: 10,
        color: colors.black4,
        backgroundColor: colors.lightRed2,
        lineHeight: 18,
        marginRight: MeasurementCalculator.measure(29.4, 'w'),
        marginTop: MeasurementCalculator.measure(11)
    }
})

export default TimeLineItem;
