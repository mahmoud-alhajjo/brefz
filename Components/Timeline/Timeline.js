import React, { Component } from 'react';
import { View, Text, StyleSheet, SectionList } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

import TimeLineItem from './TimeLineItem';
import DateItem from './DateItem';

class Timeline extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };

    DATA = [
      {
        date: '20/7/2017',
        data: [{
          type: "Added",
          title: "Meetings",
          name: "Maya Smith",
          time: '15:30',
          color: '#FFCC29',
          icon: 'Meetings',
          image: require('../../Images/imgeNotification.png'),
          description: "Has Request a Meeting",
          subtitle: "Discuss beta APK",
        },
        {
          type: "Added",
          title: "Supporting File",
          name: "Maya Smith",
          time: '15:30',
          color: '#7646D1',
          icon: 'SupportingFile',
          image: require('../../Images/imgeNotification.png'),
          description: "has added as a new supporting file",
          subtitle: "Beta APK",
        },
        {
          type: "Deleted",
          title: "Payment",
          name: "Maya Smith",
          time: '15:30',
          color: '#6DE381',
          icon: 'Payment',
          image: require('../../Images/imgeNotification.png'),
          description: "Has deleted Payment",
          subtitle: "Number #21",
        },
        {
          type: "Send Invoice",
          title: "Phase",
          name: "Maya Smith",
          time: '15:30',
          color: '#DA6DE3',
          icon: 'Phase',
          image: require('../../Images/imgeNotification.png'),
          description: "Has deleted Phase",
          subtitle: "Design",
        },
        {
          type: "Attendance",
          title: "Customer",
          name: "Maya Smith",
          time: '15:30',
          color: '#43F2B5',
          icon: 'Customer',
          image: require('../../Images/imgeNotification.png'),
          description: "Has added as Customer",
          subtitle: "Jake Gypsum",
        },
        {
          type: "Added",
          title: "Project Manager",
          name: "Maya Smith",
          time: '15:30',
          color: '#E69423',
          icon: 'ProjectManager',
          image: require('../../Images/imgeNotification.png'),
          description: "Has added as Customer",
          subtitle: "Jake Gypsum",
        },
        {
          type: "Escalation",
          title: "Escalation",
          name: "Maya Smith",
          time: '15:30',
          color: '#F83F6E',
          icon: 'Escalation',
          image: require('../../Images/imgeNotification.png'),
          description: "Has set an for the following reason:",
          subtitle: "Escalation Action",
          warning: "The project manager can't understand what i want."
        },
        {
          type: "Edit",
          title: "Project Details",
          name: "Maya Smith",
          time: '15:30',
          color: '#48C4D9',
          icon: 'ProjectDetails',
          image: require('../../Images/imgeNotification.png'),
          description: "Has set the for project details",
          subtitle: "Estimated cost to 20,000$",
        },
        ],
      },
      {
        date: '20/7/2017',
        data: [{
          type: "Added",
          title: "Meetings",
          name: "Maya Smith",
          time: '15:30',
          color: '#7646D1',
          icon: 'ProjectDetails',
          image: require('../../Images/imgeNotification.png'),
          description: "Has Request a Meeting",
          subtitle: "Discuss beta APK",
        }],
      },
    ];
  }

  render() {
    let noDates = <View style={[mainStyles.flex, { justifyContent: 'center', alignItems: 'center' }]}><Text style={styles.textNoDates}>You have no dates</Text></View>
    return (
      <View style={[mainStyles.flex, styles.container]}>
        <SectionList
          sections={DATA}
          keyExtractor={(item, index) => item + index}
          renderItem={({ item, index }) => <TimeLineItem item={item} index={index} navigation={this.props.navigation} />}
          renderSectionHeader={({ section: { date } }) => (<DateItem date={date} />)}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: MeasurementCalculator.measureFont(35), paddingTop: MeasurementCalculator.measureFont(26.8)}}
        />
        <View style={styles.timeLine} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
  },
  textNoDates: {
    fontFamily: fonts.Montserrat_SemiBold,
    fontSize: MeasurementCalculator.measureFont(18),
    textAlign: "center",
    color: colors.gray4
  },
  timeLine: {
    position: 'absolute',
    left: MeasurementCalculator.measureFont(29.5, 'w'),
    top: 0,
    width: MeasurementCalculator.measureFont(11, 'w'),
    height: mainStyles.fitScreen.height,
    backgroundColor: colors.gray2,
    zIndex: 3
  },
});

export default Timeline;
