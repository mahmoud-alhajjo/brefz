import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

class DateItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.circleTimeLine} />
                <Text style={styles.date}>{this.props.date}</Text>
                <View style={styles.triangle} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: MeasurementCalculator.measureFont(8.2),
        backgroundColor: colors.white 
    },
    date: {
        marginBottom: MeasurementCalculator.measure(18),
        marginLeft: MeasurementCalculator.measure(61.5, 'w'),
        marginRight: MeasurementCalculator.measure(238.9, 'w'),
        fontFamily: fonts.Karla_Bold,
        color: colors.white,
        borderRadius: 5,
        backgroundColor: colors.darkGray,
        textAlign: 'center',
        fontSize: MeasurementCalculator.measureFont(13),
        paddingVertical: MeasurementCalculator.measure(5),
        paddingHorizontal: MeasurementCalculator.measure(5.3, 'w'),
    },
    triangle: {
        position: 'absolute',
        width: 0,
        height: 0,
        top: MeasurementCalculator.measure(7.5),
        left: MeasurementCalculator.measure(56, 'w'),
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: MeasurementCalculator.measure(5.5),
        borderRightWidth: MeasurementCalculator.measure(5.5),
        borderBottomWidth: MeasurementCalculator.measure(8, 'w'),
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: colors.darkGray,
        transform: [
            { rotate: '-90deg' }
        ]
    },
    circleTimeLine: {
        width: MeasurementCalculator.measure(27),
        height: MeasurementCalculator.measure(27),
        borderRadius: 50,
        backgroundColor: colors.gray2,
        position: 'absolute',
        left: MeasurementCalculator.measure(21.5, 'w'),
        top: 0,
        zIndex: 2
    }
});

export default DateItem;
