import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import mainStyles from '../../css/MainStyle';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

class PaymentItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            heightPaymentValue: 0,
        };
    }

    PaymentSingleAction = () => {      
        this.props.navigation.navigate('PaymentSingle', {
            hideOverlayInNextScreen: true
        });
    }

    findDimesions(layout) {
        const { x, y, width, height } = layout;
        this.setState({ heightPaymentValue: height })
    }

    render() {
        let circleDoneBackgroundColor = this.props.item.status == 'Done' ? colors.green4 : this.props.item.status == 'WAITING' ? colors.yellow7 : colors.red6
        let textDoneBackgroundColor = this.props.item.status == 'Done' ? colors.lightGreen4 : this.props.item.status == 'WAITING' ? colors.lightYellow2 : colors.lightRed5
        let backgroundColor = colors.white
        let backgroundColorCorner = colors.lightGray
        let paddingTop = MeasurementCalculator.measure(29) - this.state.heightPaymentValue / 2
        let paddingBottomLastItem = 0
        if (this.props.index === 0) {
            paddingTop = this.props.heightDrawerHeader + MeasurementCalculator.measure(19)
        }
        if ((this.props.index % 2) == 0) {
            backgroundColor = colors.lightGray
            backgroundColorCorner = colors.white
        }
        if (this.props.lastItem == this.props.index) {
            paddingBottomLastItem = MeasurementCalculator.measure(50)
        }
        return (
            <TouchableOpacity onPress={this.PaymentSingleAction} activeOpacity={.9}>
                <View style={[mainStyles.flex, { backgroundColor: backgroundColorCorner, paddingBottom: paddingBottomLastItem }]}>
                    <View style={[styles.allPaymentsDetails, mainStyles.rowFlex, { backgroundColor: backgroundColor, marginBottom: this.state.heightPaymentValue / 2, paddingTop: paddingTop }]}>
                        <Text style={styles.numberOfPayment}>{this.props.item.number}   <Text style={styles.textPayment}>Payment</Text></Text>
                        <View style={[mainStyles.rowFlex, styles.paymentDone, { alignItems: 'center', backgroundColor: textDoneBackgroundColor }]}>
                            <View style={[styles.circleDone, { backgroundColor: circleDoneBackgroundColor }]} />
                            <Text style={styles.textDone}> {this.props.item.status}</Text>
                        </View>
                        <Text onLayout={(event) => { this.findDimesions(event.nativeEvent.layout) }} style={[styles.paymentValue, { bottom: - this.state.heightPaymentValue / 2 }]}>${this.props.item.value}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    allPaymentsDetails: {
        paddingLeft: MeasurementCalculator.measure(19, 'w'),
        paddingRight: MeasurementCalculator.measure(56, 'w'),
        paddingTop: MeasurementCalculator.measure(29),
        paddingBottom: MeasurementCalculator.measure(32.3),
        borderBottomRightRadius: 80,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    numberOfPayment: {
        fontFamily: fonts.Helvetica_Neue,
        fontSize: MeasurementCalculator.measureFont(20),
        color: colors.gray9,
        textAlign: 'center',
    },
    textPayment: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: MeasurementCalculator.measureFont(16),
        color: colors.purple2,
        textAlign: 'center',
        paddingRight: MeasurementCalculator.measure(10, 'w'),
    },
    paymentDone: {
        paddingHorizontal: MeasurementCalculator.measure(7, 'w'),
        paddingVertical: MeasurementCalculator.measure(2.5),
        borderRadius: 5,
        backgroundColor: colors.lightGreen4,
    },
    circleDone: {
        width: MeasurementCalculator.measure(8),
        height: MeasurementCalculator.measure(8),
        backgroundColor: colors.green4,
        borderRadius: 50,
    },
    textDone: {
        fontSize: MeasurementCalculator.measureFont(12),
        fontFamily: fonts.Montserrat_Medium,
        color: colors.black,
    },
    paymentValue: {
        zIndex: 2,
        position: 'absolute',
        left: MeasurementCalculator.measure(17, 'w'),
        fontFamily: fonts.Montserrat_Medium,
        fontSize: MeasurementCalculator.measureFont(12),
        color: colors.white,
        textAlign: 'center',
        letterSpacing: 1.93,
        paddingHorizontal: MeasurementCalculator.measure(14.5, 'w'),
        paddingVertical: MeasurementCalculator.measure(4),
        borderRadius: 29,
        backgroundColor: colors.lightBlue,
    }
})

export default PaymentItem;
