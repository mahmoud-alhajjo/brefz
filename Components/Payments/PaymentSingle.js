import React, { Component } from 'react';
import { View, Text, StyleSheet, Animated, Dimensions } from 'react-native';
import colors from '../../css/colors';
import * as fonts from '../../css/fonts';
import * as icons from '../../css/icons';
import FontawesomeIcon from '../../css/FontawesomeIcon';
import mainStyles from '../../css/MainStyle';
import OverlayButton from '../Helpers/OverlayButton';
import MeasurementCalculator from '../Helpers/MeasurementCalculator';

import Comments from '../Main Structure/Comments';
import DrawerHeader from '../Main Structure/DrawerHeader';

class PaymentSingle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animationDrawer: new Animated.Value(-Dimensions.get('window').width),
        };
    }

    startAnimationOpenDrawer = () => {
        Animated.timing(this.state.animationDrawer, {
            toValue: 0,
            duration: 300,
            delay: 200,
        }).start();
    }

    startAnimationCloseDrawer = () => {
        Animated.timing(this.state.animationDrawer, {
            toValue: -Dimensions.get('window').width,
            duration: 500,
        }).start();
    }

    goBackAction = () => {
        this.startAnimationCloseDrawer();
        this.props.navigation.goBack();
    }

    render() {
        this.startAnimationOpenDrawer();
        const animationStyle = { transform: [{ translateX: this.state.animationDrawer }] }
        let colorOverlay = colors.overlay
        if (this.props.navigation.state.params) {
            const { hideOverlayInNextScreen } = this.props.navigation.state.params
            colorOverlay = hideOverlayInNextScreen ? 'tranparent' : colors.overlay
        }
        return (
            <View style={[styles.overlay, { backgroundColor: colorOverlay }]}>
                <Animated.View style={[animationStyle, styles.container]}>
                    <DrawerHeader navigation={this.props.navigation} title={'Payment #21'} goBackAction={this.goBackAction} />
                    <View style={styles.paymentDetails}>
                        <View style={[mainStyles.rowFlex, { justifyContent: 'flex-start', alignItems: 'center' }]}>
                            <Text style={styles.text}>Status Amount   </Text>
                            <Text style={styles.textAlert}>WAITING</Text>
                        </View>
                        <View style={[mainStyles.rowFlex, { justifyContent: 'flex-start', alignItems: 'center' }]}>
                            <Text style={styles.text}>Due Date   </Text>
                            <Text style={styles.textAlert}>22/07/2019</Text>
                        </View>
                        <View style={[mainStyles.rowFlex, { justifyContent: 'flex-start', alignItems: 'center' }]}>
                            <Text style={styles.text}>Actual Payment Date   </Text>
                            <Text style={styles.textAlert}>22/07/2019</Text>
                        </View>
                        <Text style={styles.textPayment}>$16.287</Text>
                    </View>
                    <Comments />
                </Animated.View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    overlay: {
        flex: 1,
        width: Dimensions.get('window').width,
    },
    container: {
        flex: 1,
        width: Dimensions.get('window').width - 31,
        backgroundColor: colors.white,
    },
    paymentDetails: {
        backgroundColor: colors.lightGray,
        paddingLeft: MeasurementCalculator.measure(28, 'w'),
        paddingRight: MeasurementCalculator.measure(38.4, 'w'),
        paddingTop: MeasurementCalculator.measure(25),
        paddingBottom: MeasurementCalculator.measure(25.5),
        borderBottomRightRadius: 80,
    },
    text: {
        fontFamily: fonts.Montserrat_Light,
        fontSize: MeasurementCalculator.measureFont(16),
        color: colors.purple2,
        lineHeight: 30,
    },
    textAlert: {
        fontFamily: fonts.Montserrat_Bold,
        fontSize: MeasurementCalculator.measureFont(10),
        color: colors.black,
        borderRadius: 8,
        letterSpacing: 2.5,
        backgroundColor: colors.lightYellow,
        paddingVertical: MeasurementCalculator.measure(6.4),
        paddingHorizontal: MeasurementCalculator.measure(16.2, 'w')
    },
    textPayment: {
        fontFamily: fonts.Montserrat_Medium,
        fontSize: MeasurementCalculator.measureFont(14),
        color: colors.white,
        alignSelf: 'flex-start',
        borderRadius: 8,
        backgroundColor: colors.lightBlue,
        paddingVertical: MeasurementCalculator.measure(3.95),
        paddingHorizontal: MeasurementCalculator.measure(11.8, 'w'),
        marginTop: MeasurementCalculator.measure(16.9),
    },
})

export default PaymentSingle;
